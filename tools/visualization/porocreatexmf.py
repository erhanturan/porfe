#!/usr/bin/python

# First  version is written by Cyril Flaig - createxmf.py
# Second version is written by Erhan Turan to include pressure values as well as the flux unknonws.

# TODO
# cell normals are listed in the

# normally a face might have three components of flux values.
# however, we have used only one normal per face (because of Voxel+RT0 elements)
# depending on the orientation of the cell, we might define again three components but two of them
# will be zero. of course, it will change for each face.
# we might create a post-processing operation where the faces are listed with additional - zero components
# in other two directions.


# in paraview, there is a format named Polyhedron which defines the faces explicitly as well. Perhaps I can define Polyhedron
# instead of Hexahedron. some output on surface definitions are given in the test/ folder.
# a surface is defined like:
# POLYGONS 120 600
#4 0 1 2 3 
#4 0 4 5 1 
# where 120 is the number of surfaces and 600 is the number of entries. there are 5 entries per face. 4 is the number of nodes and the others are the information about the connectivity.


# there are two important filters in paraview. one is the "Extract Surface" and the other is "Surface Flow"

# i need a connectvity list over the nodes of the elements. which provides the corners of the polygons.


import h5py
import sys

if  len(sys.argv) != 2:
  print "usage: "+sys.argv[0]+" filename"
  sys.exit(0)

filename = sys.argv[1]
try:
  f = h5py.File(filename,'r+')
except:
  print "usgage: "+sys.argv[0]+" filename"
  sys.exit(2)

#get numbers
nr_nodes = f['Parameters']['nr_nodes'].value
nr_elements = f['Parameters']['nr_elements'].value
nr_faces = f['Parameters']['nr_faces'].value

print "nodes: ", nr_nodes, " elements: ", nr_elements, " faces: ", nr_faces

# xml
sStart = "<?xml version=\"1.0\" ?>\n" \
         "<!DOCTYPE Xdmf SYSTEM \"Xdmf.dtd\" []>\n" \
         "<Xdmf Version=\"2.0\">\n" \
         " <Domain>\n" \
         "   <Grid Name=\"mesh1\" GridType=\"Uniform\">\n"

sMesh =  "     <Topology TopologyType=\"Hexahedron\" NumberOfElements=\""+repr(nr_elements)+"\" BaseOffset=\"1\" >\n" \
         "       <DataItem Dimensions=\""+repr(nr_elements)+" 8\" NumberType=\"Int\" Format=\"HDF\">\n" \
         "         "+filename+":/Mesh/Elements\n" \
         "       </DataItem>\n" \
         "     </Topology>\n" \
         "     <Geometry GeometryType=\"XYZ\">\n" \
         "       <DataItem Dimensions=\""+repr(nr_nodes)+" 3\" NumberType=\"Float\" Precision=\"8\" Format=\"HDF\">\n" \
         "         "+filename+":/Mesh/Coordinates\n" \
         "       </DataItem>\n" \
         "     </Geometry>\n"

sDisp =  "     <Attribute Name=\"Displacement\" AttributeType=\"Vector\" Center=\"Node\">\n" \
         "       <DataItem Dimensions=\""+repr(nr_nodes)+" 3\" Format=\"HDF\" NumberType=\"Float\" Precision=\"8\" >\n" \
         "         "+filename+":/Solution/nodal_displacements\n" \
         "       </DataItem>\n" \
         "     </Attribute>\n"

sForce =  "     <Attribute Name=\"Force\" AttributeType=\"Vector\" Center=\"Node\">\n" \
         "       <DataItem Dimensions=\""+repr(nr_nodes)+" 3\" Format=\"HDF\" NumberType=\"Float\" Precision=\"8\" >\n" \
         "         "+filename+":/Solution/nodal_forces\n" \
         "       </DataItem>\n" \
         "     </Attribute>\n"
sSED =   "     <Attribute Name=\"SED\" AttributeType=\"Scalar\" Center=\"Cell\">\n" \
         "       <DataItem ItemType=\"HyperSlab\" Dimensions=\""+repr(nr_elements)+" 1\" Type=\"HyperSlab\">\n" \
         "         <DataItem Dimensions=\"3 2\" Format=\"XML\">\n" \
         "           0 6      <!-- START  : from the small dimension the 6th element (7th column) --> \n" \
         "           1 1      <!-- STRIDE : from the 2nd dim all elements -->\n" \
         "           "+repr(nr_elements)+" 1  <!-- COUNT  : -->\n" \
         "         </DataItem>\n" \
         "         <DataItem Dimensions=\""+repr(nr_elements)+" 8\" Format=\"HDF\" NumberType=\"Float\" Precision=\"8\" >\n" \
         "           "+filename+":/Solution/element_strain\n" \
         "         </DataItem>\n" \
         "       </DataItem>\n" \
         "     </Attribute>\n"

sVM =    "     <Attribute Name=\"S_vonMises\" AttributeType=\"Scalar\" Center=\"Cell\">\n" \
         "       <DataItem ItemType=\"HyperSlab\" Dimensions=\""+repr(nr_elements)+" 1\" Type=\"HyperSlab\">\n" \
         "         <DataItem Dimensions=\"3 2\" Format=\"XML\">\n" \
         "           0 6\n" \
         "           1 1\n" \
         "           "+repr(nr_elements)+" 1\n" \
         "         </DataItem>\n" \
         "         <DataItem Dimensions=\""+repr(nr_elements)+" 7\" Format=\"HDF\" NumberType=\"Float\" Precision=\"8\" >\n" \
         "           "+filename+":/Solution/element_stress\n" \
         "         </DataItem>\n" \
         "       </DataItem>\n" \
         "     </Attribute>\n"

sPr =    "     <Attribute Name=\"Pressure\" AttributeType=\"Scalar\" Center=\"Cell\">\n" \
         "       <DataItem ItemType=\"HyperSlab\" Dimensions=\""+repr(nr_elements)+" 1\" Type=\"HyperSlab\">\n" \
         "         <DataItem Dimensions=\"3 2\" Format=\"XML\">\n" \
         "           0 6\n" \
         "           1 1\n" \
         "           "+repr(nr_elements)+" 1\n" \
         "         </DataItem>\n" \
         "         <DataItem Dimensions=\""+repr(nr_elements)+" 7\" Format=\"HDF\" NumberType=\"Float\" Precision=\"8\" >\n" \
         "           "+filename+":/Solution/element_pressures\n" \
         "         </DataItem>\n" \
         "       </DataItem>\n" \
         "     </Attribute>\n"

sFl =    "     <Attribute Name=\"Flux\" AttributeType=\"Scalar\" Center=\"Surface\">\n" \
         "       <DataItem ItemType=\"HyperSlab\" Dimensions=\""+repr(nr_faces)+" 1\" Type=\"HyperSlab\">\n" \
         "         <DataItem Dimensions=\"3 2\" Format=\"XML\">\n" \
         "           0 6\n" \
         "           1 1\n" \
         "           "+repr(nr_faces)+" 1\n" \
         "         </DataItem>\n" \
         "         <DataItem Dimensions=\""+repr(nr_faces)+" 7\" Format=\"HDF\" NumberType=\"Float\" Precision=\"8\" >\n" \
         "           "+filename+":/Solution/face_fluxes\n" \
         "         </DataItem>\n" \
         "       </DataItem>\n" \
         "     </Attribute>\n"

sEnd =   "   </Grid>\n" \
         " </Domain>\n" \
         "</Xdmf>\n"
#write file
outfilename = filename.replace("mesh.h5","xmf")
outfile = open(outfilename, 'w')
outfile.write("%s%s%s%s%s%s%s%s%s" % (sStart, sMesh, sDisp, sForce, sSED, sVM, sPr, sFl, sEnd))
outfile.close()
