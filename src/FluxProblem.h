/*
 * PorFE: a poroelastic extension on ParFE
 * Copyright (C) 2012, Erhan Turan
 * https://bitbucket.org/erhanturan/porfe
 *
 * ParFE: a micro-FE solver for trabecular bone modeling
 * Copyright (C) 2011,  ParFE developers, see
 * http://parfe.sourceforge.net/developers.php
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#ifndef _FLUX_PROBLEM_H_
#define _FLUX_PROBLEM_H_

#include "ElasticityProblem.h"
#include <Epetra_Vector.h>
#include <Epetra_IntVector.h>
#include "DistMesh.h"
#include "BoundaryCondition.h"
#include "ElementByElementMatrix.h"
#include <Epetra_MpiComm.h>
#include <Epetra_FEVbrMatrix.h>
#include <Epetra_FECrsMatrix.h>
#include <Epetra_SerialDenseMatrix.h>
#include "MatrixWriter.h"
#include "VectorWriter.h"
#include "SolutionWriter.h"
#include "fem.h"
#include <Teuchos_ParameterList.hpp>

//! A class to combine parameters, mesh and boundary data into a Epetra_LinearProblem

class FluxProblem : public ElasticityProblem
{
 public:
  //! ElasticityProblem constructor

  /*! The constructor assembles the global stiffness, imposes the boundary conditions and provides a
      complete linear problem Ax = b to be solved.
      After the constructor was called, the input datastructures can be deleted to save memory.

      \param mesh
      (In) Object containing mesh datastructures.

      \param param
      (In) A  Teuchos::ParameterList containing the relevant parameters

      \return Pointer to the created ElasticityProblem.
  */
  FluxProblem(DistMesh& mesh, Teuchos::ParameterList& param, double a_deltaT, int poption_);

  ~FluxProblem();

  int Impose(BoundaryCondition& bcond);

  int Restore();

  Epetra_CrsMatrix& getMatrix();

  Epetra_FECrsMatrix& getFEMatrix();

  Epetra_Vector getDiagonal();

  void PrintSolution(SolutionWriter* sw);

  double ComputeBulkModulus(); //rosta
  double ComputeSe(); //rosta


 private:
  int Assemble();
  void InitializeMaterialProperties();

  double permeability;
  double viscosity;

  double elastic_modulus;
  double poisson_ratio;
  double bulk_modulus_solid;
  double bulk_modulus_fluid;
  double porosity;
  double bulk_modulus;
  double Se;

  int NumFacesPerElement;

  Epetra_CrsMatrix* F;

  Epetra_FECrsMatrix* FEF;

  Epetra_Vector* diag;
  double deltaT;

  Epetra_Map* fluxMap;

  int poption;
};

#endif

