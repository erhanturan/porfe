/*
 * PorFE: a poroelastic extension on ParFE
 * Copyright (C) 2012, Erhan Turan
 * https://bitbucket.org/erhanturan/porfe
 *
 * ParFE: a micro-FE solver for trabecular bone modeling
 * Copyright (C) 2011,  ParFE developers, see
 * http://parfe.sourceforge.net/developers.php
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#include <Epetra_FECrsGraph.h>
//#include <Epetra_CrsMatrix.h>
#include <Epetra_MultiVector.h>
#include "PressureProblem.h"
#include <cmath>
#include <Epetra_Util.h>
PressureProblem::PressureProblem(DistMesh& a_mesh, Teuchos::ParameterList& fe_param)
  : ElasticityProblem(a_mesh, fe_param)
{

  int NumPressurePerElement  = 1; /// might be defined within the mesh file but written here explicitly.
                                  /// similar idea of NumDofsPerNode from nr_dofs_per_node could be used.

  pressure_map = new Epetra_Map(-1, mesh.SchurMap()->NumMyElements(), mesh.SchurMap()->MyGlobalElements(), 0, comm);

  Epetra_Vector* X = new Epetra_Vector(*pressure_map);
  Epetra_Vector* B = new Epetra_Vector(*pressure_map);
  //SetOperator(schur_mat);
  //SetOperator(pressure_mat);
  SetLHS(X);
  SetRHS(B);

  ///initialize the Material properties
  InitializeMaterialProperties();

  ///Assemble();
  Assemble();
}

PressureProblem::~PressureProblem()
{
  delete GetLHS();
  delete GetRHS();
  delete GetOperator();
}



int PressureProblem::Assemble()
{

   // Schur App - BF in poisson matrix format.
   // Se*a^3 - (mu/k)^-1 * 15 * a * alpha

  double AppTerm = Se*pow(EdgeLength,3.0);


  Pvec = new Epetra_Vector(*pressure_map);

  Pvec->PutScalar(-AppTerm); // original is this one.

  return 0;

}


void PressureProblem::PrintSolution(SolutionWriter* sw) {

    Epetra_Vector& pres = *dynamic_cast<Epetra_Vector*>(GetLHS());

    sw->PrintPressures(pres);

}



void PressureProblem::InitializeMaterialProperties(){


  elastic_modulus         = MaterialProperties.A()[0];
  poisson_ratio           = MaterialProperties.A()[1];
  permeability            = MaterialProperties.A()[2];
  viscosity               = MaterialProperties.A()[3];
  bulk_modulus_solid      = MaterialProperties.A()[4];
  bulk_modulus_fluid      = MaterialProperties.A()[5];
  porosity                = MaterialProperties.A()[6];
  bulk_modulus            = ComputeBulkModulus();
  Se                      = ComputeSe();

}


double PressureProblem::ComputeBulkModulus(){

    return elastic_modulus/(3.0*(1.0-2.0*poisson_ratio));

}

double PressureProblem::ComputeSe(){

    return 1.0/bulk_modulus_solid*(1.0-bulk_modulus/bulk_modulus_solid)+porosity*(1.0/bulk_modulus_fluid-1.0/bulk_modulus_solid);

}

int PressureProblem::Impose(BoundaryCondition& bcond){}
int PressureProblem::Restore(){}

Epetra_Vector& PressureProblem::getVector(){

    return *Pvec;

    }
