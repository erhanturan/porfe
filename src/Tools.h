/*
 * PorFE: a poroelastic extension on ParFE
 * Copyright (C) 2012, Erhan Turan
 * https://bitbucket.org/erhanturan/porfe
 *
 * ParFE: a micro-FE solver for trabecular bone modeling
 * Copyright (C) 2011,  ParFE developers, see
 * http://parfe.sourceforge.net/developers.php
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#ifndef TOOLS_H
#define TOOLS_H
#endif

#include <vector>
#include <ctime>

#include "Epetra_Map.h"
#include "Epetra_Vector.h"
#include "Epetra_CrsMatrix.h"
#include "Epetra_SerialDenseMatrix.h"


class DistMesh;

//Helper function to compute memory usage
unsigned meminfo();

void set_null_space(DistMesh* mesh, vector<double>& null_space);

// simple wait function.
void wait ( double seconds );


bool CrsMatrix2MATLAB( const Epetra_CrsMatrix & A );



struct materials
{
    double elastic_modulus;     /// E - Lame
    double E;                   /// E - Lame

    double poisson_ratio;       /// v - Lame
    double v;                   /// v - Lame

    double permeability;        /// k - Darcy
    double k;                   /// k - Darcy

    double viscosity;           /// eta - Darcy
    double eta;                 /// eta - Darcy

    double bulk_modulus_solid;  /// Ks - Poro
    double Ks;                  /// Ks - Poro

    double bulk_modulus_fluid;  /// Kf - Poro
    double Kf;                  /// Kf - Poro

    double porosity;            /// phi - Poro
    double phi;                 /// phi - Poro

    double mu;                  /// mu - Lame , also equivalent to G.
    double G;                  /// mu - Lame , also equivalent to G.

    double lambda;              /// lambda - Lame

    double bulk_modulus;        /// K - Lame
    double K;                   /// K - Lame

    double alpha;               /// alpha - Poro, biot-willis coefficient

    double Se;                  /// Se - Poro, storage coefficient

    double B;                   /// B - Poro

    double poisson_ratio_u;     /// vu - Poro
    double vu;                  /// vu - Poro

    double M;                   /// M - Poro
    double Kappa;               /// kappa - Darcy
    double S;                   /// S - Poro
    double c;                   /// c - Poro
    double Ku;                  /// Ku - Poro

};



materials calculate_parameters( const Epetra_SerialDenseMatrix & MaterialProperties);
