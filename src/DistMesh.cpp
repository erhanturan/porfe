/*
 * ParFE: a micro-FE solver for trabecular bone modeling
 * Copyright (C) 2006 ParFE developers, see
 * http://parfe.sourceforge.net/developers.php
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#include "DistMesh.h"
#include <Epetra_FECrsGraph.h>
#include <Epetra_MultiVector.h>
#include <Epetra_Import.h>
#include <Epetra_Export.h>
#include "parmetis.h"
#include <set>
#include <map>
//#include <pat_api.h>
#include <iostream>

//Include Isorropia_Exception.hpp only because the helper functions at
//the bottom of this file (which create the epetra objects) can
//potentially throw exceptions.
#include <Isorropia_Exception.hpp>

//The Isorropia user-interface functions being demonstrated are declared
//in Isorropia_Epetra.hpp.
#include <Isorropia_Epetra.hpp>
#include <Isorropia_EpetraCostDescriber.hpp>
#include <Isorropia_EpetraRedistributor.hpp>
#include <Isorropia_EpetraPartitioner.hpp>
#include <Teuchos_ParameterList.hpp>
//#include <EpetraExt_MultiVectorOut.h>

//Macros to print trace information
//#define CALL(mesg)\
//     std::cerr << "Proc " << comm.MyPID() << ": "; \
//     std::cerr << "Calling " << mesg << "..." << std::endl;

//#define RET(mesg, ret_val)\
//     std::cerr << "Proc " << comm.MyPID() << ": "; \
//     std::cerr << mesg << " returned " << ret_val << "." << std::endl;

#define CALL(mesg) ;
#define RET(mesg, ret_val) ;

DistMesh::DistMesh(int num_nodes, int dimension, int num_elements, int nodes_per_element, std::map<int,int>& mat_id_map, Epetra_MpiComm& c, int num_faces, int num_fixed_faces, int uweight, int fweight, int pweight)
  : NumNodes(num_nodes),
    Dimension(dimension),
    NumElements(num_elements),
    NumFaces(num_faces), /// erhan added
    NumFixedFaces(num_fixed_faces), /// erhan added
    NumNodesPerElement(nodes_per_element),
    MaterialIDMap(mat_id_map),
    comm(c),
    coordinates(NULL),
    element_nodes(NULL),
    mat_ids(NULL),
    element_faces(NULL), /// erhan added
    element_neighbors(NULL), /// erhan added
    element_coordinates(NULL), /// erhan added
    face_coordinates(NULL), /// erhan added
    element_nodes_face(NULL), /// erhan added
    u_weight(uweight), /// erhan added
    f_weight(fweight), /// erhan added
    p_weight(pweight) /// erhan added

    ///NumNodesPerElement(faces_per_element) /// erhan added
{}


DistMesh::~DistMesh()
{
  delete coordinates;
  delete element_nodes;
  delete mat_ids;
  delete element_faces; /// erhan added
  delete element_neighbors; /// erhan added
  delete fixed_faces; /// erhan added
  delete fixed_face_values; /// erhan added
  delete face_coordinates; /// erhan added
  delete element_coordinates; /// erhan added
  delete element_nodes_face;
}


int DistMesh::VertexGraph(Epetra_CrsGraph& graph) {
  //PAT_region_begin (21, "VertexGraph");

  //PAT_region_begin (31, "Init distro in VertexGraph");
  int* my_elements;
  int num_myelements;
  CALL("DistMesh::ComputeElementEnvelopeOfNodes");
  int ret = ComputeElementEnvelopeOfNodes(graph.Map(), num_myelements, my_elements);
  RET("DistMesh::ComputeElementEnvelopeOfNodes", ret);
  //RedistributeElements(num_myelements, my_elements);
  Epetra_BlockMap elementmap(-1, num_myelements, my_elements, NumNodesPerElement, 0, comm);
  delete[] my_elements;
  Epetra_IntVector* new_elements = new Epetra_IntVector(elementmap);
  Epetra_Import elements_importer(elementmap, element_nodes->Map());
  ret = new_elements->Import(*element_nodes, elements_importer, Insert);
  //PAT_region_end(31);

  //PAT_region_begin(32, "build graph in VertexGraph");
  const Epetra_BlockMap& vtx_map = graph.Map();
  int neighbor_s[3]; //each vertex has three neighbors

  int* global_nodes = new_elements->Values();
  for (int ie = 0 ; ie < new_elements->MyLength(); ie+=NumNodesPerElement)
  {
    int* k = global_nodes+ie;
    for (int i = 0; i < NumNodesPerElement; ++i) {
      int j = k[i];
      if (vtx_map.MyGID(j)) {
	// compute neighbors
	neighbor_s[0] = k[i ^ 1];
	neighbor_s[1] = k[i ^ 3];
	neighbor_s[2] = k[i ^ 4];
	graph.InsertGlobalIndices(j, 3, neighbor_s);
      }
    }
  }
  //PAT_region_end(32);
  //PAT_region_begin(33, "FillComplete() in VertexGraph");
  // Transform to local indices
  ret = graph.FillComplete();
  //PAT_region_end(33);
  // Commented out because of bad performance
  // Arrange elements to build contigous memory blocks
  //if (graph.OptimizeStorage())
  //return -1;
  delete new_elements;
  //PAT_region_end(21);
  return 0;
}

int DistMesh::PoroGraph(Epetra_FECrsGraph& graph,int& num_myglobals, int*& my_globals, int& num_myglobals_face, int*& my_globals_face, int& num_myglobals_element, int*& my_globals_element) {

  int* my_elements;
  int num_myelements;

  const Epetra_Map vtx_map(-1, num_myglobals, my_globals, 0, comm);
  const Epetra_Map face_map(-1, num_myglobals_face, my_globals_face, 0, comm);
  ///const Epetra_Map element_map(-1, num_myglobals_element, my_globals_element, 0, comm);

  CALL("DistMesh::ComputeElementEnvelopeOfNodes");
  int ret = ComputeElementEnvelopeOfNodes(vtx_map, num_myelements, my_elements);
  RET("DistMesh::ComputeElementEnvelopeOfNodes", ret);
  //RedistributeElements(num_myelements, my_elements);
  Epetra_BlockMap elementmap(-1, num_myelements, my_elements, NumNodesPerElement, 0, comm);
  delete[] my_elements;
  Epetra_IntVector* new_elements = new Epetra_IntVector(elementmap);
  Epetra_Import elements_importer(elementmap, element_nodes->Map());
  ret = new_elements->Import(*element_nodes, elements_importer, Insert);

  int neighbor_s[3]; //each vertex has three neighbors

  int* global_nodes = new_elements->Values();
  for (int ie = 0 ; ie < new_elements->MyLength(); ie+=NumNodesPerElement)
  {
    int* k = global_nodes+ie;
    for (int i = 0; i < NumNodesPerElement; ++i) {
      int j = k[i];
      if (vtx_map.MyGID(j)) {
	// compute neighbors
	neighbor_s[0] = k[i ^ 1];
	neighbor_s[1] = k[i ^ 3];
	neighbor_s[2] = k[i ^ 4];
	graph.InsertGlobalIndices(j, 3, neighbor_s);
      }
    }
  }


  /// Fill Aff Graph

  int offsetRow = vtx_map.MaxAllGID()+1;
  int offsetColumn = offsetRow;


  Epetra_BlockMap* elemmapisoFace = new Epetra_BlockMap(-1, num_myglobals_element, my_globals_element,6, 0, comm);


  Epetra_IntVector* new_element_faces = new Epetra_IntVector(*elemmapisoFace);
  Epetra_Export ExporterFace(element_faces->Map(),*elemmapisoFace);
  new_element_faces->Export(*element_faces,ExporterFace,Insert);


  int NumMyElements = new_element_faces->Map().NumMyElements();
  int ElementSize = new_element_faces->Map().ElementSize();
  int* values = new_element_faces->Values();


  int   offIndices[2]; /// indices off-diagonal terms
  int   offIndicesF[1]; /// indices off-diagonal terms
  int myFace;

  for( int i=0 ; i<NumMyElements ; ++i ) {

  for (int j=0;j<6;j++){

      myFace=values[i*ElementSize+j]-1;

      offIndices[0] = myFace+offsetRow;
      offIndices[1] = values[i*ElementSize+5-j]-1+offsetRow;

      offIndicesF[0] = values[i*ElementSize+5-j]-1+offsetRow;


        //graph.InsertGlobalIndices(offIndices[0], 2, offIndices);
        ///graph.InsertGlobalIndices(1, offIndices, 2, offIndices); /// fix for off-proc row access!!
                                                                    /// look at the FECrsGraph for details
                                                                    /// use a 1x2 matrix to send your data!
        graph.InsertGlobalIndices(1, offIndices, 1, offIndicesF);   /// diagonal is excluded

     }

  }


  offsetRow = offsetRow + face_map.MaxAllGID() + 1;


  // Fill Apu Graph

  Epetra_BlockMap* elemmapiso = new Epetra_BlockMap(-1, num_myglobals_element, my_globals_element,8, 0, comm);

  Epetra_IntVector* new_element_nodes_face = new Epetra_IntVector(*elemmapiso);
  Epetra_Export ExporterNodeFace(element_nodes_face->Map(),*elemmapiso);
  new_element_nodes_face->Export(*element_nodes_face,ExporterNodeFace,Insert);
  delete elemmapiso;

  values = new_element_nodes_face->Values();

  ElementSize = new_element_nodes_face->Map().ElementSize();


  int* offIndicesPE;
  int* offIndicesPED;


  offIndicesPE  = new int[8]; /// indices off-diagonal terms
  offIndicesPED  = new int[1]; /// index diagonal term


  for (int i = 0; i < num_myglobals_element; i++){

        for (int j = 0; j < ElementSize; j++){

                offIndicesPE[j] =values[i*ElementSize+j];
        }

       offIndicesPED[0]=my_globals_element[i]+offsetRow;

       graph.InsertGlobalIndices(my_globals_element[i]+offsetRow, 8,  offIndicesPE);

       graph.InsertGlobalIndices(8,  offIndicesPE, 1 , offIndicesPED); ///transpose

  }

  delete[] offIndicesPE;
  delete[] offIndicesPED;

 // Fill Apf Graph

  ElementSize = new_element_faces->Map().ElementSize();
  values = new_element_faces->Values();


  int* offIndicesPF;
  int* offIndicesPFD;

  offIndicesPF  = new int[6]; /// indices off-diagonal terms
  offIndicesPFD  = new int[1]; /// index diagonal terms

  for (int i = 0; i < num_myglobals_element; i++){

        for (int j = 0; j < ElementSize; j++){

                offIndicesPF[j] = values[i*ElementSize+j]-1 + offsetColumn;
        }

       offIndicesPFD[0]=my_globals_element[i]+offsetRow;

       graph.InsertGlobalIndices(my_globals_element[i]+offsetRow, 6,  offIndicesPF);

       graph.InsertGlobalIndices(6,  offIndicesPF, 1 , offIndicesPFD); /// transpose

  }

  delete[] offIndicesPF;
  delete[] offIndicesPFD;



  delete elemmapisoFace;
  delete new_element_faces;

  // Fill App Graph
  offsetColumn = offsetRow;
  int myValue[1];


  /*
  for( int i=0 ; i<num_myglobals_element ; ++i ) {
    myValue[0]=my_globals_element[i]+offsetColumn;
    //graph.InsertGlobalIndices(myValue[0], 1, myValue);
    //if(comm.MyPID()==1) cout<<comm.MyPID()<<" - "<<myValue[0]<<endl;
    graph.InsertGlobalIndices(1,myValue, 1, myValue);
  }
  */


  // Transform to local indices
  ///ret = graph.FillComplete();
  ret = graph.GlobalAssemble();
  //cout<<"total nonzeros: "<<graph.NumGlobalNonzeros()<<" and my nonzeros: "<<graph.NumMyNonzeros()<<endl;

  delete new_elements;

  return ret;
}




int DistMesh::MatrixGraph(Epetra_CrsGraph& graph) {
  //PAT_region_begin (20, "MatrixGraph");
  const Epetra_BlockMap& matrix_map = graph.Map();

  int* global_nodes = element_nodes->Values();
  for (int ie = 0 ; ie < element_nodes->MyLength(); ie+=NumNodesPerElement)
  {
    int* k = global_nodes+ie;
    for (int i = 0; i < NumNodesPerElement; ++i) {
      int j = k[i];
      if (matrix_map.MyGID(j)) {
	graph.InsertGlobalIndices(j, NumNodesPerElement, k);
      }
    }
  }

  // Transform to local indices
  if (graph.FillComplete() != 0)
    return -1;

  // Arrange elements to build contigous memory blocks
  //if (graph.OptimizeStorage() != 0)
  //return -1;
  //PAT_region_end(20);
  return 0;
}

int DistMesh::ComputeElementEnvelopeOfElements()
{
  Epetra_Map elem_map(NumElements, 0, comm);
  Epetra_CrsGraph graph(Copy, elem_map, 32);
  ElementGraph(graph);

  //Get set of elements in the graph
  std::set<int> my_element_set;
  std::vector<int> my_elements;
  for ( int i=0; i<graph.NumMyRows(); ++i ) {
    int len;
    int* indices;
    graph.ExtractMyRowView(i, len, indices);
    my_element_set.insert(indices, indices+len);
  }
  //my_elements.insert(my_elements.end(), my_element_set.begin(), my_element_set.end());
  //TODO: store set of my element nodes
  //TODO: Redistribute Nodes, such that they are unique
  //1. Find ghost elements and the pids of the owners
  //2. Find shared nodes: Intersection of my element nodes and ghost element nodes
  //3. From each sharer, request random value for a specified node (with CreateFromRecvs)
  //4. Choose highest value. If I have highest value, keep the node, else discard.
  //5. ComputeElementEnvelopeOfNodes();
  return RedistributeElements(my_elements.size(), &my_elements[0]);
}


int DistMesh::ComputeElementEnvelopeOfNodes(const Epetra_BlockMap& vtx_map, int& num_myelements, int*& my_elements )
{
    //PAT_region_begin (11, "ComputeElementEnvelopeOfNodes");
  //Redistribute the elements
  //int max = element_nodes->MaxValue();
  //int min = element_nodes->MinValue();
  //int len = max - min + 1;
  //get node gids in the element-to-node table
  std::map<int, int> pid_map;
  //initialize gid_map
  for (int i=0; i<element_nodes->MyLength();++i)
    pid_map[element_nodes->operator[](i)] = -1;

  int len = pid_map.size();
  int* pids = new int[len];
  int* gids = new int[len];
  int* int_it = gids;
  for (std::map<int, int>::iterator it = pid_map.begin(); it != pid_map.end(); ++it)
    *int_it++ = it->first;
  int ret = vtx_map.RemoteIDList(len, gids, pids, 0);
  int_it = pids;
  for (std::map<int, int>::iterator it = pid_map.begin(); it != pid_map.end(); ++it)
     it->second = *int_it++;

  delete[] pids;
  delete[] gids;

  int* my_lids = element_nodes->Map().PointToElementList();
  std::set<int> my_element_set;
  std::set<int> export_PID_set;
  std::vector<int> export_PIDs;
  std::vector<int> export_objs;
  int num_export_IDs = 0;
  int num_remote_IDs = 0;

  len = element_nodes->MyLength();
  int* values =  element_nodes->Values();
  int count = 0;
  //let's examine the PIDList
  for (int i=0; i<len; i+=NumNodesPerElement) {
    //get actual element
    int gid = element_nodes->Map().GID(my_lids[i]);
    for (int j=0; j<NumNodesPerElement; ++j) {

      if (pid_map[values[i+j]] == comm.MyPID()) ++count;
      else export_PID_set.insert(pid_map[values[i+j]]);
    }

    if (count > 0) {     //if some nodes belong to me, I keep this element
      my_element_set.insert(gid);
    }
    if (export_PID_set.size() > 0) { //this is a remote or shared element
      export_PIDs.insert(export_PIDs.end(), export_PID_set.begin(), export_PID_set.end());
      export_objs.insert(export_objs.end(), export_PID_set.size(), gid);
    }
    count = 0;
    export_PID_set.clear();
  }

  num_export_IDs = export_PIDs.size();
  Epetra_Distributor* part_dist = comm.CreateDistributor();

  ret = part_dist->CreateFromSends(num_export_IDs, &(*(export_PIDs.begin())), true, num_remote_IDs);
  int len_import_objs = num_remote_IDs*sizeof(int);
  int* import_objs = new int[len_import_objs];
  ret = part_dist->Do( reinterpret_cast<char*>(&(*export_objs.begin())), static_cast<int>(sizeof(int)), len_import_objs, reinterpret_cast<char*&>(import_objs) );
  //add imported nodes to the new map
  int num_import_objs = 0;
  for (int i=0; i<num_remote_IDs; ++i) {
    my_element_set.insert(import_objs[num_import_objs]);
    ++num_import_objs;
  }

  delete part_dist;
  delete[] import_objs;
  num_myelements =  my_element_set.size();
  my_elements = new int[num_myelements];
  int* my_elements_it = my_elements;
  for (std::set<int>::iterator it = my_element_set.begin(); it != my_element_set.end(); ++my_elements_it)
    *my_elements_it = *it++;
  //PAT_region_end(11);
  return num_myelements;
}


int DistMesh::ElementGraph(Epetra_CrsGraph& graph) {
  const Epetra_BlockMap& elem_map = graph.Map();

  //Build ParMETIS datastructures
  // build elmdist array
  int* elmdst = new int[comm.NumProc()+1];
  int myMaxGid = elem_map.MaxMyGID() + 1;
  comm.GatherAll (&myMaxGid, elmdst+1, 1 );
  elmdst[0] = 0;

  //build eptr array
  int num_elems = ElementMap()->NumMyElements();
  int* eptr = new int[num_elems+1];
  memcpy(eptr, ElementMap()->FirstPointInElementList(),num_elems*sizeof(int));
  eptr[num_elems] = element_nodes->MyLength();

  //build eind array
  int* eind = element_nodes->Values();
  int numflag = 0;
  //common nodes is equal to 1 because every common node results in additional
  //communication
  int ncommonnodes = 1;

  MPI_Comm mpi_comm = comm.Comm();

  //output
  int* xadj;
  int* adjncy;

  //call
  ParMETIS_V3_Mesh2Dual(elmdst, eptr, eind, &numflag, &ncommonnodes, &xadj, &adjncy, &mpi_comm);

  //cleanup
  delete[] eptr;
  delete[] elmdst;

  //fill graph
  for (int i=0; i<num_elems+1; ++i) {
    int num_neighbor_s = xadj[i+1] - xadj[i];
    graph.InsertGlobalIndices(ElementMap()->GID(i), num_neighbor_s, adjncy+xadj[i]);
  }

  // Transform to local indices
  if (graph.FillComplete())
    return -1;

  // Arrange elements to build contigous memory blocks
  if (graph.OptimizeStorage())
    return -1;

 //clean up
  delete[] xadj;
  delete[] adjncy;

  return 0;
}


int DistMesh::Redistribute(bool repart, bool debug)
{
#if 1
  // Do it the vertex based way
  if (repart) {
    int* my_globals;
    int num_myglobals;
    int ret = Repartition(*coordinates, num_myglobals, my_globals, debug);
    Epetra_Map vtx_map(-1, num_myglobals, my_globals, 0, comm);
    Epetra_FECrsGraph graph(Copy, vtx_map, 64);
    ret = VertexGraph(graph);
    delete[] my_globals;
    ret = Repartition(graph, num_myglobals, my_globals, debug);
    ret = RedistributeNodes(num_myglobals, my_globals);
    delete[] my_globals;
  }
  int* my_elements;
  int num_myelements;
  CALL("DistMesh::ComputeElementEnvelopeOfNodes");
  int ret = ComputeElementEnvelopeOfNodes(coordinates->Map(), num_myelements, my_elements);
  RET("DistMesh::ComputeElementEnvelopeOfNodes", ret);
  int res = RedistributeElements(num_myelements, my_elements);
  delete[] my_elements;
    LoadBalStat(element_nodes->Map());
  return res;
#else
  // Do it the element based way
  if (repart) {
    Epetra_Map elem_map(NumElements, 0, comm);
    Epetra_CrsGraph graph = new Epetra_CrsGraph(Copy, elem_map, 0);
    ElementGraph(*graph);
    int* my_globals;
    int num_myglobals;
    Repartition(graph, num_myglobals, my_globals);
    RedistributeElements(num_myglobals, my_globals);
    delete[] my_globals;
  }
  ComputeElementEnvelopeOfElements();
#endif
}


/// erhan added
int DistMesh::RedistributePoro(bool repart, bool debug)
{
#if 1
  // Do it the vertex based way
  if (repart) {
    int* my_globals;
    int num_myglobals;
    int* my_globals_face;
    int num_myglobals_face;
    int* my_globals_element;
    int num_myglobals_element;

    int deneme;
    int ret = RepartitionPoro(*coordinates, *face_coordinates,*element_coordinates, num_myglobals, my_globals, num_myglobals_face, my_globals_face, num_myglobals_element, my_globals_element, debug);

    Epetra_Map* vtx_map = new Epetra_Map(-1, num_myglobals, my_globals, 0, comm);
    Epetra_Map* face_map = new Epetra_Map(-1, num_myglobals_face, my_globals_face, 0, comm);
    Epetra_Map* element_map = new Epetra_Map(-1, num_myglobals_element, my_globals_element, 0, comm);

    int u_weight=3;
    int f_weight=1;
    int p_weight=1;

    //create the large map.

    int size_glob_uform      =  vtx_map->NumGlobalElements();
    int size_glob_fform      =  face_map->NumGlobalElements();
    int size_glob_pform      =  element_map->NumGlobalElements();

    int size_loc_uform       =  vtx_map->NumMyElements();
    int size_loc_fform       =  face_map->NumMyElements();
    int size_loc_pform       =  element_map->NumMyElements();

    ///int  composed_glob_map_size = size_glob_uform;
    int  composed_glob_map_size = size_glob_uform + size_glob_fform + size_glob_pform;

    ///int  composed_loc_map_size  = size_loc_uform;
    int  composed_loc_map_size  = size_loc_uform  + size_loc_fform  + size_loc_pform;

    int* composed_map_indices   =  new int[composed_loc_map_size];
    int* weights   =  new int[composed_loc_map_size];


    for (int i=0; i<size_loc_uform; i++){
        composed_map_indices[i]  =  my_globals[i];
        weights[i]  =  u_weight;
        }

    for (int i=0; i<size_loc_fform; i++){
        composed_map_indices[size_loc_uform + i]  =  size_glob_uform  +  my_globals_face[i];
        weights[size_loc_uform + i]  =  f_weight;

        }

    for (int i=0; i<size_loc_pform; i++){
        composed_map_indices[ size_loc_uform + size_loc_fform + i ]
                        = size_glob_uform + size_glob_fform + my_globals_element[i];
        weights[ size_loc_uform + size_loc_fform + i ] = p_weight;
                        }

    Epetra_Map* poro_map = new Epetra_Map(composed_glob_map_size, composed_loc_map_size, composed_map_indices, 0, comm);

    Epetra_FECrsGraph graph(Copy, *poro_map, 64);
    delete poro_map;
    ret = PoroGraph(graph, num_myglobals, my_globals, num_myglobals_face, my_globals_face, num_myglobals_element, my_globals_element);
    //return 0;

    delete[] composed_map_indices;
    delete[] my_globals;
    delete[] my_globals_face;
    delete[] my_globals_element;

    int* part = new int[composed_loc_map_size];

    ///ret = Repartition(graph, num_myglobals, my_globals, debug);
    ret = RepartitionPoro(graph, composed_loc_map_size, composed_map_indices, weights, part, debug);

    delete[] weights;
    delete[] composed_map_indices;


   //for (int i=0; i< composed_loc_map_size; ++i)
    //cout<<"ID= "<<comm.MyPID()<<" i = "<<i<<" and part ="<<part[i]<<endl;

/// /////////////////////////////////////////////
    /// distribute the objects.

    std::vector<int> export_PIDs;
    std::vector<int> export_objs;
    int num_export_IDs = 0;
    int num_remote_IDs;
    int count = 0;

    int num_export_IDs_all = 0;
    int num_remote_IDs_all;
    int count_all = 0;

    for (int i=0; i<vtx_map->NumMyElements(); ++i) {
        if (part[i] != comm.MyPID()) {
            export_PIDs.push_back(part[i]);
            export_objs.push_back(vtx_map->GID(i));
        } else {
            ++count;
        }
    }

    num_export_IDs = export_PIDs.size();


    Epetra_Distributor* part_dist = comm.CreateDistributor();
    ret = part_dist->CreateFromSends(num_export_IDs,&(*(export_PIDs.begin())), true, num_remote_IDs);
    int len_import_objs = num_remote_IDs*sizeof(int);
    int* import_objs = new int[len_import_objs];
    ret = part_dist->Do( reinterpret_cast<char*>(&(*export_objs.begin())), static_cast<int>(sizeof(int)), len_import_objs, reinterpret_cast<char*&>(import_objs) );
    //PAT_region_end (6);

    num_myglobals = count+num_remote_IDs;
    my_globals = new int[num_myglobals];
    int* it = my_globals;
    //add kept nodes to the new map
    for (int i=0; i<vtx_map->NumMyElements(); ++i) {
        if (part[i] == comm.MyPID()) {
        *it++ = vtx_map->GID(i);
        }
    }

    //add imported nodes to the new map
    int num_import_objs = 0;
    for (int i=0; i<num_remote_IDs; ++i) {
        *it++ = import_objs[num_import_objs];
        ++num_import_objs;
    }

    //sort array in case we have to reread the mesh file (there the indices are sorted)
    //sort(my_globals, it);

    // clean up datastructures
    delete[] import_objs;

    num_export_IDs_all = num_export_IDs + num_export_IDs_all;
    num_remote_IDs_all = num_remote_IDs + num_remote_IDs_all;
    count_all = count + count_all;

    export_PIDs.clear();
    export_objs.clear();
    count=0;


/// ---------------------------

    for (int i=vtx_map->NumMyElements(); i<vtx_map->NumMyElements()+face_map->NumMyElements(); ++i) {
        if (part[i] != comm.MyPID()) {
            export_PIDs.push_back(part[i]);
            export_objs.push_back(face_map->GID(i-vtx_map->NumMyElements()));
        } else {
            ++count;
        }
    }


    num_export_IDs = export_PIDs.size();

    delete part_dist;
    part_dist = comm.CreateDistributor();


    ret = part_dist->CreateFromSends(num_export_IDs,&(*(export_PIDs.begin())), true, num_remote_IDs);
    len_import_objs = num_remote_IDs*sizeof(int);
    import_objs = new int[len_import_objs];
    ret = part_dist->Do( reinterpret_cast<char*>(&(*export_objs.begin())), static_cast<int>(sizeof(int)), len_import_objs, reinterpret_cast<char*&>(import_objs) );
    //PAT_region_end (6);

    num_myglobals_face = count+num_remote_IDs;
    my_globals_face = new int[num_myglobals_face];
    it = my_globals_face;
    //add kept nodes to the new map
    for (int i=vtx_map->NumMyElements(); i<vtx_map->NumMyElements()+face_map->NumMyElements(); ++i) {
        if (part[i] == comm.MyPID()) {
            *it++ = face_map->GID(i-vtx_map->NumMyElements());
        }
    }

    //add imported nodes to the new map
    num_import_objs = 0;
    for (int i=0; i<num_remote_IDs; ++i) {
        *it++ = import_objs[num_import_objs];
        ++num_import_objs;
    }

    //sort array in case we have to reread the mesh file (there the indices are sorted)
    //sort(my_globals, it);

    // clean up datastructures
    delete[] import_objs;


    num_export_IDs_all = num_export_IDs + num_export_IDs_all;
    num_remote_IDs_all = num_remote_IDs + num_remote_IDs_all;
    count_all = count + count_all;

    export_PIDs.clear();
    export_objs.clear();
    count=0;


    for (int i=vtx_map->NumMyElements()+face_map->NumMyElements(); i<vtx_map->NumMyElements()+face_map->NumMyElements()+element_map->NumMyElements(); ++i) {
        if (part[i] != comm.MyPID()) {
            export_PIDs.push_back(part[i]);
            export_objs.push_back(element_map->GID(i-vtx_map->NumMyElements()-face_map->NumMyElements()));
        } else {
            ++count;
        }
    }

    num_export_IDs = export_PIDs.size();

    delete part_dist;
    part_dist = comm.CreateDistributor();

    ret = part_dist->CreateFromSends(num_export_IDs,&(*(export_PIDs.begin())), true, num_remote_IDs);
    len_import_objs = num_remote_IDs*sizeof(int);
    import_objs = new int[len_import_objs];
    ret = part_dist->Do( reinterpret_cast<char*>(&(*export_objs.begin())), static_cast<int>(sizeof(int)), len_import_objs, reinterpret_cast<char*&>(import_objs) );

    num_myglobals_element = count+num_remote_IDs;
    my_globals_element = new int[num_myglobals_element];
    it = my_globals_element;
    //add kept nodes to the new map
    for (int i=vtx_map->NumMyElements()+face_map->NumMyElements(); i<vtx_map->NumMyElements()+face_map->NumMyElements()+element_map->NumMyElements(); ++i) {
        if (part[i] == comm.MyPID()) {
        *it++ = element_map->GID(i-vtx_map->NumMyElements()-face_map->NumMyElements());
        }
    }



  //add imported nodes to the new map
    num_import_objs = 0;
    for (int i=0; i<num_remote_IDs; ++i) {
        *it++ = import_objs[num_import_objs];
        ++num_import_objs;
    }


    //sort array in case we have to reread the mesh file (there the indices are sorted)
    //sort(my_globals, it);

    // clean up datastructures
    delete[] import_objs;


    num_export_IDs_all = num_export_IDs + num_export_IDs_all;
    num_remote_IDs_all = num_remote_IDs + num_remote_IDs_all;
    count_all = count + count_all;

    export_PIDs.clear();
    export_objs.clear();
    count=0;
/// ---------------------------

  // clean up datastructures
    delete[] part;
    delete part_dist;

    delete vtx_map;
    delete face_map;
    delete element_map;

/// /////////////////////////////////////////////

    int num_myglobals_all=count_all+num_remote_IDs_all;


    ret = RedistributeNodes(num_myglobals, my_globals);
    ret = RedistributeFaces(num_myglobals_face, my_globals_face);
    ret = RedistributeVoxels(num_myglobals_element, my_globals_element);

    Epetra_BlockMap* elemmapiso = new Epetra_BlockMap(-1, element_coordinates->Map().NumMyElements(), element_coordinates->Map().MyGlobalElements(),8, 0, comm);

    Epetra_IntVector* new_element_nodes_face = new Epetra_IntVector(*elemmapiso);
    Epetra_Export ExporterNodeFace(element_nodes_face->Map(),*elemmapiso);
    new_element_nodes_face->Export(*element_nodes_face,ExporterNodeFace,Insert);
    delete element_nodes_face;
    element_nodes_face = new_element_nodes_face;
    delete elemmapiso;

    Epetra_BlockMap* elemmapisoFace = new Epetra_BlockMap(-1, element_coordinates->Map().NumMyElements(), element_coordinates->Map().MyGlobalElements(),6, 0, comm);

    Epetra_IntVector* new_element_faces = new Epetra_IntVector(*elemmapisoFace);
    Epetra_Export ExporterFace(element_faces->Map(),*elemmapisoFace);
    new_element_faces->Export(*element_faces,ExporterFace,Insert);
    delete element_faces;
    element_faces = new_element_faces;

    Epetra_IntVector* new_element_neighbors = new Epetra_IntVector(*elemmapisoFace);
    Epetra_Export ExporterNeighhbor(element_neighbors->Map(),*elemmapisoFace);
    new_element_neighbors->Export(*element_neighbors,ExporterNeighhbor,Insert);
    delete element_neighbors;
    element_neighbors = new_element_neighbors;
    delete elemmapisoFace;


    delete[] my_globals;
    delete[] my_globals_face;
    delete[] my_globals_element;
  }
  int* my_elements;
  int num_myelements;
  CALL("DistMesh::ComputeElementEnvelopeOfNodes");
  int ret = ComputeElementEnvelopeOfNodes(coordinates->Map(), num_myelements, my_elements);
  RET("DistMesh::ComputeElementEnvelopeOfNodes", ret);



  int res = RedistributeElements(num_myelements, my_elements);


  delete[] my_elements;
    LoadBalStat(element_nodes->Map());
  return res;
#else
  // Do it the element based way
  if (repart) {
/*
    Epetra_Map elem_map(NumElements, 0, comm);
    Epetra_CrsGraph graph = new Epetra_CrsGraph(Copy, elem_map, 0);
    ElementGraph(*graph);
    int* my_globals;
    int num_myglobals;
    Repartition(graph, num_myglobals, my_globals);
    RedistributeElements(num_myglobals, my_globals);
    delete[] my_globals;
*/
  }

  ComputeElementEnvelopeOfElements();

#endif
}




int DistMesh::RedistributeElements(int num_myglobals, int* my_globals)
{
  //PAT_region_begin(10, "RedistributeElements");
  //TODO: Check:Works only if original element map has unique gids!!
  Epetra_BlockMap elementmap(-1, num_myglobals, my_globals, NumNodesPerElement, 0, comm);
  //now we have the new element-map. Remains exporting the nodes.
  Epetra_IntVector* new_elements  = new Epetra_IntVector(elementmap);
  Epetra_Import elements_importer(elementmap, element_nodes->Map());
  int ret = new_elements->Import(*element_nodes, elements_importer, Insert);
  //done. Reset pointers to new Vectors
  delete element_nodes;
  element_nodes = new_elements;
  //TODO: Check:Works only if original material id map has unique gids!!
  Epetra_Map matidmap(-1, num_myglobals, my_globals, 0, comm);
  //now we have the new material id map. Remains exporting ids
  Epetra_IntVector* new_mat_ids  = new Epetra_IntVector(matidmap);
  Epetra_Import matid_importer(matidmap, mat_ids->Map());
  ret = new_mat_ids->Import(*mat_ids, matid_importer, Insert);
  //done. Reset pointers to new Vectors
  delete mat_ids;
  mat_ids = new_mat_ids;
  //PAT_region_end(10);
  return 0;
}

int DistMesh::RedistributeNodes(int num_myglobals, int* my_globals)
{
  //PAT_region_begin (9, "RedistributeNodes");
  //TODO: Check:Works only if original node map has unique gids!!
  //Redistribute the coordinates
  Epetra_BlockMap coordmap(NumNodes, num_myglobals, my_globals, Dimension, 0, comm);
  Epetra_Vector* new_coords = new Epetra_Vector(coordmap);
  Epetra_Import coords_importer(coordmap, coordinates->Map());
  int ret = new_coords->Import(*coordinates, coords_importer, Insert);
  //done. Reset pointers to new Vectors
  delete coordinates;
  coordinates = new_coords;
  //PAT_region_end(9);
  return 0;

}

int DistMesh::RedistributeFaces(int num_myglobals, int* my_globals)
{
  //PAT_region_begin (9, "RedistributeNodes");
  //TODO: Check:Works only if original node map has unique gids!!
  //Redistribute the coordinates
  Epetra_BlockMap face_coordmap(NumFaces, num_myglobals, my_globals, Dimension, 0, comm);
  Epetra_Vector* new_face_coords = new Epetra_Vector(face_coordmap);
  Epetra_Import face_coords_importer(face_coordmap, face_coordinates->Map());
  int ret = new_face_coords->Import(*face_coordinates, face_coords_importer, Insert);
  //done. Reset pointers to new Vectors
  delete face_coordinates;
  face_coordinates = new_face_coords;
  //PAT_region_end(9);
  return 0;

}

int DistMesh::RedistributeVoxels(int num_myglobals, int* my_globals)
{
  //PAT_region_begin (9, "RedistributeNodes");
  //TODO: Check:Works only if original node map has unique gids!!
  //Redistribute the coordinates
  Epetra_BlockMap element_coordmap(NumElements, num_myglobals, my_globals, Dimension, 0, comm);
  Epetra_Vector* new_element_coords = new Epetra_Vector(element_coordmap);
  Epetra_Import element_coords_importer(element_coordmap, element_coordinates->Map());
  int ret = new_element_coords->Import(*element_coordinates, element_coords_importer, Insert);
  //done. Reset pointers to new Vectors
  delete element_coordinates;
  element_coordinates = new_element_coords;
  //PAT_region_end(9);
  return 0;

}


int DistMesh::Repartition(const Epetra_CrsGraph& graph, int& num_myglobals, int*& my_globals, bool debug)
//int DistMesh::Repartition(int& num_myglobals, int*& my_globals, bool debug)
{

  // //copy the graph map
  const Epetra_BlockMap& graph_map = graph.Map();
  //build datastructures for parmetis
  int num_indices;
  int* xadj = new int[graph.NumMyRows()+1];
  int adjacy_len = graph.NumMyNonzeros();
  int* adjacy = new int[adjacy_len];
  int ind_sum = 0;
  xadj[0] = 0;
  for (int i=0; i<graph.NumMyRows(); ++i) {
    graph.ExtractGlobalRowCopy(graph.GRID(i), adjacy_len, num_indices, adjacy + ind_sum);
    ind_sum += num_indices;
    adjacy_len -= num_indices;
    xadj[i+1] = ind_sum;
  }

  // build vtxdist array
  //int* vtxdst = new int[comm.NumProc()+1];
  //int myMaxGid = graph_map.MaxMyGID()+ 1;
  //comm.GatherAll (&myMaxGid, vtxdst+1, 1 );
  //vtxdst[0] = 0;

  return Repartition(graph_map, adjacy, xadj, num_myglobals, my_globals, debug);
}

int DistMesh::RepartitionPoro(const Epetra_CrsGraph& graph, int& num_myglobals, int*& my_globals, int*& weights, int*& part, bool debug)
//int DistMesh::Repartition(int& num_myglobals, int*& my_globals, bool debug)
{
  // //copy the graph map
  const Epetra_BlockMap& graph_map = graph.Map();
  //build datastructures for parmetis
  int num_indices;
  int* xadj = new int[graph.NumMyRows()+1];
  int adjacy_len = graph.NumMyNonzeros();
  int* adjacy = new int[adjacy_len];
  int ind_sum = 0;
  xadj[0] = 0;
  for (int i=0; i<graph.NumMyRows(); ++i) {
    graph.ExtractGlobalRowCopy(graph.GRID(i), adjacy_len, num_indices, adjacy + ind_sum);
    ind_sum += num_indices;
    adjacy_len -= num_indices;
    xadj[i+1] = ind_sum;
  }

  // build vtxdist array
  //int* vtxdst = new int[comm.NumProc()+1];
  //int myMaxGid = graph_map.MaxMyGID()+ 1;
  //comm.GatherAll (&myMaxGid, vtxdst+1, 1 );
  //vtxdst[0] = 0;
//return 0;
  return RepartitionPoro(graph_map, adjacy, xadj, num_myglobals, my_globals, weights, part,  debug);
}





int DistMesh::Repartition(const Epetra_BlockMap& graph_map, int* adjacy, int* xadj, int& num_myglobals, int*& my_globals, bool debug)
{


  //PAT_region_begin (4, "Repartition");
  int zero = 0;
  int ncon = 1;
  int nparts = comm.NumProc();
  int edgecut;
  int options[3] = {1, 0, 7};
  if (debug) {
      //options[0] = 1;
      options[1] = 7;
  }
  int* part = new int[graph_map.NumMyElements()];
  float* tpwgts = new float[nparts];
  for (int i=0; i<nparts; ++i)
      tpwgts[i] = 1.0/nparts;
 float ubvec = 1.05;
 //float* ubvec = new float[ncon];
 //ubvec[0]=1.05;
 //ubvec[1]=1.05;


  int* vtxdst = new int[comm.NumProc()+1];
  int NumMyElements = graph_map.NumMyElements();
  int myMaxGid;
  int ret = comm.ScanSum(&NumMyElements, &myMaxGid, 1);
  ret = comm.GatherAll(&myMaxGid, vtxdst+1, 1 );
  vtxdst[0] = 0;


  std::map<int, std::pair<int,int> >* pid_map = new std::map<int, std::pair<int,int> >;
   //initialize gid_map
  std::pair<int, int> a_pair(-1,-1);
   for (int i=0; i<xadj[graph_map.NumMyElements()];++i)
       pid_map->operator[](adjacy[i]) = a_pair;

   int len = pid_map->size();
   int* pids = new int[len];
   int* gids = new int[len];
   int* lids = new int[len];
   int* int_it = gids;
   for (std::map<int, std::pair<int,int> >::iterator it = pid_map->begin(); it != pid_map->end(); ++it)
       *int_it++ = it->first;

   ret = graph_map.RemoteIDList(len, gids, pids, lids);

   int* pids_it = pids;
   int* lids_it = lids;
   for (std::map<int, std::pair<int,int> >::iterator it = pid_map->begin(); it != pid_map->end(); ++it) {
       it->second.first = *pids_it++;
       it->second.second = *lids_it++;
   }

   delete[] pids;
   delete[] gids;
   delete[] lids;

   for (int j=0; j<xadj[graph_map.NumMyElements()]; ++j) {
       adjacy[j] = vtxdst[pid_map->operator[](adjacy[j]).first]+pid_map->operator[](adjacy[j]).second;
   }

   delete pid_map;

  MPI_Comm mpi_comm = comm.Comm();

  CALL("ParMETIS_V3_PartKway");
  ///*
  ParMETIS_V3_PartKway(vtxdst, xadj, adjacy, NULL, NULL,
	    &zero, &zero, &ncon, &nparts, tpwgts, &ubvec,
	    options, &edgecut, part, &mpi_comm );
  //*/
  /*
  ParMETIS_V3_PartKway(vtxdst, xadj, adjacy, NULL, NULL,
	    &zero, &zero, &ncon, &nparts, tpwgts, ubvec,
	    options, &edgecut, part, &mpi_comm );
  RET("ParMETIS_V3_PartKway", 0);
  */
  //free graph memory
  delete[] xadj;
  delete[] adjacy;
  delete[] vtxdst;
  delete[] tpwgts;

  //PAT_region_begin (6, "Redistribution in Repartition");
  //Redistribution using Epetra_Distributor
  std::vector<int> export_PIDs;
  std::vector<int> export_objs;
  int num_export_IDs = 0;
  int num_remote_IDs;
  int count = 0;
  for (int i=0; i<graph_map.NumMyElements(); ++i) {
    if (part[i] != comm.MyPID()) {
      export_PIDs.push_back(part[i]);
      export_objs.push_back(graph_map.GID(i));
    } else {
      ++count;
    }
  }

  num_export_IDs = export_PIDs.size();

  Epetra_Distributor* part_dist = comm.CreateDistributor();
  ret = part_dist->CreateFromSends(num_export_IDs,&(*(export_PIDs.begin())), true, num_remote_IDs);

  int len_import_objs = num_remote_IDs*sizeof(int);
  int* import_objs = new int[len_import_objs];

  ret = part_dist->Do( reinterpret_cast<char*>(&(*export_objs.begin())), static_cast<int>(sizeof(int)), len_import_objs, reinterpret_cast<char*&>(import_objs) );
  //PAT_region_end (6);

  num_myglobals = count+num_remote_IDs;
  my_globals = new int[num_myglobals];
  int* it = my_globals;
  //add kept nodes to the new map
  for (int i=0; i<graph_map.NumMyElements(); ++i) {
    if (part[i] == comm.MyPID()) {
      *it++ = graph_map.GID(i);
    }
  }

  //add imported nodes to the new map
  int num_import_objs = 0;
  for (int i=0; i<num_remote_IDs; ++i) {
    *it++ = import_objs[num_import_objs];
    ++num_import_objs;
  }

  //sort array in case we have to reread the mesh file (there the indices are sorted)
  //sort(my_globals, it);

  // clean up datastructures
  delete[] part;
  delete[] import_objs;
  delete part_dist;
  //PAT_region_end(4);
  return num_myglobals;
}

int DistMesh::RepartitionPoro(const Epetra_BlockMap& graph_map, int* adjacy, int* xadj, int& num_myglobals, int*& my_globals, int*& weights, int*& part, bool debug)
{


  //PAT_region_begin (4, "Repartition");
  int zero = 0;int two = 2;
  int ncon = 1;
  int nparts = comm.NumProc();
  int edgecut;
  int options[3] = {1, 0, 7};
  if (debug) {
      //options[0] = 1;
      options[1] = 7;
  }
  ///int* part = new int[graph_map.NumMyElements()];
  float* tpwgts = new float[nparts];
  for (int i=0; i<nparts; ++i)
      tpwgts[i] = 1.0/nparts;
  float ubvec = 1.05f;

  int* vtxdst = new int[comm.NumProc()+1];
  int NumMyElements = graph_map.NumMyElements();
  int myMaxGid;
  int ret = comm.ScanSum(&NumMyElements, &myMaxGid, 1);
  ret = comm.GatherAll(&myMaxGid, vtxdst+1, 1 );
  vtxdst[0] = 0;

  std::map<int, std::pair<int,int> >* pid_map = new std::map<int, std::pair<int,int> >;
   //initialize gid_map
  std::pair<int, int> a_pair(-1,-1);
   for (int i=0; i<xadj[graph_map.NumMyElements()];++i)
       pid_map->operator[](adjacy[i]) = a_pair;

   int len = pid_map->size();
   int* pids = new int[len];
   int* gids = new int[len];
   int* lids = new int[len];
   int* int_it = gids;
   for (std::map<int, std::pair<int,int> >::iterator it = pid_map->begin(); it != pid_map->end(); ++it)
       *int_it++ = it->first;

   ret = graph_map.RemoteIDList(len, gids, pids, lids); /// asks where the global ids (gids) are located
                                                        /// returns the processors pids and local id (lid)
                                                        /// at that lids.

   int* pids_it = pids;
   int* lids_it = lids;
   for (std::map<int, std::pair<int,int> >::iterator it = pid_map->begin(); it != pid_map->end(); ++it) {
       it->second.first = *pids_it++;
       it->second.second = *lids_it++;
   }

   delete[] pids;
   delete[] gids;
   delete[] lids;


   for (int j=0; j<xadj[graph_map.NumMyElements()]; ++j) {
       adjacy[j] = vtxdst[pid_map->operator[](adjacy[j]).first]+pid_map->operator[](adjacy[j]).second;
   }


   delete pid_map;

  MPI_Comm mpi_comm = comm.Comm();

  CALL("ParMETIS_V3_PartKway");
/*
  ParMETIS_V3_PartKway(vtxdst, xadj, adjacy, NULL, NULL,
	    &zero, &zero, &ncon, &nparts, tpwgts, &ubvec,
	    options, &edgecut, part, &mpi_comm );
*/
/// weighted PartKway.
  ParMETIS_V3_PartKway(vtxdst, xadj, adjacy, weights, NULL,
	    &two, &zero, &ncon, &nparts, tpwgts, &ubvec,
	    options, &edgecut, part, &mpi_comm );
  RET("ParMETIS_V3_PartKway", 0);
  //free graph memory
  delete[] xadj;
  delete[] adjacy;
  delete[] vtxdst;
  delete[] tpwgts;

  //PAT_region_begin (6, "Redistribution in Repartition");
  //Redistribution using Epetra_Distributor
  std::vector<int> export_PIDs;
  std::vector<int> export_objs;
  int num_export_IDs = 0;
  int num_remote_IDs;
  int count = 0;
  for (int i=0; i<graph_map.NumMyElements(); ++i) {
    if (part[i] != comm.MyPID()) {
      export_PIDs.push_back(part[i]);
      export_objs.push_back(graph_map.GID(i));
    } else {
      ++count;
    }
  }

  num_export_IDs = export_PIDs.size();

  Epetra_Distributor* part_dist = comm.CreateDistributor();
  ret = part_dist->CreateFromSends(num_export_IDs,&(*(export_PIDs.begin())), true, num_remote_IDs);

  int len_import_objs = num_remote_IDs*sizeof(int);
  int* import_objs = new int[len_import_objs];

  ret = part_dist->Do( reinterpret_cast<char*>(&(*export_objs.begin())), static_cast<int>(sizeof(int)), len_import_objs, reinterpret_cast<char*&>(import_objs) );
  //PAT_region_end (6);

  num_myglobals = count+num_remote_IDs;
  my_globals = new int[num_myglobals];
  int* it = my_globals;
  //add kept nodes to the new map
  for (int i=0; i<graph_map.NumMyElements(); ++i) {
    if (part[i] == comm.MyPID()) {
      *it++ = graph_map.GID(i);
    }
  }

  //add imported nodes to the new map
  int num_import_objs = 0;
  for (int i=0; i<num_remote_IDs; ++i) {
    *it++ = import_objs[num_import_objs];
    ++num_import_objs;
  }

  //sort array in case we have to reread the mesh file (there the indices are sorted)
  //sort(my_globals, it);

  // clean up datastructures
  ///delete[] part;
  delete[] import_objs;
  delete part_dist;
  //PAT_region_end(4);
  return num_myglobals;
}






int DistMesh::Repartition(const Epetra_Vector& coord_vec, int& num_myglobals, int*& my_globals, bool debug)
{

  //PAT_region_begin (4, "Repartition");
  // build vtxdist array
  const Epetra_BlockMap& coord_map = coord_vec.Map(); /// coord_vec map is created in scan func. number of nodes (* 3)
  int* vtxdst = new int[comm.NumProc()+1]; /// a dynamic vector of size total number processors + 1
  int myMaxGid = coord_map.MaxMyGID()+ 1; /// myMaxGid is the maximum global id at that proc - FS.
  int ret = comm.GatherAll (&myMaxGid, vtxdst+1, 1 ); /// gather at proc 0  - I guess.
                                                    /// here each proc collects maxGID of all procs.
  vtxdst[0] = 0;
  int ndims = Dimension;

  float* xyz = new float[coord_vec.MyLength()]; /// total number of coordinates. i.e number of nodes at that proc * 3
  for (int i=0; i< coord_vec.MyLength(); ++i)
    xyz[i] = float(coord_vec.operator[](i)); /// fill the vector
  int* part = new int[coord_map.NumMyElements()];
  MPI_Comm mpi_comm = comm.Comm();
  CALL("ParMETIS_V3_PartGeom");
  ParMETIS_V3_PartGeom (vtxdst, &ndims, xyz, part, &mpi_comm);
  RET("ParMETIS_V3_PartGeom", 0);
  delete[] vtxdst;
  delete[] xyz;

  //Redistribution using Epetra_Distributor
  std::vector<int> export_PIDs;
  std::vector<int> export_objs;
  int num_export_IDs = 0;
  int num_remote_IDs;
  int count = 0;
  for (int i=0; i<coord_map.NumMyElements(); ++i) {
    if (part[i] != comm.MyPID()) {
      export_PIDs.push_back(part[i]);
      export_objs.push_back(coord_map.GID(i));
    } else {
      ++count;
    }
  }

  num_export_IDs = export_PIDs.size();

  Epetra_Distributor* part_dist = comm.CreateDistributor();
  ret = part_dist->CreateFromSends(num_export_IDs,&(*(export_PIDs.begin())), true, num_remote_IDs);
  int len_import_objs = num_remote_IDs*sizeof(int);
  int* import_objs = new int[len_import_objs];
  ret = part_dist->Do( reinterpret_cast<char*>(&(*export_objs.begin())), static_cast<int>(sizeof(int)), len_import_objs, reinterpret_cast<char*&>(import_objs) );
  //PAT_region_end (6);

  num_myglobals = count+num_remote_IDs;
  my_globals = new int[num_myglobals];
  int* it = my_globals;
  //add kept nodes to the new map
  for (int i=0; i<coord_map.NumMyElements(); ++i) {
    if (part[i] == comm.MyPID()) {
      *it++ = coord_map.GID(i);
    }
  }

  //add imported nodes to the new map
  int num_import_objs = 0;
  for (int i=0; i<num_remote_IDs; ++i) {
    *it++ = import_objs[num_import_objs];
    ++num_import_objs;
  }

  //sort array in case we have to reread the mesh file (there the indices are sorted)
  //sort(my_globals, it);

  // clean up datastructures
  delete[] part;
  delete[] import_objs;
  delete part_dist;
  //PAT_region_end(4);
  return num_myglobals;
}

int DistMesh::RepartitionPoro(const Epetra_Vector& coord_vec, const Epetra_Vector& face_coord_vec, const Epetra_Vector& element_coord_vec, int& num_myglobals, int*& my_globals, int& num_myglobals_face, int*& my_globals_face, int& num_myglobals_element, int*& my_globals_element, bool debug)
{

  //PAT_region_begin (4, "Repartition");
  // build vtxdist array
  const Epetra_BlockMap& coord_map = coord_vec.Map(); /// coord_vec map is created in scan func. number of nodes (* 3)
  const Epetra_BlockMap& face_coord_map = face_coord_vec.Map(); /// coord_vec map is created in scan func. number of nodes (* 3)
  const Epetra_BlockMap& element_coord_map = element_coord_vec.Map(); /// coord_vec map is created in scan func. number of nodes (* 3)


  int* vtxdst = new int[comm.NumProc()+1]; /// a dynamic vector of size total number processors + 1
  int myMaxGid = coord_map.MaxMyGID()+ 1; /// myMaxGid is the maximum global id at that proc - FS.
  int ret = comm.GatherAll (&myMaxGid, vtxdst+1, 1 ); /// here each proc collects maxGID of all procs.


  int* vtxdst_face = new int[comm.NumProc()+1]; /// a dynamic vector of size total number processors + 1
  int myMaxGid_face = face_coord_map.MaxMyGID()+ 1; /// myMaxGid is the maximum global id at that proc - FS.
  ret = comm.GatherAll (&myMaxGid_face, vtxdst_face+1, 1 ); /// here each proc collects maxGID of all procs.

  int* vtxdst_element = new int[comm.NumProc()+1]; /// a dynamic vector of size total number processors + 1
  int myMaxGid_element = element_coord_map.MaxMyGID()+ 1; /// myMaxGid is the maximum global id at that proc - FS.
  ret = comm.GatherAll (&myMaxGid_element, vtxdst_element+1, 1 ); /// here each proc collects maxGID of all procs.


  vtxdst[0] = 0;
  vtxdst_face[0] = 0;
  vtxdst_element[0] = 0;

  int ndims = Dimension;

  int* vtxdst_all = new int[comm.NumProc()+1]; /// a dynamic vector of size total number processors + 1

  for(int i=0;i<comm.NumProc()+1;i++)
    vtxdst_all[i]=vtxdst[i]+vtxdst_face[i]+vtxdst_element[i];

  int xyz_length;
  xyz_length = coord_vec.MyLength()+face_coord_vec.MyLength()+element_coord_vec.MyLength();

  /// local number of all coordinates
  float* xyz = new float[xyz_length];
  /// fill the vector
  for (int i=0; i< coord_vec.MyLength(); ++i)
    xyz[i] = float(coord_vec.operator[](i));
  for (int i=0; i< face_coord_vec.MyLength(); ++i)
    xyz[i+coord_vec.MyLength()] = float(face_coord_vec.operator[](i));
  for (int i=0; i< element_coord_vec.MyLength(); ++i)
    xyz[i+coord_vec.MyLength()+face_coord_vec.MyLength()] = float(element_coord_vec.operator[](i));


  int part_size;
  part_size = coord_map.NumMyElements()+face_coord_map.NumMyElements()+element_coord_map.NumMyElements();

  int* part = new int[part_size];

  MPI_Comm mpi_comm = comm.Comm();
  CALL("ParMETIS_V3_PartGeom");
  ParMETIS_V3_PartGeom (vtxdst_all, &ndims, xyz, part, &mpi_comm);
  RET("ParMETIS_V3_PartGeom", 0);
  delete[] vtxdst_all;
  delete[] xyz;

  /// all export objects could normally be collected in single vectors to perform a global export list
  /// however, that will allow one map for u+f+p unknowns but we need separate for each variable.
  /// creation of a unified map from those three is straightforward - other way around is not.
  /// that's why the same operation is repated three times instead meaning three distributor use are considered
  //Redistribution using Epetra_Distributor
  std::vector<int> export_PIDs;
  std::vector<int> export_objs;
  int num_export_IDs = 0;
  int num_remote_IDs;
  int count = 0;

  int num_export_IDs_all = 0;
  int num_remote_IDs_all;
  int count_all = 0;


  // redistribute nodes

  for (int i=0; i<coord_map.NumMyElements(); ++i) {
    if (part[i] != comm.MyPID()) {
      export_PIDs.push_back(part[i]);
      export_objs.push_back(coord_map.GID(i));
    } else {
      ++count;
    }
  }


  num_export_IDs = export_PIDs.size();

  Epetra_Distributor* part_dist = comm.CreateDistributor();
  ret = part_dist->CreateFromSends(num_export_IDs,&(*(export_PIDs.begin())), true, num_remote_IDs);
  int len_import_objs = num_remote_IDs*sizeof(int);
  int* import_objs = new int[len_import_objs];
  ret = part_dist->Do( reinterpret_cast<char*>(&(*export_objs.begin())), static_cast<int>(sizeof(int)), len_import_objs, reinterpret_cast<char*&>(import_objs) );
  //PAT_region_end (6);

  num_myglobals = count+num_remote_IDs;
  my_globals = new int[num_myglobals];
  int* it = my_globals;
  //add kept nodes to the new map
  for (int i=0; i<coord_map.NumMyElements(); ++i) {
    if (part[i] == comm.MyPID()) {
      *it++ = coord_map.GID(i);
    }
  }

  //add imported nodes to the new map
  int num_import_objs = 0;
  for (int i=0; i<num_remote_IDs; ++i) {
    *it++ = import_objs[num_import_objs];
    ++num_import_objs;
  }

  //sort array in case we have to reread the mesh file (there the indices are sorted)
  //sort(my_globals, it);

  // clean up datastructures
  delete[] import_objs;
  //delete part_dist;
  //PAT_region_end(4);


  num_export_IDs_all = num_export_IDs + num_export_IDs_all;
  num_remote_IDs_all = num_remote_IDs + num_remote_IDs_all;
  count_all = count + count_all;

  export_PIDs.clear();
  export_objs.clear();
  count=0;


  // redistribute faces

  for (int i=coord_map.NumMyElements(); i<coord_map.NumMyElements()+face_coord_map.NumMyElements(); ++i) {
    if (part[i] != comm.MyPID()) {
      export_PIDs.push_back(part[i]);
      export_objs.push_back(face_coord_map.GID(i-coord_map.NumMyElements()));
    } else {
      ++count;
    }
  }


  num_export_IDs = export_PIDs.size();

  delete part_dist;
  part_dist = comm.CreateDistributor();

  ret = part_dist->CreateFromSends(num_export_IDs,&(*(export_PIDs.begin())), true, num_remote_IDs);
  len_import_objs = num_remote_IDs*sizeof(int);
  import_objs = new int[len_import_objs];
  ret = part_dist->Do( reinterpret_cast<char*>(&(*export_objs.begin())), static_cast<int>(sizeof(int)), len_import_objs, reinterpret_cast<char*&>(import_objs) );
  //PAT_region_end (6);

  num_myglobals_face = count+num_remote_IDs;
  my_globals_face = new int[num_myglobals_face];
  it = my_globals_face;
  //add kept nodes to the new map
  for (int i=coord_map.NumMyElements(); i<coord_map.NumMyElements()+face_coord_map.NumMyElements(); ++i) {
    if (part[i] == comm.MyPID()) {
      *it++ = face_coord_map.GID(i-coord_map.NumMyElements());
    }
  }

  //add imported nodes to the new map
  num_import_objs = 0;
  for (int i=0; i<num_remote_IDs; ++i) {
    *it++ = import_objs[num_import_objs];
    ++num_import_objs;
  }

  //sort array in case we have to reread the mesh file (there the indices are sorted)
  //sort(my_globals, it);

  // clean up datastructures
  delete[] import_objs;

  //PAT_region_end(4);


  num_export_IDs_all = num_export_IDs + num_export_IDs_all;
  num_remote_IDs_all = num_remote_IDs + num_remote_IDs_all;
  count_all = count + count_all;

  export_PIDs.clear();
  export_objs.clear();
  count=0;

  // redistribute elements

  for (int i=coord_map.NumMyElements()+face_coord_map.NumMyElements(); i<coord_map.NumMyElements()+face_coord_map.NumMyElements()+element_coord_map.NumMyElements(); ++i) {
    if (part[i] != comm.MyPID()) {
      export_PIDs.push_back(part[i]);
      export_objs.push_back(element_coord_map.GID(i-coord_map.NumMyElements()-face_coord_map.NumMyElements()));
    } else {
      ++count;
    }
  }


  num_export_IDs = export_PIDs.size();

  delete part_dist;
  part_dist = comm.CreateDistributor();

  ret = part_dist->CreateFromSends(num_export_IDs,&(*(export_PIDs.begin())), true, num_remote_IDs);
  len_import_objs = num_remote_IDs*sizeof(int);
  import_objs = new int[len_import_objs];
  ret = part_dist->Do( reinterpret_cast<char*>(&(*export_objs.begin())), static_cast<int>(sizeof(int)), len_import_objs, reinterpret_cast<char*&>(import_objs) );
  //PAT_region_end (6);

  num_myglobals_element = count+num_remote_IDs;
  my_globals_element = new int[num_myglobals_element];
  it = my_globals_element;
  //add kept nodes to the new map
  for (int i=coord_map.NumMyElements()+face_coord_map.NumMyElements(); i<coord_map.NumMyElements()+face_coord_map.NumMyElements()+element_coord_map.NumMyElements(); ++i) {
    if (part[i] == comm.MyPID()) {
      *it++ = element_coord_map.GID(i-coord_map.NumMyElements()-face_coord_map.NumMyElements());
    }
  }

  //add imported nodes to the new map
  num_import_objs = 0;
  for (int i=0; i<num_remote_IDs; ++i) {
    *it++ = import_objs[num_import_objs];
    ++num_import_objs;
  }


  //sort array in case we have to reread the mesh file (there the indices are sorted)
  //sort(my_globals, it);

  // clean up datastructures
  delete[] import_objs;

  //PAT_region_end(4);


  num_export_IDs_all = num_export_IDs + num_export_IDs_all;
  num_remote_IDs_all = num_remote_IDs + num_remote_IDs_all;
  count_all = count + count_all;

  export_PIDs.clear();
  export_objs.clear();
  count=0;


  // clean up datastructures
  delete[] part;
  delete part_dist;

  int num_myglobals_all=count_all+num_remote_IDs_all;

  return num_myglobals;
}

/// get the maps.

/// get the unique node partition
const Epetra_BlockMap* DistMesh::NodeMap()
{
  return &coordinates->Map();
}

/// get the element map based on the nodes
const Epetra_BlockMap* DistMesh::ElementMap()
{
  return &element_nodes->Map();
}

/// get the unique face partition
const Epetra_BlockMap* DistMesh::FaceMap()
{

  return &face_coordinates->Map();

}

/// get the element map based on the faces
const Epetra_BlockMap* DistMesh::ElementFaceMap() /// erhan added
{
  return &element_faces->Map();
}


/// get the unique element partition
const Epetra_BlockMap* DistMesh::SchurMap()
{

  return &element_coordinates->Map();
}


/// get the element map based on the pressure
const Epetra_BlockMap* DistMesh::NeighborMap() /// erhan added
{
  return &element_neighbors->Map();
}



/// get the tables

Epetra_IntVector* DistMesh::ElementNodes()
{
  return element_nodes;
}

Epetra_IntVector* DistMesh::ElementNodesFace()
{
  return element_nodes_face;
}

Epetra_IntVector* DistMesh::MaterialIDs()
{
  return mat_ids;
}


Epetra_Vector* DistMesh::Coordinates()
{
  return coordinates;
}

Epetra_Vector* DistMesh::FaceCoordinates()
{
  return face_coordinates;
}

Epetra_Vector* DistMesh::ElementCoordinates()
{
  return element_coordinates;
}


Epetra_IntVector* DistMesh::ElementFaces()  /// erhan added
{
  return element_faces;
}


Epetra_IntVector* DistMesh::ElementNeighbors()  /// erhan added
{
  return element_neighbors;
}


Epetra_IntVector* DistMesh::FixedFaces()  /// erhan added
{
  return fixed_faces;
}

Epetra_Vector* DistMesh::FixedFaceValues()  /// erhan added
{
  return fixed_face_values;
}



int DistMesh::ReferenceElement()
{
  int* values = element_nodes->Values();
  int len = element_nodes->MyLength();
  int mine = true;
  for (int i=0; i<len; i+=NumNodesPerElement) {
    int* elem_values = values+i;
    for (int j=0; j<NumNodesPerElement; ++j) {
      if (!coordinates->Map().MyGID(elem_values[j]))
	mine = false;
    }
    if (mine)
      return element_nodes->Map().PointToElementList()[i];
    mine = true;
  }
  return -1;
}


void DistMesh::Print(MeshWriter* mw) {
    mw->PrintCoordinates(*coordinates);
    mw->PrintElements(*element_nodes);
}

void DistMesh::Print(ProblemWriter* pw) {
    pw->PrintCoordinates(*coordinates);
    pw->PrintElements(*element_nodes);

    pw->PrintFaces(*element_faces);  ///erhan added

    pw->PrintNeighbors(*element_neighbors); ///erhan added

    //pw->PrintFaceCoordinates(*coordinates); ///erhan added
    pw->PrintFaceCoordinates(*face_coordinates); ///erhan added

    //pw->PrintElementCoordinates(*coordinates); ///erhan added
    pw->PrintElementCoordinates(*element_coordinates); ///erhan added

    pw->PrintFixedFaces(*fixed_faces); ///erhan added

    pw->PrintFixedFaceValues(*fixed_face_values); ///erhan added


    if (MaterialIDMap.size() > 1) {
      std::vector<int> material_ids(MaterialIDMap.size());
      for (std::map<int, int>::const_iterator it = MaterialIDMap.begin(); it !=  MaterialIDMap.end(); ++it)
	material_ids[(*it).second] = (*it).first;

      for (int i=0; i<mat_ids->MyLength(); ++i)
	mat_ids->operator[](i) = material_ids[mat_ids->operator[](i)];
      pw->PrintMaterialIDs(*mat_ids);
    }
}

void DistMesh::Scan(ProblemReader* pr) {

    Epetra_BlockMap coordmap(NumNodes, Dimension, 0, comm);
    coordinates = new Epetra_Vector(coordmap);
    pr->ScanCoordinates(*coordinates);

    Epetra_BlockMap elemmap(NumElements, NumNodesPerElement, 0, comm);
    element_nodes = new Epetra_IntVector(elemmap);
    pr->ScanElements(*element_nodes);

    element_nodes_face = new Epetra_IntVector(*element_nodes);

    ///Epetra_BlockMap facemap(NumElements, NumFacesPerElement, 0, comm);
    /// changed DistMesh constructor so NumFacesPerElement can be initialized.

    Epetra_BlockMap facemap(NumElements, 6, 0, comm);
    element_faces = new Epetra_IntVector(facemap);
    pr->ScanFaces(*element_faces);


    Epetra_BlockMap neighmap(NumElements, 6, 0, comm);
    element_neighbors = new Epetra_IntVector(neighmap);
    pr->ScanNeighbors(*element_neighbors);


    //Epetra_BlockMap fixedfcsmap(NumFixedFaces, 1, 0, comm);
    //fixed_faces = new Epetra_IntVector(fixedfcsmap);
    //pr->ScanFixedFaces(*fixed_faces);
    pr->ScanFixedFaces(fixed_faces);

    //cout<<*fixed_faces<<endl;


    Epetra_BlockMap fixedfcsvalmap(NumFixedFaces, 1, 0, comm);
    fixed_face_values = new Epetra_Vector(fixedfcsvalmap);
    pr->ScanFixedFaceValues(*fixed_face_values);


    Epetra_BlockMap facecoordmap(NumFaces, Dimension, 0, comm);
    face_coordinates = new Epetra_Vector(facecoordmap);
    pr->ScanFaceCoordinates(*face_coordinates);

    Epetra_BlockMap elementcoordmap(NumElements, Dimension, 0, comm);
    element_coordinates = new Epetra_Vector(elementcoordmap);
    pr->ScanElementCoordinates(*element_coordinates);



    Epetra_Map matidmap(NumElements, 0, comm);
    mat_ids = new Epetra_IntVector(matidmap);
    if (MaterialIDMap.size() > 1) {
      pr->ScanMaterialIDs(*mat_ids);
      for (int i=0; i<mat_ids->MyLength(); ++i)
	mat_ids->operator[](i) = MaterialIDMap[mat_ids->operator[](i)];
    }
}

int DistMesh::RedistributeWIsorropia(bool repart, bool debug)
{
    int ret = 0;

    if (repart) {

        Epetra_BlockMap map1d(NumNodes, coordinates->Map().NumMyElements(), 1, 0, comm);
        Epetra_MultiVector *coords = new Epetra_MultiVector(map1d, 3, false);

        //convert Epetra_Vector to Epetra_MultiVector
        for (int i=0; i<coordinates->Map().NumMyElements(); i++){
            coords->operator()(0)->operator[](i) = coordinates->operator[](3*i);
            coords->operator()(1)->operator[](i) = coordinates->operator[](3*i+1);
            coords->operator()(2)->operator[](i) = coordinates->operator[](3*i+2);
        }

        Teuchos::ParameterList paramlist;
        paramlist.set("Partitioning Method", "RCB");
        Teuchos::ParameterList &sublist = paramlist.sublist("Zoltan");
        sublist.set("RCB_RECTILINEAR_BLOCKS", "1");
        sublist.set("DEBUG_LEVEL", "0");

        Teuchos::RCP<const Epetra_MultiVector> coord = Teuchos::rcp(coords);
        Teuchos::RCP<Isorropia::Epetra::Partitioner> part = Teuchos::rcp(new Isorropia::Epetra::Partitioner(coord, paramlist));
        Isorropia::Epetra::Redistributor rd(part);
        Teuchos::RCP<Epetra_MultiVector> new_coord = rd.redistribute(*coord);

        ret = RedistributeNodes(new_coord->Map().NumMyElements(), new_coord->Map().MyGlobalElements());

        //FIXME: actually we have all information we need in the MultiVector.
        //is the following code replacing the RedistributeNodes stuff
        //delete coordinates;
        //coordinates = new Epetra_Vector(Copy, *new_coord, 0);

        //delete coords;

    }

    //FIXME: evt use redistributor
    int* my_elements;
    int num_myelements;
    CALL("DistMesh::ComputeElementEnvelopeOfNodes");
    ret = ComputeElementEnvelopeOfNodes(coordinates->Map(), num_myelements, my_elements);
    RET("DistMesh::ComputeElementEnvelopeOfNodes", ret);
    int res = RedistributeElements(num_myelements, my_elements);
    delete[] my_elements;
    LoadBalStat(element_nodes->Map());
    return res;
}



int DistMesh::RedistributeWIsorropiaElements(bool repart, bool debug)
{
    int ret = 0;

    if (repart) {

        Epetra_BlockMap map1d(NumElements, element_coordinates->Map().NumMyElements(), 1, 0, comm);
        Epetra_MultiVector *elem_coords = new Epetra_MultiVector(map1d, 3, false);

        //convert Epetra_Vector to Epetra_MultiVector
        for (int i=0; i<element_coordinates->Map().NumMyElements(); i++){
            elem_coords->operator()(0)->operator[](i) = element_coordinates->operator[](3*i);
            elem_coords->operator()(1)->operator[](i) = element_coordinates->operator[](3*i+1);
            elem_coords->operator()(2)->operator[](i) = element_coordinates->operator[](3*i+2);
        }

        Teuchos::ParameterList paramlist;
        paramlist.set("Partitioning Method", "RCB");
        Teuchos::ParameterList &sublist = paramlist.sublist("Zoltan");
        //sublist.set("RCB_RECTILINEAR_BLOCKS", "1");
        //sublist.set("DEBUG_LEVEL", "0");
        sublist.set("LB_METHOD", "Parmetis");

        Teuchos::RCP<const Epetra_MultiVector> coord = Teuchos::rcp(elem_coords);
        Teuchos::RCP<Isorropia::Epetra::Partitioner> part = Teuchos::rcp(new Isorropia::Epetra::Partitioner(coord, paramlist));
        Isorropia::Epetra::Redistributor rd(part);
        Teuchos::RCP<Epetra_MultiVector> new_coord = rd.redistribute(*coord);

        ret = RedistributeVoxels(new_coord->Map().NumMyElements(), new_coord->Map().MyGlobalElements());

        Epetra_BlockMap* elemmapiso = new Epetra_BlockMap(-1, element_coordinates->Map().NumMyElements(), element_coordinates->Map().MyGlobalElements(),8, 0, comm);

        Epetra_IntVector* new_element_nodes_face = new Epetra_IntVector(*elemmapiso);
        Epetra_Export ExporterNodeFace(element_nodes_face->Map(),*elemmapiso);
        new_element_nodes_face->Export(*element_nodes_face,ExporterNodeFace,Insert);
        delete element_nodes_face;
        element_nodes_face = new_element_nodes_face;
        delete elemmapiso;

        Epetra_BlockMap* elemmapisoFace = new Epetra_BlockMap(-1, element_coordinates->Map().NumMyElements(), element_coordinates->Map().MyGlobalElements(),6, 0, comm);

        Epetra_IntVector* new_element_faces = new Epetra_IntVector(*elemmapisoFace);
        Epetra_Export ExporterFace(element_faces->Map(),*elemmapisoFace);
        new_element_faces->Export(*element_faces,ExporterFace,Insert);
        delete element_faces;
        element_faces = new_element_faces;

        Epetra_IntVector* new_element_neighbors = new Epetra_IntVector(*elemmapisoFace);
        Epetra_Export ExporterNeighhbor(element_neighbors->Map(),*elemmapisoFace);
        new_element_neighbors->Export(*element_neighbors,ExporterNeighhbor,Insert);
        delete element_neighbors;
        element_neighbors = new_element_neighbors;
        delete elemmapisoFace;

    }


/*
    //FIXME: evt use redistributor
    int* my_elements;
    int num_myelements;
    CALL("DistMesh::ComputeElementEnvelopeOfNodes");
    ret = ComputeElementEnvelopeOfNodes(element_coordinates->Map(), num_myelements, my_elements);
    RET("DistMesh::ComputeElementEnvelopeOfNodes", ret);
    int res = RedistributeElements(num_myelements, my_elements);
    delete[] my_elements;
    LoadBalStat(element_nodes->Map());
    return res;
*/
}



int DistMesh::RedistributeWIsorropiaFaces(bool repart, bool debug)
{
    int ret = 0;

    if (repart) {

        Epetra_BlockMap map1d(NumFaces, face_coordinates->Map().NumMyElements(), 1, 0, comm);
        Epetra_MultiVector *face_coords = new Epetra_MultiVector(map1d, 3, false);

        //convert Epetra_Vector to Epetra_MultiVector
        for (int i=0; i<face_coordinates->Map().NumMyElements(); i++){
            face_coords->operator()(0)->operator[](i) = face_coordinates->operator[](3*i);
            face_coords->operator()(1)->operator[](i) = face_coordinates->operator[](3*i+1);
            face_coords->operator()(2)->operator[](i) = face_coordinates->operator[](3*i+2);
        }

        Teuchos::ParameterList paramlist;
        paramlist.set("Partitioning Method", "RCB");
        Teuchos::ParameterList &sublist = paramlist.sublist("Zoltan");
        //sublist.set("RCB_RECTILINEAR_BLOCKS", "1");
        //sublist.set("DEBUG_LEVEL", "0");
        sublist.set("LB_METHOD", "Parmetis");

        Teuchos::RCP<const Epetra_MultiVector> coord = Teuchos::rcp(face_coords);
        Teuchos::RCP<Isorropia::Epetra::Partitioner> part = Teuchos::rcp(new Isorropia::Epetra::Partitioner(coord, paramlist));
        Isorropia::Epetra::Redistributor rd(part);
        Teuchos::RCP<Epetra_MultiVector> new_coord = rd.redistribute(*coord);

        ret = RedistributeFaces(new_coord->Map().NumMyElements(), new_coord->Map().MyGlobalElements());

    }



}


void DistMesh::LoadBalStat(Epetra_BlockMap map)
{
    //compute some statistic about load balancing
    int localw = map.NumMyElements();
    double globalw = map.NumGlobalElements() * 1.0/comm.NumProc();
    double imbalance = 1.0;
    if (localw >= globalw)
        imbalance += (localw-globalw)/globalw;
    else
        imbalance += (globalw-localw)/globalw;

    double max, min, avg;
    comm.MaxAll(&imbalance, &max, 1);
    comm.MinAll(&imbalance, &min, 1);
    comm.SumAll(&imbalance, &avg, 1);

    avg /= comm.NumProc();

    ///if(comm.MyPID() == 0) cout << "min = " << min << ", max = " << max << ", avg = " << avg << endl;

}
