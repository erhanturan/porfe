/*
 * PorFE: a poroelastic extension on ParFE
 * Copyright (C) 2012, Erhan Turan
 * https://bitbucket.org/erhanturan/porfe
 *
 * ParFE: a micro-FE solver for trabecular bone modeling
 * Copyright (C) 2011,  ParFE developers, see
 * http://parfe.sourceforge.net/developers.php
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#include <Epetra_FECrsGraph.h>
#include <Epetra_MultiVector.h>
#include "FluxProblem.h"
#include <Epetra_SerialDenseMatrix.h>
#include <Epetra_IntSerialDenseVector.h>

#include <set>
#include <vector>

#include "MATLAB_VectorWriter.hpp"

FluxProblem::FluxProblem(DistMesh& a_mesh, Teuchos::ParameterList& fe_param, double a_deltaT, int poption_)
  : ElasticityProblem(a_mesh, fe_param)
{

  NumFacesPerElement=6;
  poption = poption_;
  fluxMap = new Epetra_Map(-1, mesh.FaceMap()->NumMyElements(), mesh.FaceMap()->MyGlobalElements(), 0, comm);

  Epetra_Vector* X = new Epetra_Vector(*fluxMap);
  Epetra_Vector* B = new Epetra_Vector(*fluxMap);
  ///SetOperator(flux_mat);
  SetLHS(X);
  SetRHS(B);


  InitializeMaterialProperties();
  deltaT = a_deltaT;
  Assemble();
}

FluxProblem::~FluxProblem()
{
  delete GetLHS();
  delete GetRHS();
  delete GetOperator();
}



int FluxProblem::Assemble()
{

   // Schur App - BF in poisson matrix format.
   // Se*a^3 - (mu/k)^-1 * 15 * a * alpha


  double BfTerm = (viscosity/permeability)*EdgeLength/6.0;
  //double BfTerm = (viscosity/permeability)/(EdgeLength*6.0);
  //double BfTerm = (viscosity/permeability)*EdgeLength*EdgeLength/6.0;

  //BfTerm=9.81e-6*BfTerm;

  BfTerm=deltaT*BfTerm;


  int NeighborCount;

  int NumGlobalElements = mesh.ElementFaces()->Map().NumGlobalElements();
  int NumMyElements = mesh.ElementFaces()->Map().NumMyElements();
  int ElementSize = mesh.ElementFaces()->Map().ElementSize();
  int* values = mesh.ElementFaces()->Values();

  //F = new Epetra_CrsMatrix(Copy,fluxMap,3);



  Epetra_SerialDenseMatrix ValuesMatrix;
  ValuesMatrix.Shape(6, 6);
  ValuesMatrix.Scale(0.0); // normally set to zero but to make sure it is zero, the entries are scaled.

  Epetra_IntSerialDenseVector IndicesVector;
  IndicesVector.Size(6);



if (poption==1){

  FEF= new Epetra_FECrsMatrix(Copy,*fluxMap,3);

  F = FEF;



  for( int i=0 ; i<6 ; ++i ){
    ValuesMatrix(i,  i  ) = 2.0*BfTerm;
    ValuesMatrix(i,6-i-1) = 1.0*BfTerm;
  }
  //[X 0 0 0 0 x]
  //[0 X 0 0 x 0]
  //[0 0 X x 0 0]
  //[0 0 x X 0 0]
  //[0 x 0 0 X 0]
  //[x 0 0 0 0 X]


  for( int i=0 ; i<NumMyElements ; ++i ) {

    for (int j=0;j<6;j++)
      IndicesVector[j] = values[i*ElementSize+j]-1;

      FEF->InsertGlobalValues(IndicesVector, ValuesMatrix);

  }
}


if (poption==2){

  FEF= new Epetra_FECrsMatrix(Copy,*fluxMap,11);

  F = FEF;

  BfTerm = -EdgeLength*EdgeLength*deltaT*deltaT;

  double divisor;

  divisor= Se*pow(EdgeLength,3.0);

  int mySign[6] = {1, -1, -1, 1, 1, -1};

  for( int i=0 ; i<6 ; ++i ){
    for( int j=0 ; j<6 ; ++j ){
        ValuesMatrix(i,j) = mySign[i]*mySign[j]*BfTerm/divisor;
    }
  }
  //[ X -X -X  X  X -X]
  //[-X  X  X -X -X  X]
  //[-X  X  X -X -X  X]
  //[ X -X -X  X  X -X]
  //[ X -X -X  X  X -X]
  //[-X  X  X -X -X  X]


  for( int i=0 ; i<NumMyElements ; ++i ) {

    for (int j=0;j<6;j++)
      IndicesVector[j] = values[i*ElementSize+j]-1;

      FEF->InsertGlobalValues(IndicesVector, ValuesMatrix);

  }
}

  FEF->GlobalAssemble();

  F->OptimizeStorage();


  // impose bc //////////////////


  /// new version for the bc application


  /// multiply RHS with deltaT !!!!


  int num_myglobals = fluxMap->NumMyElements();

  int* my_globals = new int[num_myglobals];

  my_globals = fluxMap->MyGlobalElements();


  std::vector<int> node_vec;
  int* PIDList = new int[num_myglobals];
  mesh.FixedFaces()->Map().RemoteIDList(num_myglobals, my_globals, PIDList, NULL);
  for (int i=0; i<num_myglobals; ++i)
    if (PIDList[i] >= 0)
      node_vec.push_back(my_globals[i]);


  Epetra_BlockMap* new_fixed_faces_map = new Epetra_BlockMap(-1, node_vec.size(), &node_vec[0], 1, 0, comm);

  Epetra_Export exporter(mesh.FixedFaces()->Map(), *new_fixed_faces_map);

  Epetra_IntVector* new_fixed_faces = new Epetra_IntVector(*new_fixed_faces_map);
  Epetra_IntVector* new_fixed_face_values = new Epetra_IntVector(*new_fixed_faces_map);

  new_fixed_faces->Export(*mesh.FixedFaces(), exporter, Insert);
  new_fixed_face_values->Export(*mesh.FixedFaceValues(), exporter, Insert);

  //now_on use new_fixed_faces

  delete[] PIDList;
  my_globals = NULL;
  delete[] my_globals;
  delete new_fixed_faces_map;

  int num_mygids;
  int num_myvalues;
  int* gids;
  int* myvalues = new_fixed_face_values->Values();

  // apply bc values. - should update Apf, as well. careful with Bp.

  Epetra_Vector corr(*fluxMap);
  Epetra_Vector fixed_faces(*fluxMap);
  num_mygids = new_fixed_face_values->Map().NumMyElements();
  num_myvalues = new_fixed_face_values->MyLength();
  gids = new_fixed_face_values->Map().MyGlobalElements();

  // non-zero flux bc's are not tested!
  for (int i=0; i<num_mygids; ++i) {

	fixed_faces.ReplaceGlobalValue(gids[i], 0, myvalues[i]);

  }

  Epetra_Vector* B = dynamic_cast<Epetra_Vector*>(GetRHS());
  F->Multiply(false, fixed_faces, corr);
  B->Update(-1, corr, 1);


  Epetra_Vector bc_filter(*fluxMap);
  bc_filter.PutScalar(1.0);


  num_mygids = new_fixed_faces->Map().NumMyElements();
  num_myvalues = new_fixed_faces->MyLength();
  gids = new_fixed_faces->Map().MyGlobalElements();


  for (int i=0; i<num_mygids; ++i) {

	bc_filter.ReplaceGlobalValue(gids[i], 0, 0.0);

  }

  F->LeftScale(bc_filter);
  F->RightScale(bc_filter);

  //assing ones for the diagonals
  Epetra_Vector diagonal(F->Map());
  F->ExtractDiagonalCopy(diagonal);


  for (int i=0; i < num_mygids; ++i) {
	diagonal.ReplaceGlobalValue(gids[i], 0,  1.0);
  }

if(poption==1){
  F->ReplaceDiagonalValues(diagonal);
}

  delete new_fixed_faces;
  delete new_fixed_face_values;


  return 0;

}


int FluxProblem::Impose(BoundaryCondition& bcond)
{
  //fixed faces.
  const Epetra_BlockMap& FFMap = mesh.FixedFaces()->Map(); /// same length vectors, Maps should be identical.
  const Epetra_BlockMap& FFVMap = mesh.FixedFaceValues()->Map();

  return 0;
}


int FluxProblem::Restore()
{

}

void FluxProblem::InitializeMaterialProperties(){


  permeability            = MaterialProperties.A()[2];
  viscosity               = MaterialProperties.A()[3];

  elastic_modulus         = MaterialProperties.A()[0];
  poisson_ratio           = MaterialProperties.A()[1];


  bulk_modulus_solid      = MaterialProperties.A()[4];
  bulk_modulus_fluid      = MaterialProperties.A()[5];
  porosity                = MaterialProperties.A()[6];
  bulk_modulus            = ComputeBulkModulus();
  Se                      = ComputeSe();


}

Epetra_FECrsMatrix& FluxProblem::getFEMatrix(){

    return *FEF;

    }

Epetra_CrsMatrix& FluxProblem::getMatrix(){

    return *F;

    }

Epetra_Vector FluxProblem::getDiagonal(){

  const Epetra_Map face_map(-1, mesh.FaceMap()->NumMyElements(), mesh.FaceMap()->MyGlobalElements(), 0, comm);

  diag = new Epetra_Vector(face_map);

  F->ExtractDiagonalCopy(*diag);

  return *diag;

}


void FluxProblem::PrintSolution(SolutionWriter* sw) {

    Epetra_Vector& flux = *dynamic_cast<Epetra_Vector*>(GetLHS());

    sw->PrintFluxes(flux);

}

double FluxProblem::ComputeBulkModulus(){

    return elastic_modulus/(3.0*(1.0-2.0*poisson_ratio));

}

double FluxProblem::ComputeSe(){

    return 1.0/bulk_modulus_solid*(1.0-bulk_modulus/bulk_modulus_solid)+porosity*(1.0/bulk_modulus_fluid-1.0/bulk_modulus_solid);

}
