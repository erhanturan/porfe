/*
 * PorFE: a poroelastic extension on ParFE
 * Copyright (C) 2012, Erhan Turan
 * https://bitbucket.org/erhanturan/porfe
 *
 * ParFE: a micro-FE solver for trabecular bone modeling
 * Copyright (C) 2011,  ParFE developers, see
 * http://parfe.sourceforge.net/developers.php
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#include <Epetra_Map.h>
#include <Epetra_Vector.h>
#include "PoroMatrix.h"
///#include <Epetra_BlockMap.h>

#include "ElementByElementMatrix.h"

/// send  App as vector, others as CRS or FECrs matrices.

PoroMatrix::PoroMatrix(Epetra_RowMatrix&   A_uu_,
        Epetra_CrsMatrix &  A_ff_,
        Epetra_Vector &     A_p_,
        Epetra_CrsMatrix &  A_pu_,
        Epetra_CrsMatrix &  A_pf_,
        BoundaryCondition &       bcond,
        const Epetra_BlockMap &       Xu_Map_,
        const Epetra_MpiComm      comm_)
        :   map_uform(A_uu_.OperatorDomainMap()),
                map_fform(A_ff_.DomainMap()),
                map_pform(A_p_.Map()),
                Xu_Map(Xu_Map_),
                A_uu(A_uu_),
                A_ff(A_ff_),
                A_p(A_p_),
                A_pu(A_pu_),
                A_pf(A_pf_),
                bcond(bcond),
                comm(comm_),
                composed_map(NULL) {  /// this is just to create a map



            // composing the global map indices from the three partial maps into one 'composed_map'
            size_glob_uform      =  A_uu.OperatorDomainMap().NumGlobalElements(); ///rowmap to OperatorDomainMap?
            size_glob_fform      =  A_ff.DomainMap().NumGlobalElements();
            size_glob_pform      =  A_p.Map().NumGlobalElements();

            size_loc_uform       =  A_uu.OperatorDomainMap().NumMyElements();
            size_loc_fform       =  A_ff.DomainMap().NumMyElements();
            size_loc_pform       =  A_p.Map().NumMyElements();


            /*
            cout<<comm.MyPID()<<": g u="<<size_glob_uform<<endl;
            cout<<comm.MyPID()<<": g f="<<size_glob_fform<<endl;
            cout<<comm.MyPID()<<": g p="<<size_glob_pform<<endl;

            cout<<comm.MyPID()<<": l u="<<size_loc_uform<<endl;
            cout<<comm.MyPID()<<": l f="<<size_loc_fform<<endl;
            cout<<comm.MyPID()<<": l p="<<size_loc_pform<<endl;
             */

            ///int  composed_glob_map_size = size_glob_uform;
            int  composed_glob_map_size = size_glob_uform + size_glob_fform + size_glob_pform;

            ///int  composed_loc_map_size  = size_loc_uform;
            int  composed_loc_map_size  = size_loc_uform  + size_loc_fform  + size_loc_pform;

            composed_map_indices   =  new int[composed_loc_map_size];

            int * indices_uform        =  map_uform.MyGlobalElements();
            int * indices_fform        =  map_fform.MyGlobalElements();
            int * indices_pform        =  map_pform.MyGlobalElements();


            for (int i=0; i<size_loc_uform; i++)
                composed_map_indices[i]  =  indices_uform[i];

///*
            for (int i=0; i<size_loc_fform; i++)
                composed_map_indices[size_loc_uform + i]  =  size_glob_uform  +  indices_fform[i];

            for (int i=0; i<size_loc_pform; i++)
                composed_map_indices[ size_loc_uform + size_loc_fform + i ]
                        = size_glob_uform + size_glob_fform + indices_pform[i];
///*/


            composed_map = new Epetra_Map(composed_glob_map_size, composed_loc_map_size, composed_map_indices, 0, comm);


            X_uform = new Epetra_Vector(map_uform);
            X_fform = new Epetra_Vector(map_fform);
            X_pform = new Epetra_Vector(map_pform);

            tmp_uform = new Epetra_Vector(map_uform);
            tmp_fform = new Epetra_Vector(map_fform);
            tmp_pform = new Epetra_Vector(map_pform);




            /// create the scaling vectors.


/*
            Au = new Epetra_Vector(A_uu.Map());
            Af = new Epetra_Vector(A_ff.DomainMap());

            A_uu.ExtractDiagonalCopy(*Au);
            A_ff.ExtractDiagonalCopy(*Af);


            const Epetra_Map pressure_map_column(-1, Xu_Map.NumMyElements(), Xu_Map.MyGlobalElements(), 0, comm);

            // initialize the vectors
            scaleU = new Epetra_Vector(pressure_map_column);
            scaleF = new Epetra_Vector(*Af);
            scaleP = new Epetra_Vector(A_p);


            /// legend
            /// assuming N nodes and d number of dofs per node:
            int myElementSize=A_uu.Map().ElementSize(); /// d
            int numMyElements=A_uu.Map().NumMyElements(); /// N
            int numMyPoints=A_uu.Map().NumMyPoints(); /// N*d

            int*  Xgids;
            Xgids = A_uu.Map().MyGlobalElements(); /// indices
            double* Xivalues = Au->Values(); ///values

            int myIndex;
            double myTestval1;

            for (int i=0; i<numMyElements; ++i) {
                for (int j=0; j<myElementSize; ++j) {

                    myIndex=Xgids[i]*myElementSize+j;

                    myTestval1 = Xivalues[scaleU->Map().LID(myIndex)];

                    //both of them works!
                    scaleU->ReplaceGlobalValue(myIndex,  0, myTestval1);
                    //scaleU->ReplaceMyValue(scaleU->Map().LID(myIndex),  0, myTestval1);

                }
            }


            // calculate the absolute values.
            scaleU->Abs(*scaleU);
            scaleF->Abs(*scaleF);
            scaleP->Abs(*scaleP);
            // calculate the element-wise inverse values
            scaleU->Reciprocal(*scaleU);
            scaleF->Reciprocal(*scaleF);
            scaleP->Reciprocal(*scaleP);
            // calculate the square-root.
            double myVecValue;
            double myVecValue1;
            int NumMyElementsVec;


            // u - vector
            NumMyElementsVec=scaleU->Map().NumMyElements();

            /// check whether the block-map is working
            for (int i = 0; i < NumMyElementsVec; i++){

                myVecValue=scaleU->Values()[i];
                scaleU->ReplaceMyValue(i, 0, sqrt(myVecValue));

            }


            // f - vector
            NumMyElementsVec=scaleF->Map().NumMyElements();

            for (int i = 0; i < NumMyElementsVec; i++){
                myVecValue=scaleF->Values()[i];
                scaleF->ReplaceMyValue(i, 0, sqrt(myVecValue));
            }

            // p - vector
            NumMyElementsVec=scaleP->Map().NumMyElements();

            for (int i = 0; i < NumMyElementsVec; i++){
                myVecValue=scaleP->Values()[i];
                scaleP->ReplaceMyValue(i, 0, sqrt(myVecValue));
            }
//scaling vectors are calculated.
*/

        }






        PoroMatrix::~PoroMatrix() {

            delete[] composed_map_indices;
            delete composed_map;
            delete X_uform;
            delete X_fform;
            delete X_pform;
            delete tmp_uform;
            delete tmp_fform;
            delete tmp_pform;

        }

        void PoroMatrix::myPrint() {

            std::cout<<"this is a print function to test the method."<<std::endl;
        }



        int PoroMatrix::Apply(const Epetra_MultiVector& X, Epetra_MultiVector& Y) const {

            ///cout<<"A proc: "<<comm.MyPID()<<endl;

            //Y.PutScalar(0.0);
            //Y=X;

            const Epetra_Vector* X_vec = X(0);
            Epetra_Vector* Y_vec  = Y(0);

/*
            Epetra_Vector   X_uform(map_uform);
            Epetra_Vector   X_fform(map_fform);
            Epetra_Vector   X_pform(map_pform);

            Epetra_Vector   tmp_uform(map_uform);
            Epetra_Vector   tmp_fform(map_fform);
            Epetra_Vector   tmp_pform(map_pform);
*/

            // Extract the partial vectors from X
            for (int i=0; i<size_loc_uform; i++)  (*X_uform)[i] = (*X_vec)[i                                  ];
            for (int i=0; i<size_loc_fform; i++)  (*X_fform)[i] = (*X_vec)[i + size_loc_uform                 ];
            for (int i=0; i<size_loc_pform; i++)  (*X_pform)[i] = (*X_vec)[i + size_loc_uform + size_loc_fform];


            /// Auu*Xu + Apu^T*Xp
            A_uu.Apply(*X_uform  ,  *tmp_uform );

            for (int i=0; i<size_loc_uform; i++)
                (*Y_vec)[ i ] = (*tmp_uform)[i];

///*
            A_pu.Multiply( true , *X_pform  ,  *tmp_uform );
            for (int i=0; i<size_loc_uform; i++)
                (*Y_vec)[ i ]                                     += (*tmp_uform)[i];
                //(*Y_vec)[ i ]                                     += tmp_uform[i];
//*/

            /// Aff*Xf + Apf^T*Xp
            A_ff.Multiply(false, *X_fform  ,  *tmp_fform );
            for (int i=0; i<size_loc_fform; i++)  {
                //cout<<"==== X: "<<X_fform[i]<<" T: "<<tmp_fform[i]<<endl;
                (*Y_vec)[ i + size_loc_uform ]                     = (*tmp_fform)[i];
                //(*Y_vec)[ i + size_loc_uform ]                     = 2; ///tmp_fform[i];
            }
///*
            A_pf.Multiply(true , *X_pform  ,  *tmp_fform );
            for (int i=0; i<size_loc_fform; i++) {
                (*Y_vec)[ i + size_loc_uform ]                    += (*tmp_fform)[i];
                //(*Y_vec)[ i + size_loc_uform ]                    += tmp_fform[i];
            }
//*/

///*
            /// Apu*Xu + Apf*Xf + App*Xp
            A_pu.Multiply( false, *X_uform  ,  *tmp_pform );
            for (int i=0; i<size_loc_pform; i++)  {
                (*Y_vec)[ i + size_loc_uform + size_loc_fform ]    = (*tmp_pform)[i];
            }

            A_pf.Multiply( false, *X_fform  ,  *tmp_pform );
            for (int i=0; i<size_loc_pform; i++) {
                (*Y_vec)[ i + size_loc_uform + size_loc_fform ]   += (*tmp_pform)[i];
            }
//*/
            //A_pp.Multiply( false, X_pform  ,  tmp_pform );    for (int i=0; i<size_loc_pform; i++)   (*Y_vec)[ i + size_loc_uform + size_loc_fform ]   += tmp_pform[i];
            for (int i=0; i<size_loc_pform; i++){
                (*Y_vec)[ i + size_loc_uform + size_loc_fform ]  += (*X_pform)[ i ]*A_p[i];
            }


            return 0;
        }



        int PoroMatrix::ApplyInverse(const Epetra_MultiVector& X, Epetra_MultiVector& Y) const {
            return -1;
        }


        void PoroMatrix::Scale(){
        /// scale the problem instead Ax=b, solve D*A*D=D*b (after the solve, the true solution is x=D*x)
        /// D is a diagonal matrix where Dii=1/sqrt(abs(Aii))

            //Apply A1=A*D which requires rightscale - i.e columns are scaled

            A_uu.RightScale(*scaleU);
            A_ff.RightScale(*scaleF);
            A_pu.RightScale(*scaleU);
            A_pf.RightScale(*scaleF);
            A_p.Multiply(1.0, A_p, *scaleP, 0.0);

            //Apply A2=D*A1 which requires leftscale - i.e rows are scaled.

            A_uu.LeftScale(*scaleU);
            A_ff.LeftScale(*scaleF);
            A_pu.LeftScale(*scaleP);
            A_pf.LeftScale(*scaleP);
            A_p.Multiply(1.0, A_p, *scaleP, 0.0);

        }


        void PoroMatrix::ScaleU(Epetra_Vector & A_u){
            //Apply vec=D*vec, D being the scaling matrix.
            A_u.Multiply(1.0, A_u, *scaleU, 0.0);
        }

        void PoroMatrix::ScaleF(Epetra_Vector & A_f){
            //Apply vec=D*vec, D being the scaling matrix.
            A_f.Multiply(1.0, A_f, *scaleF, 0.0);
        }

        void PoroMatrix::ScaleP(Epetra_Vector & A_p){
            //Apply vec=D*vec, D being the scaling matrix.
            A_p.Multiply(1.0, A_p, *scaleP, 0.0);
        }



        void PoroMatrix::ScaleP(Epetra_CrsMatrix& S_pp){
        /// scale the problem instead Ax=b, solve D*A*D=D*b (after the solve, the true solution is x=D*x)
        /// D is a diagonal matrix where Dii=1/sqrt(abs(Aii))

            //Apply A1=A*D which requires rightscale - i.e columns are scaled

            S_pp.RightScale(*scaleP);

            //Apply A2=D*A1 which requires leftscale - i.e rows are scaled.

            S_pp.LeftScale(*scaleP);

        }


        Epetra_Vector PoroMatrix::compose_vector( const Epetra_Vector& vec_uform, const Epetra_Vector& vec_fform, const Epetra_Vector& vec_pform ) {
//Epetra_Vector PoroMatrix::compose_vector( const Epetra_Vector& vec_uform ) {



            Epetra_Vector composed_vector(*composed_map);


            for (int i=0; i<size_loc_uform; i++)   composed_vector[ i ]                                    = vec_uform[i];
            for (int i=0; i<size_loc_fform; i++)   composed_vector[ i + size_loc_uform ]                   = vec_fform[i];
            for (int i=0; i<size_loc_pform; i++)   composed_vector[ i + size_loc_uform + size_loc_fform ]  = vec_pform[i];

            return composed_vector;
        }


//void PoroMatrix::extract_partial_vectors( Epetra_Vector*      vec_uform, Epetra_Vector*      vec_fform, Epetra_Vector*     vec_pform,
//                                                 const Epetra_Vector&      composed_vector ) const  {


        void PoroMatrix::extract_partial_vectors( Epetra_Vector&      vec_uform, Epetra_Vector&      vec_fform, Epetra_Vector&      vec_pform,
                const Epetra_Vector&      composed_vector ) const  {



            //vec_uform= new Epetra_Vector(map_uform);
            //vec_fform= new Epetra_Vector(map_fform);
            //vec_pform= new Epetra_Vector(map_pform);


            for (int i=0; i<size_loc_uform; i++)   vec_uform[ i ]  =  composed_vector[ i ];
            for (int i=0; i<size_loc_fform; i++)   vec_fform[ i ]  =  composed_vector[ i + size_loc_uform ];
            for (int i=0; i<size_loc_pform; i++)   vec_pform[ i ]  =  composed_vector[ i + size_loc_uform + size_loc_fform ];

        }


        void PoroMatrix::update_vector( Epetra_Vector&      vec_pform,
                Epetra_Vector&      composed_vector ) {



            //vec_uform= new Epetra_Vector(map_uform);
            //vec_fform= new Epetra_Vector(map_fform);
            //vec_pform= new Epetra_Vector(map_pform);


            for (int i=0; i<size_loc_pform; i++)   composed_vector[ i + size_loc_uform + size_loc_fform ] = vec_pform[ i ];

        }


        /*
int PoroMatrix::PrepareApply(Epetra_Vector* fixed_dofs, Epetra_IntVector* restrained_dofs)
{
  fixed_domain_inds_u .resize(0);
  //precalculation of time-critical datastructures
  int element, ldof;
  int nodof = dof_map.ElementSize();
  //local indices mapping from fixed dofs to domain map
  for (int i = 0 ; i < fixed_dofs->MyLength(); ++i) {
    if (fixed_dofs->operator[](i) != 0.0) {
      fixed_dofs->Map().FindLocalElementID(i, element, ldof);
      fixed_domain_inds_u.push_back( domain_map->LID(fixed_dofs->Map().GID(element)*nodof + ldof) );
    }
  }
  restrained_domain_inds.resize(0);
  //local indices mapping from restrained dofs to domain map
  for (int i = 0 ; i < restrained_dofs->MyLength(); ++i) {
    if (restrained_dofs->operator[](i) == 0) {
      restrained_dofs->Map().FindLocalElementID(i, element, ldof);
      restrained_domain_inds_u.push_back( domain_map->LID(restrained_dofs->Map().GID(element)*nodof + ldof) );
    }
  }

  return 0;
}
         */
