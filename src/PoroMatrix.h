/*
 * PorFE: a poroelastic extension on ParFE
 * Copyright (C) 2012, Erhan Turan
 * https://bitbucket.org/erhanturan/porfe
 *
 * ParFE: a micro-FE solver for trabecular bone modeling
 * Copyright (C) 2011,  ParFE developers, see
 * http://parfe.sourceforge.net/developers.php
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#ifndef _PORO_MATRIX_H_
#define _PORO_MATRIX_H_

#include <Epetra_Map.h>
#include <Epetra_Vector.h>
#include <Epetra_Operator.h>
#include <Epetra_RowMatrix.h>
#include <Epetra_FECrsMatrix.h>
#include <Epetra_MpiComm.h>

///#include <Epetra_VbrMatrix.h> /// erhan added
#include <Epetra_FEVbrMatrix.h> /// erhan added
///#include <Epetra_BlockMap.h>

#include "BoundaryCondition.h"




/// class example is taken by Marcus Wittberger and changed for Poroelasticity problem.
/// this class enables us to perform matrix-vector product using the submatrices defined inf ParFE.


/*!
 *
 *
 *
 */
class PoroMatrix : public Epetra_Operator, public Epetra_SrcDistObject {

public:

    //! PoroMatrix constructor
    /*!
     *  \param a_dof_map
     *  (In) A Epetra_BlockMap indicating how the DOFs are distributed
     *
     *  \param element_matrix
     *  (In) A ElementIntergrator object that is able to compute any needed element stiffness matrices.
     *  Normally, the element stiffness matrix has to be computed only once.
     *
     *  \param elem2node
     *  (In) This Epetra_IntVector contains the element-to-node table.
     *
     *  \param mat_ids
     *  (In) This Epetra_IntVector contains the material IDs.
     *
     *  \return Pointer to the PoroMatrix object.
     */
    /// //TODO: not every matrix is FECrsMatrix!!!
    PoroMatrix( Epetra_RowMatrix &  A_uu_,
            Epetra_CrsMatrix &  A_ff_,
            Epetra_Vector &  A_p_,
            Epetra_CrsMatrix &  A_pu_,
            Epetra_CrsMatrix &  A_pf_,
            BoundaryCondition & bcond,
            const Epetra_BlockMap &       Xu_Map_,
            const Epetra_MpiComm comm_);


    //! PoroMatrix destructor
    ~PoroMatrix();

    //! Do not use the transpose while applying
    int SetUseTranspose(bool UseTranspose) { return -1; };


    //! Returns the result of the PoroMatrix multiplied with a Epetra_MultiVector U in V.
    /*!
     *  \param U
     *  (In) A Epetra_Vector of dimension NumVectors to be multiplied by the PoroMatrix.
     *
     *  \param V
     *  (Out) A Epetra_Vector of dimension NumVectors containing the result.
     *
     *  \return Integer error code, set to 0 if successful.
     */
    int Apply(const Epetra_MultiVector& X, Epetra_MultiVector& Y) const;
    //int Apply(const Epetra_MultiVector& X, Epetra_MultiVector& Y) const;

    //! Returns -1. Only implemented for convenience.
    int ApplyInverse(const Epetra_MultiVector& X, Epetra_MultiVector& Y) const;

    //! Returns -1.0. Only implemented for convenience.
    double NormInf()                      const { return -1.0; };

    //! Returns a character string describing the operator.
    const char* Label()                   const { return "PoroMatrix"; };

    //! Returns false
    bool UseTranspose()                   const { return false; };

    //! Returns false
    bool HasNormInf()                     const { return false; };

    //! Returns the Epetra_Comm object associated with this operator.
    const Epetra_Comm& Comm()             const { return comm; };

    //! Returns the Epetra_Map object associated with the domain of this operator.
    const Epetra_Map& OperatorDomainMap() const { return (*composed_map); };

    //! Returns the Epetra_Map object associated with the range of this operator.
    const Epetra_Map& OperatorRangeMap()  const { return (*composed_map); };

    //! Returns the Epetra_BlockMap object associated with this operator.
    const Epetra_BlockMap& Map()          const {
        return (*composed_map);
    };


    void Scale();

    void ScaleU(Epetra_Vector& A_u);
    void ScaleF(Epetra_Vector& A_f);
    void ScaleP(Epetra_Vector& A_p);

    void ScaleP(Epetra_CrsMatrix& S_pp);


    Epetra_Vector compose_vector( const Epetra_Vector& vec_uform,
            const Epetra_Vector& vec_fform,
            const Epetra_Vector& vec_pform );


    void extract_partial_vectors( Epetra_Vector&      vec_uform,
            Epetra_Vector&      vec_fform,
            Epetra_Vector&      vec_pform,
            const Epetra_Vector& vec            ) const;

    void update_vector( Epetra_Vector&      vec_pform,
                Epetra_Vector&      vec );



    void myPrint();


private:


    const Epetra_BlockMap map_uform;
    const Epetra_BlockMap map_fform;
    const Epetra_BlockMap map_pform;
    const Epetra_BlockMap Xu_Map;


    Epetra_RowMatrix& A_uu;
    Epetra_CrsMatrix& A_ff;
    Epetra_Vector& A_p;
    Epetra_CrsMatrix& A_pu;
    Epetra_CrsMatrix& A_pf;
    BoundaryCondition & bcond;


    //diagonals of the Auu and Aff blocks. Ap is already diagonal.
    Epetra_Vector* Au;
    Epetra_Vector* Af;

    //scaling vectors. d of D.
    Epetra_Vector* scaleU;
    Epetra_Vector* scaleF;
    Epetra_Vector* scaleP;



    const Epetra_MpiComm  comm;

    int*        composed_map_indices;
    Epetra_Map*  composed_map;


    int size_glob_uform;
    int size_glob_fform;
    int size_glob_pform;

    int size_loc_uform;
    int size_loc_fform;
    int size_loc_pform;


    Epetra_Vector* X_uform;
    Epetra_Vector* X_fform;
    Epetra_Vector* X_pform;

    Epetra_Vector* tmp_uform;
    Epetra_Vector* tmp_fform;
    Epetra_Vector* tmp_pform;

};



#endif

