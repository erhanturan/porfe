/*
 * PorFE: a poroelastic extension on ParFE
 * Copyright (C) 2012, Erhan Turan
 * https://bitbucket.org/erhanturan/porfe
 *
 * ParFE: a micro-FE solver for trabecular bone modeling
 * Copyright (C) 2011,  ParFE developers, see
 * http://parfe.sourceforge.net/developers.php
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#ifndef _PORO_MATRIX_PRECONDITIONERH_H_
#define _PORO_MATRIX_PRECONDITIONERH_H_


#include <Epetra_Map.h>
#include <Epetra_Vector.h>
#include <Epetra_Operator.h>
#include <Epetra_FECrsMatrix.h>
#include <Epetra_CrsMatrix.h>
#include <Epetra_MpiComm.h>

#include <Epetra_FEVbrMatrix.h>

#include "BoundaryCondition.h"

#include <ml_MultiLevelPreconditioner.h>
#include <ml_MatrixFreePreconditioner.h>
#include <Teuchos_ParameterList.hpp>
#include "ml_include.h"

#include "Amesos.h"
#include "Amesos_BaseSolver.h"
#include "AztecOO.h"
#include <Epetra_RowMatrix.h>

#include "BelosConfigDefs.hpp"
#include "BelosLinearProblem.hpp"
#include "BelosEpetraAdapter.hpp"
#include "BelosPCPGSolMgr.hpp"
#include "BelosMinresSolMgr.hpp"
#include <Teuchos_ParameterList.hpp>
#include <Teuchos_RefCountPtr.hpp>
#include <Teuchos_CommandLineProcessor.hpp>
#include "Teuchos_RCP.hpp"
#include <Ifpack.h>
//#include <Amesos_Superludist.h>
/*!
 *
 * class example is taken by Marcus Wittberger and adapted to Poroelasticity problem.
 * this class enables us to perform matrix-vector product using the submatrices defined in ParFE.
 *
 */

class PoroMatrixPreconditionerH : public Epetra_Operator, public Epetra_SrcDistObject {

public:

    //! PoroMatrixPreconditionerH constructor
    /*!
     *  \param a_dof_map
     *  (In) A Epetra_BlockMap indicating how the DOFs are distributed
     *
     *  \param element_matrix
     *  (In) A ElementIntergrator object that is able to compute any needed element stiffness matrices.
     *  Normally, the element stiffness matrix has to be computed only once.
     *
     *  \param elem2node
     *  (In) This Epetra_IntVector contains the element-to-node table.
     *
     *  \param mat_ids
     *  (In) This Epetra_IntVector contains the material IDs.
     *
     *  \return Pointer to the PoroMatrixPreconditionerH object.
     */
    /// //TODO: not every matrix is FECrsMatrix!!!


    PoroMatrixPreconditionerH(  Epetra_RowMatrix &  A_uu_,
                                Epetra_CrsMatrix & A_ff_,
                                Epetra_CrsMatrix & A_pu_,
                                Epetra_CrsMatrix & A_pf_,
                          const Epetra_Vector & A_f_,
                          const Epetra_Vector & A_p_,
                                Epetra_CrsMatrix & S_pp_ ,
                          const Epetra_MpiComm comm_,
                          const Teuchos::ParameterList MLList_,
                          const Teuchos::ParameterList MLList_Spp_,
                                double AuuTol_,
                                double AffTol_,
                                double SppTol_,
                                int MaxAuuIter_,
                                int MaxAffIter_,
                                int MaxSppIter_);



    //! PoroMatrixPreconditionerH destructor
    ~PoroMatrixPreconditionerH();

    //! Do not use the transpose while applying
    int SetUseTranspose(bool UseTranspose) { return -1; };


    //! Returns the result of the PoroMatrixPreconditionerH multiplied with a Epetra_MultiVector U in V.
    /*!
     *  \param U
     *  (In) A Epetra_Vector of dimension NumVectors to be multiplied by the PoroMatrixPreconditionerH.
     *
     *  \param V
     *  (Out) A Epetra_Vector of dimension NumVectors containing the result.
     *
     *  \return Integer error code, set to 0 if successful.
     */
    int Apply(const Epetra_MultiVector& X, Epetra_MultiVector& Y) const;
    //int Apply(const Epetra_MultiVector& X, Epetra_MultiVector& Y) const;

    //! Returns -1. Only implemented for convenience.
    int ApplyInverse(const Epetra_MultiVector& X, Epetra_MultiVector& Y) const;

    //! Returns -1.0. Only implemented for convenience.
    double NormInf()                      const { return -1.0; };

    //! Returns a character string describing the operator.
    const char* Label()                   const { return "PoroMatrixPreconditionerH"; };

    //! Returns false
    bool UseTranspose()                   const { return false; };

    //! Returns false
    bool HasNormInf()                     const { return false; };

    //! Returns the Epetra_Comm object associated with this operator.
    const Epetra_Comm& Comm()             const { return comm; };

    //! Returns the Epetra_Map object associated with the domain of this operator.
    const Epetra_Map& OperatorDomainMap() const { return (*composed_map); };

    //! Returns the Epetra_Map object associated with the range of this operator.
    const Epetra_Map& OperatorRangeMap()  const { return (*composed_map); };

    //! Returns the Epetra_BlockMap object associated with this operator.
    const Epetra_BlockMap& Map()          const {
        return (*composed_map);
    };


    void myPrint();


private:


    const Epetra_BlockMap map_uform;
    const Epetra_BlockMap map_fform;
    const Epetra_BlockMap map_pform;


    Epetra_RowMatrix& A_uu;
    const Epetra_Vector& A_f;
    const Epetra_Vector& A_p;
    Epetra_CrsMatrix& S_pp;
    ///BoundaryCondition & bcond;
    const Teuchos::ParameterList& MLList;
    const Teuchos::ParameterList& MLList_Spp;

    Epetra_CrsMatrix& A_ff;
    Epetra_CrsMatrix& A_pu;
    Epetra_CrsMatrix& A_pf;

    ML_Epetra::MultiLevelPreconditioner* MA_uu;
    ML_Epetra::MultiLevelPreconditioner* MS_pp;

    Epetra_Vector* Sp;

    const Epetra_MpiComm  comm;

    int* composed_map_indices;
    Epetra_Map*  composed_map;

    double AuuTol;
    double AffTol;
    double SppTol;

    int MaxAuuIter;
    int MaxAffIter;
    int MaxSppIter;

    int size_glob_uform;
    int size_glob_fform;
    int size_glob_pform;

    int size_loc_uform;
    int size_loc_fform;
    int size_loc_pform;


    Ifpack_Preconditioner* Prec;

    Epetra_Vector* X_uform;
    Epetra_Vector* X_fform;
    Epetra_Vector* X_pform;

    Epetra_Vector* tmp_uform;
    Epetra_Vector* tmp_fform;
    Epetra_Vector* tmp_pform;

};

#endif
