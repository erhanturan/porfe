/*
 * PorFE: a poroelastic extension on ParFE
 * Copyright (C) 2012, Erhan Turan
 * https://bitbucket.org/erhanturan/porfe
 *
 * ParFE: a micro-FE solver for trabecular bone modeling
 * Copyright (C) 2011,  ParFE developers, see
 * http://parfe.sourceforge.net/developers.php
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#include <Epetra_FECrsGraph.h>
//#include <Epetra_CrsMatrix.h>
#include <Epetra_MultiVector.h>
#include "SchurProblem.h"
#include <cmath>

SchurProblem::SchurProblem(DistMesh& a_mesh, Teuchos::ParameterList& fe_param, double a_deltaT)
  : ElasticityProblem(a_mesh, fe_param)
{

  int NumPressurePerElement  = 1; /// might be defined within the mesh file but written here explicitly.
                                  /// similar idea of NumDofsPerNode from nr_dofs_per_node could be used.

  schur_map = new Epetra_Map(-1, mesh.SchurMap()->NumMyElements(), mesh.SchurMap()->MyGlobalElements(), 0, comm);

  /// X and B are not needed. remove carefully.
  Epetra_Vector* X = new Epetra_Vector(*schur_map);
  Epetra_Vector* B = new Epetra_Vector(*schur_map);
  //SetOperator(schur_mat);
  //SetOperator(block_mat);
  SetLHS(X);
  SetRHS(B);


  ///initialize the Material properties
  InitializeMaterialProperties();

  deltaT=a_deltaT;
  Assemble();

}

SchurProblem::~SchurProblem()
{
  delete GetLHS();
  delete GetRHS();
  delete GetOperator();
}



int SchurProblem::Assemble()
{

   // Schur App - BF in poisson matrix format.
   // Se*a^3 - (mu/k)^-1 * 15 * a * alpha

  double AppTerm = -Se*pow(EdgeLength,3.0);
  alpha=1.0;
  double BfTerm = 1.5*(permeability/viscosity)*EdgeLength*alpha*deltaT;

  double BfTermDiagonal;
  double SchurDiagonal;


  int NeighborCount;

  int NumGlobalElements = mesh.ElementNeighbors()->Map().NumGlobalElements();
  int NumMyElements = mesh.ElementNeighbors()->Map().NumMyElements();
  int ElementSize = mesh.ElementNeighbors()->Map().ElementSize();
  int* values = mesh.ElementNeighbors()->Values();


  /// detect the fill pattern.
  int * NumNz = new int[NumMyElements];
  int * NumNzDiag = new int[NumMyElements];
  int fluxCheck;

  for (int i = 0; i < NumMyElements; i++){

        NeighborCount=0;
        fluxCheck=0;

        for (int j = 0; j < ElementSize; j++){

            ///if (values[i*ElementSize+j] == -1)
            if (values[i*ElementSize+j] == -1)
                 NeighborCount++;
            if (values[i*ElementSize+j] == -2){
                 NeighborCount++;
                 fluxCheck=1;
            }

            }
        NumNz[i] = 7 - NeighborCount; /// 7 is max amount of entries per row.
        NumNzDiag[i] = NumNz[i]-1; /// 7 is max amount of entries per row.
        if (fluxCheck == 1) NumNzDiag[i] = NumNz[i]+1;
        ///cout<<NumNz[i]<<endl;
  }


  // create the matrix with the nonzero pattern.
  ///S = new Epetra_CrsMatrix(Copy,*schur_map,NumNz);

  SEF= new Epetra_FECrsMatrix(Copy,*schur_map,NumNz);

  S = SEF;


  int * MyGlobalElements = schur_map->MyGlobalElements();

  int myCount;
  double *offValues; /// values off-diagonal terms
  int   *offIndices; /// indices off-diagonal terms

  for (int i = 0; i < NumMyElements; i++){

       offValues  = new double[NumNz[i]-1]; /// values off-diagonal terms
       offIndices  = new int[NumNz[i]-1]; /// indices off-diagonal terms

        myCount=0;

        for (int j = 0; j < ElementSize; j++){


            //if (values[i*ElementSize+j] != -1){
            if (values[i*ElementSize+j] > -1){
                offValues[myCount] = -BfTerm;
                offIndices[myCount] = values[i*ElementSize+j]-1;
                myCount++;
            }

        }


        /// insert off-diagonal values. ///

         ///S->InsertGlobalValues(MyGlobalElements[i], NumNz[i]-1, offValues, offIndices);
         SEF->InsertGlobalValues(MyGlobalElements[i], NumNz[i]-1, offValues, offIndices);

        //S->InsertGlobalValues(MyGlobalElements[i], 2, myval, myInd);


        BfTermDiagonal = BfTerm*NumNzDiag[i];

        SchurDiagonal  = -AppTerm + BfTermDiagonal; // original is this one - not that Appterm is negative (to be equal to App terms yet App is not negative anymore)
        //SchurDiagonal  = AppTerm - BfTermDiagonal;
        SchurDiagonal  = SchurDiagonal;

        ///S->InsertGlobalValues(MyGlobalElements[i], 1, &SchurDiagonal, MyGlobalElements+i);
        SEF->InsertGlobalValues(MyGlobalElements[i], 1, &SchurDiagonal, MyGlobalElements+i);
        //schur_mat->InsertGlobalValues(MyGlobalElements[i], 1, &SchurDiagonal, MyGlobalElements+i);

        delete[] offValues;
        delete[] offIndices;

  }

  ///S->FillComplete();
  ///S->OptimizeStorage();
  SEF->GlobalAssemble(true, Add);
  S->OptimizeStorage();

  delete[] NumNz;
  delete[] NumNzDiag;

  return 0;

}



void SchurProblem::InitializeMaterialProperties(){


  elastic_modulus         = MaterialProperties.A()[0];
  poisson_ratio           = MaterialProperties.A()[1];
  permeability            = MaterialProperties.A()[2];
  viscosity               = MaterialProperties.A()[3];
  bulk_modulus_solid      = MaterialProperties.A()[4];
  bulk_modulus_fluid      = MaterialProperties.A()[5];
  porosity                = MaterialProperties.A()[6];
  bulk_modulus            = ComputeBulkModulus();
  alpha                   = ComputeAlpha();
  Se                      = ComputeSe();

}


double SchurProblem::ComputeBulkModulus(){

    return elastic_modulus/(3.0*(1.0-2.0*poisson_ratio));

}

double SchurProblem::ComputeAlpha(){

    return 1.0-bulk_modulus/bulk_modulus_solid;

}

double SchurProblem::ComputeSe(){

    return 1.0/bulk_modulus_solid*(1.0-bulk_modulus/bulk_modulus_solid)+porosity*(1.0/bulk_modulus_fluid-1.0/bulk_modulus_solid);

}

int SchurProblem::Impose(BoundaryCondition& bcond){}
int SchurProblem::Restore(){}

Epetra_CrsMatrix& SchurProblem::getMatrix(){

    return *S;


    }
