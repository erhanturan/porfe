/*
 * PorFE: a poroelastic extension on ParFE
 * Copyright (C) 2012, Erhan Turan
 * https://bitbucket.org/erhanturan/porfe
 *
 * ParFE: a micro-FE solver for trabecular bone modeling
 * Copyright (C) 2011,  ParFE developers, see
 * http://parfe.sourceforge.net/developers.php
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#include <Epetra_Map.h>
#include <Epetra_Vector.h>
#include "PoroMatrixPreconditionerB.h"

#include "ElementByElementMatrix.h"
#include <Teuchos_ParameterList.hpp>
#include "ml_include.h"
#include <ml_MultiLevelPreconditioner.h>
#include <ml_MatrixFreePreconditioner.h>
#include <Ifpack.h>
#include <Ifpack_AdditiveSchwarz.h>
#include "AztecOO.h"

#include "Amesos.h"
#include "Amesos_ConfigDefs.h"
#include "Amesos_BaseSolver.h"
//#include <Amesos_Superludist.h>
PoroMatrixPreconditionerB::PoroMatrixPreconditionerB(   Epetra_RowMatrix &  A_uu_,
                                                        Epetra_CrsMatrix &  A_ff_,
                                                        Epetra_CrsMatrix &  A_pu_,
                                                        Epetra_CrsMatrix &  A_pf_,
                                                  const Epetra_Vector & A_f_,
                                                  const Epetra_Vector & A_p_,
                                                        Epetra_CrsMatrix & S_pp_,
                                                  const Epetra_MpiComm comm_,
                                                  const Teuchos::ParameterList MLList_,
                                                  const Teuchos::ParameterList MLList_Spp_,
                                                        double AuuTol_,
                                                        double AffTol_,
                                                        double SppTol_,
                                                        int MaxAuuIter_,
                                                        int MaxAffIter_,
                                                        int MaxSppIter_)
                                                    :   map_uform(A_uu_.OperatorDomainMap()),
                                                        map_fform(A_f_.Map()),
                                                        map_pform(S_pp_.DomainMap()),
                                                        A_uu(A_uu_),
                                                        A_ff(A_ff_),
                                                        A_pu(A_pu_),
                                                        A_pf(A_pf_),
                                                        A_f(A_f_),
                                                        A_p(A_p_),
                                                        S_pp(S_pp_),
                                                        comm(comm_),
                                                        composed_map(NULL),
                                                        MA_uu(NULL),
                                                        MS_pp(NULL),
                                                        MLList(MLList_),
                                                        MLList_Spp(MLList_Spp_),
                                                        AuuTol(AuuTol_),
                                                        AffTol(AffTol_),
                                                        SppTol(SppTol_),
                                                        MaxAuuIter(MaxAuuIter_),
                                                        MaxAffIter(MaxAffIter_),
                                                        MaxSppIter(MaxSppIter_)

        {
            /// composing the global map indices from the three partial maps into one 'composed_map'
            size_glob_uform      =  A_uu.OperatorDomainMap().NumGlobalElements();
            size_glob_fform      =  A_f.Map().NumGlobalElements();
            size_glob_pform      =  S_pp.DomainMap().NumGlobalElements();

            size_loc_uform       =  A_uu.OperatorDomainMap().NumMyElements();
            size_loc_fform       =  A_f.Map().NumMyElements();
            size_loc_pform       =  S_pp.DomainMap().NumMyElements();

            //int  composed_glob_map_size = size_glob_uform;
            int  composed_glob_map_size = size_glob_uform + size_glob_fform + size_glob_pform;

            //int  composed_loc_map_size  = size_loc_uform;
            int  composed_loc_map_size  = size_loc_uform  + size_loc_fform  + size_loc_pform;

            composed_map_indices   =  new int[composed_loc_map_size];

            int * indices_uform        =  map_uform.MyGlobalElements();
            int * indices_fform        =  map_fform.MyGlobalElements();
            int * indices_pform        =  map_pform.MyGlobalElements();


            for (int i=0; i<size_loc_uform; i++)
                composed_map_indices[i]  =  indices_uform[i];

            for (int i=0; i<size_loc_fform; i++)
                composed_map_indices[size_loc_uform + i]  =  size_glob_uform  +  indices_fform[i];

            for (int i=0; i<size_loc_pform; i++)
                composed_map_indices[ size_loc_uform + size_loc_fform + i ]
                        = size_glob_uform + size_glob_fform + indices_pform[i];

            composed_map = new Epetra_Map(composed_glob_map_size, composed_loc_map_size, composed_map_indices, 0, comm);

            X_uform = new Epetra_Vector(map_uform);
            X_fform = new Epetra_Vector(map_fform);
            X_pform = new Epetra_Vector(map_pform);

            tmp_uform = new Epetra_Vector(map_uform);
            tmp_fform = new Epetra_Vector(map_fform);
            tmp_pform = new Epetra_Vector(map_pform);

            /// creation of the preconditioners

            // allocate the ML preconditioner

            MS_pp = new ML_Epetra::MultiLevelPreconditioner(S_pp, MLList_Spp, true);
            if (comm.MyPID()==0) cout<<"%*** INFO ***: MS_pp is created"<<endl;

            MA_uu = new ML_Epetra::MultiLevelPreconditioner(A_uu, MLList, true);
            ///MA_uu = new ML_Epetra::MultiLevelPreconditioner(A_uu, MLList, true);
            if (comm.MyPID()==0) cout<<"%*** INFO ***: MA_uu is created"<<endl;


            /// an idea to try diag of Spp instead of MS_pp.

            //Sp = new Epetra_Vector(S_pp.DomainMap());

            //S_pp.ExtractDiagonalCopy(*Sp);


            mySppProblem = new Epetra_LinearProblem;
            mySppProblem->SetOperator(&S_pp);

            myAuuProblem = new Epetra_LinearProblem;
            myAuuProblem->SetOperator(&A_uu);

            myAffProblem = new Epetra_LinearProblem;
            myAffProblem->SetOperator(&A_ff);

            /// IFPACK Preconditioner
            Teuchos::ParameterList List;

            Ifpack iFactory;

            //string PrecType = "IC";
            string PrecType = "Amesos";
            int OverlapLevel = 0;

            Prec = iFactory.Create(PrecType, &A_ff, OverlapLevel);

            // specify parameters for IC
            //List.set("fact: drop tolerance", 1e-9);
            List.set("fact: level-of-fill", 1); /// 0 returns only the diagonal.

            // the combine mode is on the following:
            // "Add", "Zero", "Insert", "InsertAdd", "Average", "AbsMax"
            List.set("schwarz: combine mode", "Zero");

            //List.set("schwarz: reordering type", "rcm");


            Prec->SetParameters(List);

            Prec->Initialize();

            Prec->Compute();

            total_auu_iters=0;
            total_aff_iters=0;
            total_spp_iters=0;

            mySppProblem->SetLHS(tmp_pform);
            mySppProblem->SetRHS(X_pform);
            mySppSolver = new AztecOO(*mySppProblem);
            mySppSolver->SetAztecOption(AZ_solver, AZ_cg);
            mySppSolver->SetAztecOption(AZ_output, AZ_none);
            mySppSolver->SetPrecOperator(MS_pp);

            myAuuProblem->SetLHS(tmp_uform);
            myAuuProblem->SetRHS(tmp_uform);
            myAuuSolver = new AztecOO(*myAuuProblem);
            myAuuSolver->SetAztecOption(AZ_solver, AZ_cg);
            myAuuSolver->SetAztecOption(AZ_output, AZ_none);
            myAuuSolver->SetPrecOperator(MA_uu);

            myAffProblem->SetLHS(tmp_fform);
            myAffProblem->SetRHS(tmp_fform);
            myAffSolver = new AztecOO(*myAffProblem);
            myAffSolver->SetAztecOption(AZ_solver, AZ_cg);
            myAffSolver->SetAztecOption(AZ_output, AZ_none);
            myAffSolver->SetAztecOption(AZ_precond, AZ_none);
            myAffSolver->SetPrecOperator(Prec);

        }



        PoroMatrixPreconditionerB::~PoroMatrixPreconditionerB() {

            delete[] composed_map_indices;
            delete composed_map;
            delete MA_uu;
            delete MS_pp;
            delete mySppProblem;
            delete myAuuProblem;
            delete myAffProblem;
            delete mySppSolver;
            delete myAuuSolver;
            delete myAffSolver;

            delete Prec;

            delete X_uform;
            delete X_fform;
            delete X_pform;
            delete tmp_uform;
            delete tmp_fform;
            delete tmp_pform;


        }

        void PoroMatrixPreconditionerB::myPrint() {

            std::cout<<"/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*"<<std::endl;
        }



        int PoroMatrixPreconditionerB::Apply(const Epetra_MultiVector& X, Epetra_MultiVector& Y) const {

            return -1;
        }



        int PoroMatrixPreconditionerB::ApplyInverse(const Epetra_MultiVector& X, Epetra_MultiVector& Y) const {

            //Y.PutScalar(0.0);
            PoroMatrixPreconditionerB * This = const_cast<PoroMatrixPreconditionerB *>(this);

            Y=X;

            //   const Epetra_Vector* X_vec = dynamic_cast<const Epetra_Vector*>(&X);
            //        Epetra_Vector* Y_vec = dynamic_cast<      Epetra_Vector*>(&Y);
            const Epetra_Vector* X_vec = X(0);
            Epetra_Vector* Y_vec  = Y(0);

            // Extract the partial vectors from X
            for (int i=0; i<size_loc_uform; i++)  (*X_uform)[i] = (*X_vec)[i                                  ];
            for (int i=0; i<size_loc_fform; i++)  (*X_fform)[i] = (*X_vec)[i + size_loc_uform                 ];
            for (int i=0; i<size_loc_pform; i++)  (*X_pform)[i] = (*X_vec)[i + size_loc_uform + size_loc_fform];

            //ResetBoundary_u(X_uform);
            //ResetBoundary_f(X_fform);

            int auu_iters;
            int aff_iters;
            int spp_iters;

            ///*
            ///*************************************************************
            /// Step 1 - compute yp.

            mySppSolver->Iterate (MaxSppIter, SppTol); /// 3 is enough with 1e-5 Az_r0

            *tmp_pform=*mySppProblem->GetLHS()->operator()(0);

            for (int i=0; i<size_loc_pform; i++){
                    //tmp_pform[i]  = X_pform[i]/A_p[i]; /// comment to use Spp instead of A_p
                    (*Y_vec)[ i + size_loc_uform + size_loc_fform ]   = (*tmp_pform)[i];
            }

            spp_iters = mySppSolver->NumIters();
            This->total_spp_iters += spp_iters;

            ///*************************************************************
            /// Step 2 - update tmpu and tmpf.

            A_pu.Multiply(true , *tmp_pform  ,  *tmp_uform ); /// there is no Y_pform but yp = tmp_p after step 1.
            for (int i=0; i<size_loc_uform; i++) {
                    (*tmp_uform)[i]                    = (*X_uform)[i] - (*tmp_uform)[i];
            }
            //tmp_uform->Update(1.0, *X_uform,-1.0);

            /// A_ff.Multiply(false , tmp_fform  ,  tmp_fform ); /// yf is already zero since not yet calculated. so avoid this multiplication.

            A_pf.Multiply(true , *tmp_pform  ,  *tmp_fform ); /// there is no Y_pform but yp = tmp_p after step 1.


            for (int i=0; i<size_loc_fform; i++) {
                    (*tmp_fform)[i]                    = (*X_fform)[i] - (*tmp_fform)[i];
            }
            //tmp_fform->Update(1.0, *X_fform,-1.0);

            ///*************************************************************
            /// Step 3 - calculate yu.

            myAuuSolver->Iterate (MaxAuuIter, AuuTol); /// 25 iters seem to be working well

            *tmp_uform=*myAuuProblem->GetLHS()->operator()(0);

            for (int i=0; i<size_loc_uform; i++){
                    (*Y_vec)[ i ] = (*tmp_uform)[i];
            }

            auu_iters = myAuuSolver->NumIters();

            This->total_auu_iters += auu_iters;

            ///*************************************************************
            /// Step 4 - calculate yf.

            /// apply IC only.
            //Prec->ApplyInverse(tmp_fform,tmp_fform);

            myAffSolver->Iterate (MaxAffIter, AffTol); /// 3 is enough with 1e-5 Az_r0

            *tmp_fform=*myAffProblem->GetLHS()->operator()(0);

            for (int i=0; i<size_loc_fform; i++){
                    (*Y_vec)[ i + size_loc_uform ]  = (*tmp_fform)[i];
            }

            aff_iters = myAffSolver->NumIters();
            This->total_aff_iters += aff_iters;

            return 0;
        }

        int PoroMatrixPreconditionerB::totalAuuIters() {

            return total_auu_iters;
            //return 0;
        }

        int PoroMatrixPreconditionerB::totalAffIters() {

            return total_aff_iters;
            //return 0;
        }

        int PoroMatrixPreconditionerB::totalSppIters() {

            return total_spp_iters;
            //return 0;
        }

