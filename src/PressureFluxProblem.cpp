/*
 * PorFE: a poroelastic extension on ParFE
 * Copyright (C) 2012, Erhan Turan
 * https://bitbucket.org/erhanturan/porfe
 *
 * ParFE: a micro-FE solver for trabecular bone modeling
 * Copyright (C) 2011,  ParFE developers, see
 * http://parfe.sourceforge.net/developers.php
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#include <Epetra_FECrsGraph.h>
//#include <Epetra_CrsMatrix.h>
#include <Epetra_MultiVector.h>
#include "PressureFluxProblem.h"
#include <cmath>
#include "MATLAB_VectorWriter.hpp"

PressureFluxProblem::PressureFluxProblem(DistMesh& a_mesh, Teuchos::ParameterList& fe_param, double a_deltaT)
  : ElasticityProblem(a_mesh, fe_param)
{

  int NumPressurePerElement  = 1; /// might be defined within the mesh file but written here explicitly.
                                  /// similar idea of NumDofsPerNode from nr_dofs_per_node could be used.

  //pressure_map = new Epetra_Map(*mesh.SchurMap());
  pressure_map = new Epetra_Map(-1, mesh.SchurMap()->NumMyElements(), mesh.SchurMap()->MyGlobalElements(), 0, comm);

  Epetra_Vector* X = new Epetra_Vector(*pressure_map);
  Epetra_Vector* B = new Epetra_Vector(*pressure_map);
  //SetOperator(schur_mat);
//  SetOperator(block_mat);
  SetLHS(X);
  SetRHS(B);

  deltaT = a_deltaT;
  Assemble();
}

PressureFluxProblem::~PressureFluxProblem()
{
  delete GetLHS();
  delete GetRHS();
  delete GetOperator();
}



int PressureFluxProblem::Assemble()
{

///When GlobalAssemble() calls FillComplete(), it passes the arguments 'DomainMap()' and 'RangeMap()',
///which are the map attributes held by the base-class CrsMatrix and its graph. If a rectangular
///matrix is being assembled, the correct domain-map and range-map must be passed to
///GlobalAssemble (there are two overloadings of this method) -- otherwise, it has no way of knowing
/// what these maps should really be.

///Note that, even after a matrix is constructed (FillComplete has been called), it is possible
///to update existing matrix entries. It is not possible to create new entries.




   // Schur App - BF in poisson matrix format.
   // Se*a^3 - (mu/k)^-1 * 15 * a * alpha

  const Epetra_Map pressure_map_column(-1, mesh.FaceMap()->NumMyElements(), mesh.FaceMap()->MyGlobalElements(), 0, comm);

  // create the matrix with the nonzero pattern.
  PF = new Epetra_CrsMatrix(Copy,*pressure_map,6);


  int NumGlobalElements = pressure_map->NumGlobalElements();
  int NumMyElements = pressure_map->NumMyElements();
  int* values = mesh.ElementFaces()->Values();
  int ElementSize = mesh.ElementFaces()->Map().ElementSize();


  double* offValues  = new double[6];
  ///*
  offValues[0]=EdgeLength*deltaT;
  offValues[1]=-EdgeLength*deltaT;
  offValues[2]=-EdgeLength*deltaT;
  offValues[3]=EdgeLength*deltaT;
  offValues[4]=EdgeLength*deltaT;
  offValues[5]=-EdgeLength*deltaT;
  //*/


  int* offIndices;

  int * MyGlobalElements = pressure_map->MyGlobalElements();

  offIndices  = new int[6]; /// indices off-diagonal terms

  for (int i = 0; i < NumMyElements; i++){


        for (int j = 0; j < ElementSize; j++){
                offIndices[j] = values[i*ElementSize+j]-1;
        }

         PF->InsertGlobalValues(MyGlobalElements[i], 6, offValues, offIndices);

  }

  delete[] offIndices;
  delete[] offValues;

  PF->FillComplete(pressure_map_column,*pressure_map);
  PF->OptimizeStorage();


  /// new version for the bc application - CORRECTED!

  Epetra_Map fluxMap(-1, mesh.FaceMap()->NumMyElements(), mesh.FaceMap()->MyGlobalElements(), 0, comm);

  int num_myglobals = fluxMap.NumMyElements();

  int* my_globals = new int[num_myglobals];

  my_globals = fluxMap.MyGlobalElements();


  std::vector<int> node_vec;
  int* PIDList = new int[num_myglobals];
  mesh.FixedFaces()->Map().RemoteIDList(num_myglobals, my_globals, PIDList, NULL);
  for (int i=0; i<num_myglobals; ++i)
    if (PIDList[i] >= 0)
      node_vec.push_back(my_globals[i]);


  ///Epetra_BlockMap new_fixed_faces_map(-1, node_vec.size(), &node_vec[0], 1, 0, comm);
  Epetra_BlockMap* new_fixed_faces_map = new Epetra_BlockMap(-1, node_vec.size(), &node_vec[0], 1, 0, comm);

  Epetra_Export exporter(mesh.FixedFaces()->Map(), *new_fixed_faces_map);

  Epetra_IntVector* new_fixed_faces = new Epetra_IntVector(*new_fixed_faces_map);
  Epetra_IntVector* new_fixed_face_values = new Epetra_IntVector(*new_fixed_faces_map);

  new_fixed_faces->Export(*mesh.FixedFaces(), exporter, Insert);
  new_fixed_face_values->Export(*mesh.FixedFaceValues(), exporter, Insert);

  delete[] PIDList;
  my_globals = NULL;
  delete[] my_globals;
  delete new_fixed_faces_map;
  /// ////////////////////
  int num_mygids;
  int num_myvalues;
  int* gids;
  int* myvalues = new_fixed_face_values->Values();

  // apply bc values. - should update Apf, as well. careful with Bp.

  Epetra_Vector corr(pressure_map_column);
  Epetra_Vector fixed_faces(fluxMap);
  num_mygids = new_fixed_face_values->Map().NumMyElements();
  num_myvalues = new_fixed_face_values->MyLength();
  gids = new_fixed_face_values->Map().MyGlobalElements();

  // non-zero flux bc's are not tested!
  for (int i=0; i<num_mygids; ++i) {

	fixed_faces.ReplaceGlobalValue(gids[i], 0, myvalues[i]);

  }

  Epetra_Vector* B = dynamic_cast<Epetra_Vector*>(GetRHS());
  PF->Multiply(false, fixed_faces, corr);
  B->Update(-1, corr, 1);



  Epetra_Vector bc_filter(pressure_map_column);
  bc_filter.PutScalar(1.0);



  num_mygids = new_fixed_faces->Map().NumMyElements();
  num_myvalues = new_fixed_faces->MyLength();
  gids = new_fixed_faces->Map().MyGlobalElements();


  for (int i=0; i<num_mygids; ++i) {

	bc_filter.ReplaceGlobalValue(gids[i], 0, 0, 0.0);

  }

  //filter out bc by applying rightscale only to the Matrix.
  // Since this is an off-diagonal submatrix, only the columns will be zeroed.
  PF->RightScale(bc_filter);
  delete new_fixed_faces;
  delete new_fixed_face_values;

  return 0;


}


int PressureFluxProblem::Impose(BoundaryCondition& bcond){}
int PressureFluxProblem::Restore(){}


Epetra_CrsMatrix& PressureFluxProblem::getMatrix(){

    return *PF;


    }
