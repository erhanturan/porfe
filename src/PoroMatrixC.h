/*
 * PorFE: a poroelastic extension on ParFE
 * Copyright (C) 2012, Erhan Turan
 * https://bitbucket.org/erhanturan/porfe
 *
 * ParFE: a micro-FE solver for trabecular bone modeling
 * Copyright (C) 2011,  ParFE developers, see
 * http://parfe.sourceforge.net/developers.php
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#ifndef _PORO_MATRIXC_H_
#define _PORO_MATRIXC_H_

#include <Epetra_Map.h>
#include <Epetra_Vector.h>
#include <Epetra_Operator.h>
#include <Epetra_RowMatrix.h>
#include <Epetra_FECrsMatrix.h>
#include <Epetra_MpiComm.h>

///#include <Epetra_VbrMatrix.h> /// erhan added
#include <Epetra_FEVbrMatrix.h> /// erhan added
///#include <Epetra_BlockMap.h>

#include "BoundaryCondition.h"




/// class example is taken by Marcus Wittberger and changed for Poroelasticity problem.
/// this class enables us to perform matrix-vector product using the submatrices defined inf ParFE.


/*!
 *
 *
 *
 */
class PoroMatrixC : public Epetra_Operator, public Epetra_SrcDistObject {

public:

    //! PoroMatrixC constructor
    /*!
     *  \param a_dof_map
     *  (In) A Epetra_BlockMap indicating how the DOFs are distributed
     *
     *  \param element_matrix
     *  (In) A ElementIntergrator object that is able to compute any needed element stiffness matrices.
     *  Normally, the element stiffness matrix has to be computed only once.
     *
     *  \param elem2node
     *  (In) This Epetra_IntVector contains the element-to-node table.
     *
     *  \param mat_ids
     *  (In) This Epetra_IntVector contains the material IDs.
     *
     *  \return Pointer to the PoroMatrixB object.
     */
    /// //TODO: not every matrix is FECrsMatrix!!!
    PoroMatrixC( Epetra_RowMatrix &  A_uu_,
            BoundaryCondition & bcond,
            const Epetra_MpiComm comm_);


    //! PoroMatrixC destructor
    ~PoroMatrixC();

    //! Do not use the transpose while applying
    int SetUseTranspose(bool UseTranspose) { return -1; };


    //! Returns the result of the PoroMatrixB multiplied with a Epetra_MultiVector U in V.
    /*!
     *  \param U
     *  (In) A Epetra_Vector of dimension NumVectors to be multiplied by the PoroMatrixB.
     *
     *  \param V
     *  (Out) A Epetra_Vector of dimension NumVectors containing the result.
     *
     *  \return Integer error code, set to 0 if successful.
     */
    int Apply(const Epetra_MultiVector& X, Epetra_MultiVector& Y) const;
    //int Apply(const Epetra_MultiVector& X, Epetra_MultiVector& Y) const;

    //! Returns -1. Only implemented for convenience.
    int ApplyInverse(const Epetra_MultiVector& X, Epetra_MultiVector& Y) const;

    //! Returns -1.0. Only implemented for convenience.
    double NormInf()                      const { return -1.0; };

    //! Returns a character string describing the operator.
    const char* Label()                   const { return "PoroMatrixB"; };

    //! Returns false
    bool UseTranspose()                   const { return false; };

    //! Returns false
    bool HasNormInf()                     const { return false; };

    //! Returns the Epetra_Comm object associated with this operator.
    const Epetra_Comm& Comm()             const { return comm; };

    //! Returns the Epetra_Map object associated with the domain of this operator.
    const Epetra_Map& OperatorDomainMap() const { return (*composed_map); };

    //! Returns the Epetra_Map object associated with the range of this operator.
    const Epetra_Map& OperatorRangeMap()  const { return (*composed_map); };

    //! Returns the Epetra_BlockMap object associated with this operator.
    const Epetra_BlockMap& Map()          const {
        return (*composed_map);
    };




    Epetra_Vector compose_vector( const Epetra_Vector& vec_uform);


    void extract_partial_vectors( Epetra_Vector&      vec_uform,   const Epetra_Vector& vec            ) const;


    void myPrint();


private:


    const Epetra_BlockMap map_uform;


    Epetra_RowMatrix& A_uu;

    BoundaryCondition & bcond;





    const Epetra_MpiComm  comm;

    int*        composed_map_indices;
    Epetra_Map*  composed_map;


    int size_glob_uform;

    int size_loc_uform;

};



#endif

