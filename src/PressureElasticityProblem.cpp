/*
 * PorFE: a poroelastic extension on ParFE
 * Copyright (C) 2012, Erhan Turan
 * https://bitbucket.org/erhanturan/porfe
 *
 * ParFE: a micro-FE solver for trabecular bone modeling
 * Copyright (C) 2011,  ParFE developers, see
 * http://parfe.sourceforge.net/developers.php
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#include <Epetra_FECrsGraph.h>
#include <Epetra_MultiVector.h>
#include "PressureElasticityProblem.h"
#include "Tools.h"
#include "MATLAB_VectorWriter.hpp"

PressureElasticityProblem::PressureElasticityProblem(DistMesh& a_mesh, BoundaryCondition& a_bcond, Teuchos::ParameterList& fe_param, const Epetra_BlockMap& a_u_map)
  : ElasticityProblem(a_mesh, fe_param), bcond(a_bcond), u_map(a_u_map)
{
  //Create block map with degrees of freedoms
  //Epetra_BlockMap row_map(-1, mesh.NodeMap()->NumMyElements(), mesh.NodeMap()->MyGlobalElements(), NumDofsPerNode, 0, comm);


  pressure_map = new Epetra_Map(-1, mesh.SchurMap()->NumMyElements(), mesh.SchurMap()->MyGlobalElements(), 0, comm);

  Epetra_Vector* X = new Epetra_Vector(*pressure_map);
  Epetra_Vector* B = new Epetra_Vector(*pressure_map);
  //SetOperator(block_mat);
  SetLHS(X);
  SetRHS(B);


  InitializeMaterialProperties();

  Assemble();

}

PressureElasticityProblem::~PressureElasticityProblem()
{
  delete GetLHS();
  delete GetRHS();
  delete GetOperator();
}


int PressureElasticityProblem::Assemble()
{


///When GlobalAssemble() calls FillComplete(), it passes the arguments 'DomainMap()' and 'RangeMap()',
///which are the map attributes held by the base-class CrsMatrix and its graph. If a rectangular
///matrix is being assembled, the correct domain-map and range-map must be passed to
///GlobalAssemble (there are two overloadings of this method) -- otherwise, it has no way of knowing
/// what these maps should really be.


   // Schur App - BF in poisson matrix format.
   // Se*a^3 - (mu/k)^-1 * 15 * a * alpha


///I believe the order should be 0 0 0 1 1 1 ... 7 7 7



  /// this map defines the row number
  ///const Epetra_Map pressure_map(mesh.SchurMap()->NumGlobalElements(), 0, comm);

  ///const Epetra_Map pressure_map(mesh.SchurElementMap()->NumGlobalElements(),mesh.SchurElementMap()->NumMyElements(), mesh.SchurElementMap()->MyGlobalElements(), 0, comm);


  const Epetra_Map pressure_map_column(-1, u_map.NumMyElements(), u_map.MyGlobalElements(), 0, comm);


  PE = new Epetra_CrsMatrix(Copy,*pressure_map,24);


  int NumGlobalElements = pressure_map->NumGlobalElements();
  int NumMyElements = pressure_map->NumMyElements();
  int* values = mesh.ElementNodesFace()->Values();
  int ElementSize = mesh.ElementNodesFace()->Map().ElementSize();

  alpha = 1.0;
  double ApuTerm = EdgeLength*EdgeLength*alpha/4.0;


  double localPF_vec[24]={-ApuTerm, -ApuTerm, -ApuTerm, ApuTerm, -ApuTerm, -ApuTerm,
                          ApuTerm, ApuTerm, -ApuTerm, -ApuTerm, ApuTerm, -ApuTerm,
                          -ApuTerm, -ApuTerm, ApuTerm, ApuTerm, -ApuTerm, ApuTerm,
                          ApuTerm, ApuTerm, ApuTerm, -ApuTerm, ApuTerm, ApuTerm};

/*
  double localPF_vec[24]={-ApuTerm, -ApuTerm, -ApuTerm, -ApuTerm, -ApuTerm, ApuTerm,
                          ApuTerm, -ApuTerm, ApuTerm, ApuTerm, -ApuTerm, -ApuTerm,
                          -ApuTerm, ApuTerm, -ApuTerm, -ApuTerm, ApuTerm, ApuTerm,
                          ApuTerm, ApuTerm, ApuTerm, ApuTerm, ApuTerm, -ApuTerm}; */



  double* offValues  = new double[24];

  for (int i = 0; i < 24 ; i++)
    offValues[i]=-localPF_vec[i];
    ///offValues[i]=localPF_vec[i];


  int* offIndices;

  int * MyGlobalElements = pressure_map->MyGlobalElements();


  int nodeIndex;
  int myCount;
  offIndices  = new int[24]; /// indices off-diagonal terms

  for (int i = 0; i < NumMyElements; i++){

        myCount=0;

        for (int j = 0; j < ElementSize; j++){
            ///nodeIndex = values[i*ElementSize+j]-1;  /// no -1 is needed here because the values are read by substracting -1
            nodeIndex = values[i*ElementSize+j];  /// although in the mesh file we see it started from 1

            for (int k=0;k<3;k++){
                offIndices[myCount] =nodeIndex*3+k;
                myCount++;
            }
        }

        /// insert off-diagonal values. ///

         PE->InsertGlobalValues(MyGlobalElements[i], 24, offValues, offIndices);
         //PEF->InsertGlobalValues(MyGlobalElements[i], 24, offValues, offIndices);

  }

  delete[] offIndices;
  delete[] offValues;


  PE->FillComplete(pressure_map_column,*pressure_map);
  PE->OptimizeStorage();
    //PEF->GlobalAssemble(pressure_map_column,*pressure_map);


/// impose bc //////////////////
//apply the similar idea as in VbrElasticityProblem but directly during the assembly.

///return 0;


  Epetra_Vector bc_filter(pressure_map_column);

  bc_filter.PutScalar(1.0);

  int num_mygids;
  int num_myvalues;
  int* gids;


  num_mygids = bcond.RestrainedNodes()->Map().NumMyElements();
  num_myvalues = bcond.RestrainedNodes()->MyLength();
  gids = bcond.RestrainedNodes()->Map().MyGlobalElements();
  int* ivalues = bcond.RestrainedNodes()->Values();

  for (int i=0; i<num_mygids; ++i) {
    for (int j=0; j<NumDofsPerNode; ++j) {
      if (ivalues[j] == 0) {
	bc_filter.ReplaceGlobalValue(gids[i]*NumDofsPerNode+ j,  0, 0.0);

      }
    }
    ivalues += NumDofsPerNode;
  }

  double* fvalues = bcond.FixedNodes()->Values();

  num_mygids = bcond.FixedNodes()->Map().NumMyElements();
  num_myvalues = bcond.FixedNodes()->MyLength();
  gids = bcond.FixedNodes()->Map().MyGlobalElements();
  fvalues = bcond.FixedNodes()->Values();

  for (int i=0; i<num_mygids; ++i) {
    for (int j=0; j<NumDofsPerNode; ++j) {
      if (fvalues[j] != 0) {
	bc_filter.ReplaceGlobalValue(gids[i]*NumDofsPerNode+ j,  0, 0.0);

      }
    }
    fvalues += NumDofsPerNode;
  }


/*
  VectorWriter* myMatlabVec = 0;
  myMatlabVec = new MATLAB_VectorWriter<C_ASCII_GWriter>("bc_filter", comm);
  myMatlabVec->PrintVector(bc_filter);
*/


  //filter out bc by applying rightscale only to the Matrix.
  // Since this is an off-diagonal submatrix, only the columns will be zeroed.
  PE->RightScale(bc_filter);



return 0;


}




int PressureElasticityProblem::Impose(BoundaryCondition& bcond)
{

  return 0;
}


int PressureElasticityProblem::Restore()
{

  return 0;
}



void PressureElasticityProblem::InitializeMaterialProperties(){


  elastic_modulus         = MaterialProperties.A()[0];
  poisson_ratio           = MaterialProperties.A()[1];
  bulk_modulus_solid      = MaterialProperties.A()[4];
  bulk_modulus            = ComputeBulkModulus();
  alpha                   = ComputeAlpha();


}


double PressureElasticityProblem::ComputeBulkModulus(){

    return elastic_modulus/(3.0*(1.0-2.0*poisson_ratio));

}

double PressureElasticityProblem::ComputeAlpha(){

    return 1.0-bulk_modulus/bulk_modulus_solid;

}

Epetra_CrsMatrix& PressureElasticityProblem::getMatrix(){

    return *PE;


    }

int PressureElasticityProblem::BlockMap2PointMap(const Epetra_BlockMap& BlockMap, Epetra_Map * & PointMap) const
{
  // Generate an Epetra_Map that has the same number and distribution of points
  // as the input Epetra_BlockMap object.  The global IDs for the output PointMap
  // are computed by using the MaxElementSize of the BlockMap.  For variable block
  // sizes this will create gaps in the GID space, but that is OK for Epetra_Maps.

  int MaxElementSize = BlockMap.MaxElementSize();
  int PtNumMyElements = BlockMap.NumMyPoints();
  int * PtMyGlobalElements = 0;
  if (PtNumMyElements>0) PtMyGlobalElements = new int[PtNumMyElements];

  int NumMyElements = BlockMap.NumMyElements();

  int curID = 0;
  for (int i=0; i<NumMyElements; i++) {
    int StartID = BlockMap.GID(i)*MaxElementSize;
    int ElementSize = BlockMap.ElementSize(i);
    for (int j=0; j<ElementSize; j++) PtMyGlobalElements[curID++] = StartID+j;
  }
  assert(curID==PtNumMyElements); // Sanity test

  PointMap = new Epetra_Map(-1, PtNumMyElements, PtMyGlobalElements, BlockMap.IndexBase(), BlockMap.Comm());

  if (PtNumMyElements>0) delete [] PtMyGlobalElements;

  if (!BlockMap.PointSameAs(*PointMap)) {EPETRA_CHK_ERR(-1);} // Maps not compatible
  return(0);

}

