/*
 * PorFE: a poroelastic extension on ParFE
 * Copyright (C) 2012, Erhan Turan
 * https://bitbucket.org/erhanturan/porfe
 *
 * ParFE: a micro-FE solver for trabecular bone modeling
 * Copyright (C) 2011,  ParFE developers, see
 * http://parfe.sourceforge.net/developers.php
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#include "parfe_ConfigDefs.h"
#include "Tools.h"
#if defined(HAVE_MALLINFO)
#include <malloc.h>
#elif defined(HAVE_HEAP_INFO)
#include <catamount/catmalloc.h>
#endif
#include "DistMesh.h"


#include "Epetra_Map.h"
#include "Epetra_Vector.h"
#include "Epetra_CrsMatrix.h"










unsigned meminfo()
{
#if defined(HAVE_MALLINFO)
    long int fragments, total_free, largest_free, total_used;
    /* int percent; */

    /* use system call to get memory used information */

    struct mallinfo M = mallinfo();
    fragments = M.ordblks + M.smblks + M.hblks;
    total_free = M.fsmblks + M.fordblks;
    total_used = M.hblkhd + M.usmblks + M.uordblks;
    /*  total_free = ml_total_mem - total_used;  */
    largest_free = -1024;

    /* convert to Mbytes */

    return( (unsigned)(total_used/(1024*1024)) );

#elif defined(HAVE_HEAP_INFO)
    size_t fragments;
    unsigned long total_free;
    unsigned long largest_free;
    unsigned long total_used;

    heap_info(&fragments, &total_free, &largest_free, &total_used);

    return( (unsigned)(total_used/(1024*1024)) );

#else
    return(0);
#endif
}

// ============================================================================
void set_null_space(DistMesh* mesh, vector<double>& null_space)
{
  int num_rows =  mesh->NodeMap()->NumMyPoints();
  null_space.resize(6 * num_rows);

  double* row_it = &null_space[0];

  //clear vector
  for (double* it = &null_space[0]; it < &null_space[0] + 6 * num_rows; ++it)
    *it = 0;

  //first coordinate is constant
  for (double* it = row_it; it < row_it + num_rows; it += 3)
    *it = 1;

  //second coordinate is constant
  row_it += num_rows;
  for (double* it = row_it+1;it < row_it+num_rows; it+=3)
    *it = 1;

  //third coordinate is constant
  row_it += num_rows;
  for (double* it = row_it+2;it < row_it+num_rows; it+=3)
    *it = 1;

  // u = y and v = -x
  double* values = mesh->Coordinates()->Values();
  row_it += num_rows;
  for (double* it = row_it;it < row_it+num_rows; it+=3) {
    *it = *(values+1);
    *(it+1) = -*values;
    values+=3;
  }

  // v = z and w = -y
  values = mesh->Coordinates()->Values();
  row_it += num_rows;
  for (double* it = row_it;it < row_it+num_rows; it+=3) {
    *(it+1) = *(values+2);
    *(it+2) = -*(values+1);
    values+=3;
  }

  // u = z and w = -x
  values = mesh->Coordinates()->Values();
  row_it += num_rows;
  for (double* it = row_it;it < row_it+num_rows; it+=3) {
    *it = *(values+2);
    *(it+2) = -*values;
    values+=3;
  }
}

void wait ( double seconds )
{
  clock_t endwait;
  endwait = clock () + seconds * CLOCKS_PER_SEC ;
  while (clock() < endwait) {}
}


bool CrsMatrix2MATLAB( const Epetra_CrsMatrix & A )

{

  int MyPID = A.Comm().MyPID();
  int NumProc = A.Comm().NumProc();

  // work only on transformed matrices;
  if( A.IndicesAreLocal() == false ) {
    if( MyPID == 0 ) {
      cerr << "ERROR in "<< __FILE__ << ", line " << __LINE__ << endl;
      cerr << "Function CrsMatrix2MATLAB accepts\n";
      cerr << "transformed matrices ONLY. Please call A.FillComplete()\n";
      cerr << "on your matrix A to that purpose.\n";
      cerr << "Now returning...\n";
    }
    return false;
  }

  int NumMyRows = A.NumMyRows(); // number of rows on this process
  int NumNzRow;   // number of nonzero elements for each row
  int NumEntries; // number of extracted elements for each row
  int NumGlobalRows; // global dimensio of the problem
  int GlobalRow;  // row in global ordering
  int NumGlobalNonzeros; // global number of nonzero elements

  NumGlobalRows = A.NumGlobalRows();
  NumGlobalNonzeros = A.NumGlobalNonzeros();

  // print out on cout if no filename is provided

  int IndexBase = A.IndexBase(); // MATLAB start from 0
  if( IndexBase == 0 ) IndexBase = 1;

  // write on file the dimension of the matrix

  if( MyPID==0 ) {
    cout << "A = spalloc(";
    cout << NumGlobalRows << ',' << NumGlobalRows;
    cout << ',' << NumGlobalNonzeros << ");\n";
  }

  for( int Proc=0 ; Proc<NumProc ; ++Proc ) {

    if( MyPID == Proc ) {

      cout << "% On proc " << Proc << ": ";
      cout << NumMyRows << " rows and ";
      cout << A.NumMyNonzeros() << " nonzeros\n";

      // cycle over all local rows to find out nonzero elements
      for( int MyRow=0 ; MyRow<NumMyRows ; ++MyRow ) {

	GlobalRow = A.GRID(MyRow);

	NumNzRow = A.NumMyEntries(MyRow);
	double *Values = new double[NumNzRow];
	int *Indices = new int[NumNzRow];

	A.ExtractMyRowCopy(MyRow, NumNzRow,
			   NumEntries, Values, Indices);
	// print out the elements with MATLAB syntax
	for( int j=0 ; j<NumEntries ; ++j ) {
	  cout << "A(" << GlobalRow  + IndexBase
	       << "," << A.GCID(Indices[j]) + IndexBase
	       << ") = " << Values[j] << ";\n";
	}

	delete Values;
	delete Indices;
      }

    }
    A.Comm().Barrier();
    if( MyPID == 0 ) {
      cout << " %End of Matrix Output\n";
    }
  }

  return true;

}

//int initialize_parameters(Teuchos::ParameterList& fe_param)
materials calculate_parameters( const Epetra_SerialDenseMatrix & MaterialProperties)
{

materials mm;


mm.elastic_modulus      = mm.E   = MaterialProperties.A()[0]; ///
mm.poisson_ratio        = mm.v   = MaterialProperties.A()[1]; ///
mm.permeability         = mm.k   = MaterialProperties.A()[2]; ///
mm.viscosity            = mm.eta = MaterialProperties.A()[3]; ///
mm.bulk_modulus_solid   = mm.Ks  = MaterialProperties.A()[4]; ///
mm.bulk_modulus_fluid   = mm.Kf  = MaterialProperties.A()[5]; ///
mm.porosity             = mm.phi = MaterialProperties.A()[6]; ///

mm.mu                   = mm.G   = mm.E/(2.0*(1.0+mm.v)); ///
mm.lambda               = mm.E*mm.v/((1.0+mm.v)*(1.0-2.0*mm.v)); ///
mm.bulk_modulus         = mm.K   = mm.E/(3.0*(1.0-2.0*mm.v));
mm.alpha                = 1.0-mm.K/mm.Ks;
mm.alpha                = 1.0; /// explicitly defined.


mm.Se                   = 1.0/mm.Ks*(1.0-mm.K/mm.Ks)+mm.porosity*(1.0/mm.Kf-1.0/mm.Ks);

mm.B                    = 0.9891; /// for sand
mm.B                    = 0.989119683481695; /// for sand - add betaf*phi related biot compressibility calculation for M, then vu.

mm.poisson_ratio_u      = mm.vu  = (3.0*mm.v + mm.alpha*mm.B*(1.0 - 2.0*mm.v))/(3.0 - mm.alpha*mm.B*(1.0 - 2.0*mm.v));



mm.M                    = 2.0*mm.G*(mm.vu-mm.v)/(mm.alpha*mm.alpha*(1.0-2.0*mm.vu)*(1.0-2.0*mm.v));
mm.Kappa                = mm.k/mm.eta;
mm.S                    = (1.0-mm.vu)*(1.0-2.0*mm.v)/(mm.M*(1.0-mm.v)*(1.0-2.0*mm.vu));
mm.c                    = mm.Kappa/mm.S;

mm.Ku                   = mm.K/(1.0-mm.alpha*mm.B);

return mm;


}

