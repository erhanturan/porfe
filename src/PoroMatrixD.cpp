/*
 * PorFE: a poroelastic extension on ParFE
 * Copyright (C) 2012, Erhan Turan
 * https://bitbucket.org/erhanturan/porfe
 *
 * ParFE: a micro-FE solver for trabecular bone modeling
 * Copyright (C) 2011,  ParFE developers, see
 * http://parfe.sourceforge.net/developers.php
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#include <Epetra_Map.h>
#include <Epetra_Vector.h>
#include "PoroMatrixD.h"
///#include <Epetra_BlockMap.h>

#include "ElementByElementMatrix.h"

/// send  App as vector, others as CRS or FECrs matrices.

PoroMatrixD::PoroMatrixD(Epetra_RowMatrix&   A_uu_,
        BoundaryCondition &       bcond,
        const Epetra_MpiComm      comm_)
        :   map_uform(A_uu_.OperatorDomainMap()),
                A_uu(A_uu_),
                bcond(bcond),
                comm(comm_),
                composed_map(NULL) {  /// this is just to create a map


            cout<<"INTERFACE B IS USED"<<endl;



            // composing the global map indices from the three partial maps into one 'composed_map'
            size_glob_uform      =  A_uu.OperatorDomainMap().NumGlobalElements(); ///rowmap to OperatorDomainMap?

            size_loc_uform       =  A_uu.OperatorDomainMap().NumMyElements();



            /*
            cout<<comm.MyPID()<<": g u="<<size_glob_uform<<endl;
            cout<<comm.MyPID()<<": g f="<<size_glob_fform<<endl;
            cout<<comm.MyPID()<<": g p="<<size_glob_pform<<endl;

            cout<<comm.MyPID()<<": l u="<<size_loc_uform<<endl;
            cout<<comm.MyPID()<<": l f="<<size_loc_fform<<endl;
            cout<<comm.MyPID()<<": l p="<<size_loc_pform<<endl;
             */

            ///int  composed_glob_map_size = size_glob_uform;
            int  composed_glob_map_size = size_glob_uform;

            ///int  composed_loc_map_size  = size_loc_uform;
            int  composed_loc_map_size  = size_loc_uform;

            composed_map_indices   =  new int[composed_loc_map_size];

            int * indices_uform        =  map_uform.MyGlobalElements();



            for (int i=0; i<size_loc_uform; i++)
                composed_map_indices[i]  =  indices_uform[i];


            composed_map = new Epetra_Map(composed_glob_map_size, composed_loc_map_size, composed_map_indices, 0, comm);


        }






        PoroMatrixD::~PoroMatrixD() {

            delete[] composed_map_indices;
            delete composed_map;
        }

        void PoroMatrixD::myPrint() {

            std::cout<<"this is a print function to test the method."<<std::endl;
        }



        int PoroMatrixD::Apply(const Epetra_MultiVector& X, Epetra_MultiVector& Y) const {

            ///cout<<"A proc: "<<comm.MyPID()<<endl;

            //Y.PutScalar(0.0);
            //Y=X;

            const Epetra_Vector* X_vec = X(0);
            Epetra_Vector* Y_vec  = Y(0);


            Epetra_Vector   X_uform(map_uform);


            Epetra_Vector   tmp_uform(map_uform);


            // Extract the partial vectors from X
            for (int i=0; i<size_loc_uform; i++)  X_uform[i] = (*X_vec)[i                                  ];



                        /// Auu*Xu + Apu^T*Xp
            A_uu.Apply(X_uform  ,  tmp_uform );

            for (int i=0; i<size_loc_uform; i++)
                (*Y_vec)[ i ] = tmp_uform[i];



            return 0;
        }



        int PoroMatrixD::ApplyInverse(const Epetra_MultiVector& X, Epetra_MultiVector& Y) const {
            return -1;
        }



        Epetra_Vector PoroMatrixD::compose_vector( const Epetra_Vector& vec_uform ) {
//Epetra_Vector PoroMatrixD::compose_vector( const Epetra_Vector& vec_uform ) {



            Epetra_Vector composed_vector(*composed_map);


            for (int i=0; i<size_loc_uform; i++)   composed_vector[ i ]                                    = vec_uform[i];

            return composed_vector;
        }


//void PoroMatrixD::extract_partial_vectors( Epetra_Vector*      vec_uform, Epetra_Vector*      vec_fform, Epetra_Vector*     vec_pform,
//                                                 const Epetra_Vector&      composed_vector ) const  {


        void PoroMatrixD::extract_partial_vectors( Epetra_Vector&      vec_uform,
                const Epetra_Vector&      composed_vector ) const  {



            //vec_uform= new Epetra_Vector(map_uform);
            //vec_fform= new Epetra_Vector(map_fform);
            //vec_pform= new Epetra_Vector(map_pform);


            for (int i=0; i<size_loc_uform; i++)   vec_uform[ i ]  =  composed_vector[ i ];

        }
        /*
int PoroMatrixD::PrepareApply(Epetra_Vector* fixed_dofs, Epetra_IntVector* restrained_dofs)
{
  fixed_domain_inds_u .resize(0);
  //precalculation of time-critical datastructures
  int element, ldof;
  int nodof = dof_map.ElementSize();
  //local indices mapping from fixed dofs to domain map
  for (int i = 0 ; i < fixed_dofs->MyLength(); ++i) {
    if (fixed_dofs->operator[](i) != 0.0) {
      fixed_dofs->Map().FindLocalElementID(i, element, ldof);
      fixed_domain_inds_u.push_back( domain_map->LID(fixed_dofs->Map().GID(element)*nodof + ldof) );
    }
  }
  restrained_domain_inds.resize(0);
  //local indices mapping from restrained dofs to domain map
  for (int i = 0 ; i < restrained_dofs->MyLength(); ++i) {
    if (restrained_dofs->operator[](i) == 0) {
      restrained_dofs->Map().FindLocalElementID(i, element, ldof);
      restrained_domain_inds_u.push_back( domain_map->LID(restrained_dofs->Map().GID(element)*nodof + ldof) );
    }
  }

  return 0;
}
         */
