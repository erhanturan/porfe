/*
 * ParFE: a micro-FE solver for trabecular bone modeling
 * Copyright (C) 2006 ParFE developers, see
 * http://parfe.sourceforge.net/developers.php
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

/*
 * This code should be used to perform a single finite element analysis
 * of a trabecular bone, stored in either ASCII or HDF5 format. Three
 * preconditioners are available: "Jacobi" (for comparisons only, not
 * for production), "ml" (matrix-ready multilevel preconditioner)
 * and "matrixfree" (matrix-free multilevel preconditioner).
 *
 * \author Marzio Sala
 *
 * \date Last modified on 15-Nov-06
 */

#include "parfe_ConfigDefs.h"
#include <string>
#include <fstream>
#include <vector>
#include <Epetra_Time.h>
#ifdef HAVE_MPI
#include <mpi.h>
#include <Epetra_MpiComm.h>
#else
#include <Epetra_SerialComm.h>
#endif
#include <Epetra_Vector.h>
#include <Epetra_IntVector.h>
#include <time.h>
#include "DistMesh.h"
#include "MEDIT_MeshWriter.hpp"
#include "PMVIS_MeshWriter.hpp"
#include "MEDIT_DispWriter.hpp"
#include "IBT_ProblemReader.hpp"
#include "IBT_ProblemWriter.hpp"
#include "MATLAB_MatrixWriter.hpp"
#include "MATLAB_VectorWriter.hpp"
#include "IBT_DispWriter.hpp"
#include "IBT_SolutionWriter.hpp"
#include "EbeElasticityProblem.h"
#include "VbrElasticityProblem.h"
#include "Jacobi.h"
#include "Tools.h"
#include "FEParameterList.hpp"

#include <ml_MultiLevelPreconditioner.h>
#include <ml_MatrixFreePreconditioner.h>
#include <AztecOO.h>
///new belos includes*****
#include "BelosConfigDefs.hpp"
#include "BelosLinearProblem.hpp"
#include "BelosEpetraAdapter.hpp"
#include "BelosPCPGSolMgr.hpp"
#include "BelosMinresSolMgr.hpp"
#include <Teuchos_ParameterList.hpp>
#include <Teuchos_RefCountPtr.hpp>
#include <Teuchos_CommandLineProcessor.hpp>
#include "Teuchos_RCP.hpp"
#include "BelosPseudoBlockGmresSolMgr.hpp"
#include "BelosBlockCGSolMgr.hpp"
#include "BelosTFQMRSolMgr.hpp"
////#include "BelosBlockCGSolMgr.hpp"

#include "ml_include.h" //--enable-epetra --enable-teuchos.
#include "Epetra_Map.h"


///#include "Epetra_CrsMatrix.h"
///#include "EpetraExt_MatrixMatrix.h"  // build the example


///using namespace ML_Epetra;
///using namespace Teuchos;


// =========== //
// main driver //
// =========== //

int main( int argc, char* argv[] ) {
#ifdef HAVE_MPI
    MPI_Init(&argc, &argv);
// define an Epetra communicator
Epetra_MpiComm comm(MPI_COMM_WORLD);
#else
Epetra_SerialComm comm;
#endif

/// /*
using Teuchos::ParameterList; // all of this may look fine but do not be fooled ...
using Teuchos::RCP;           // it is not so clear what any of this does
using Teuchos::rcp;
/// */

// get the proc ID of this process
int MyPID = comm.MyPID();

// get the total number of processes
int NumProc = comm.NumProc();

Epetra_Time timer(comm);
double start_time = timer.WallTime();
double temp_time = start_time;


//Macros to print information
#define INFO(mesg) \
if (MyPID == 0) cout << "*** INFO ***: " << mesg << endl;

#define WARN(mesg) \
if (MyPID == 0) cout << "*** WARNING ***: " << mesg << endl;

#define ERROR(mesg, exit_code)\
if (MyPID == 0) cerr << "*** ERROR ***: " << mesg << endl; \
        exit(exit_code);

Teuchos::CommandLineProcessor CLP;

char help[] = "This is the latest version of the ParFE driver.\n"
        "It can solve problems stored in ASCII or HDF5 format, perform\n"
        "load balance, build the preconditioner for matrix-free or\n"
        "matrix-ready problems, and print out the solution.\n";
CLP.setDocString(help);

string inputFileName = "../mesh/simple_test.mesh";
string outputFileName = "./output";
string precType = "ml";
string repartition = "parmetis";
int maxIters = 1550;
double tolerance = 1.e-5;
bool verbose = true;
bool printMEDIT = false;
bool printPMVIS = false;
int outputFrequency = 16;

CLP.setOption("filename", &inputFileName, "Name of input file");
CLP.setOption("output", &outputFileName, "Base name of output file");
CLP.setOption("precond", &precType,
        "Preconditioner to be used [Jacobi/ml/matrixfree]");
CLP.setOption("repart", &repartition,
        "Repartioner to be used [parmetis/rcb]");

CLP.setOption("maxiters", &maxIters, "Maximum CG iterations");
CLP.setOption("tolerance", &tolerance, "Tolerance for CG");
CLP.setOption("verbose", "silent", &verbose,
        "Set verbosity level");
CLP.setOption("printmedit", "noprintmedit", &printMEDIT,
        "Print solution in MEDIT format");
CLP.setOption("printpmvis", "noprintpmvis", &printPMVIS,
        "Print partition in PMVIS format");
CLP.setOption("frequency", &outputFrequency,
        "Prints out residual every specified iterations");

CLP.recogniseAllOptions(false);
CLP.throwExceptions(false);

if (CLP.parse(argc, argv) == Teuchos::CommandLineProcessor::PARSE_HELP_PRINTED) {
#ifdef HAVE_MPI
    MPI_Finalize();
#endif
exit(EXIT_SUCCESS);
}

FEParameterList fe_param(NumProc);
fe_param.set("input", outputFileName);

Epetra_MultiVector* BCs = 0;

ProblemReader* pr = 0;
if (inputFileName.find(".mesh.h5") != string::npos)
    pr = new IBT_ProblemReader<HDF5_GReader>(inputFileName, comm);
else if (inputFileName.find(".mesh") != string::npos)
    pr = new IBT_ProblemReader<C_ASCII_GReader>(inputFileName, comm);
else {
    cerr << "File name (" << inputFileName << ") not in ASCII or HDF5 format" << endl;
    exit(EXIT_FAILURE);
}

fe_param.Scan(pr);

INFO("#nodes       = " << fe_param.get<int>("#nodes"));
INFO("#dimensions  = " << fe_param.get<int>("#dimensions"));
INFO("#elements    = " << fe_param.get<int>("#elements"));
INFO("#faces       = " << fe_param.get<int>("#faces"));
INFO("#free faces  = " << fe_param.get<int>("#free faces"));

temp_time = timer.WallTime();

//If these parameters are still unset, provide some default values
fe_param.get("iteration limit", maxIters);
fe_param.get("tolerance", tolerance);

DistMesh* mesh = new DistMesh(fe_param.get<int>("#nodes"), fe_param.get<int>("#dimensions"), fe_param.get<int>("#elements"), fe_param.get<int>("#nodes per element"), fe_param.get<IntMap>("material ids"), comm, fe_param.get<int>("#faces"), fe_param.get<int>("#free faces"),1,1,1);

mesh->Scan(pr);
BoundaryCondition* bcond = new BoundaryCondition(fe_param.get<int>("#dofs per node"), comm);
bcond->Scan(pr);

delete pr;

//--------------------------end of input--------------------------------

INFO("Input Time: " << timer.WallTime() - temp_time);
temp_time = timer.WallTime();


if (repartition == "parmetis"){
    mesh->Redistribute(true, verbose);
}
else if (repartition == "rcb")
    mesh->RedistributeWIsorropia(true, verbose);
else
    mesh->Redistribute(false, verbose);

if (repartition == "parmetis" || repartition == "rcb") {
    INFO("Time used to partition mesh: " << timer.WallTime() - temp_time);
} else {
    WARN("No load balancing used");
}

//redistribute the boundary condition data according to the node map
bcond->Redistribute(mesh->NodeMap()->NumMyElements(), mesh->NodeMap()->MyGlobalElements());

if (printMEDIT) {
    MEDIT_MeshWriter meshw(outputFileName + ".medit.mesh", 3, comm);
    mesh->Print(&meshw);
}

if (fe_param.get<bool>("print for PMVIS")) {
    std::string con = outputFileName + ".con";
    std::string xyz = outputFileName + ".xyz";
    std::string part = outputFileName + ".part";
    PMVIS_MeshWriter meshw(xyz, con, part, comm);
    mesh->Print(&meshw);
}

int memused = meminfo();
INFO("Mem used to hold mesh information: " << memused <<  "MB");

INFO("Total Mesh setup time: " << timer.WallTime() - start_time);

//-------------------------beginning of assembly------------------------

temp_time = timer.WallTime();

ElasticityProblem* ep;
if (precType == "Jacobi" || precType == "matrixfree") {
    fe_param.set("element by element", true);
    ep = new EbeElasticityProblem(*mesh, fe_param);
}
else
    ep = new VbrElasticityProblem(*mesh, fe_param, 0.0, 1);

ep->Impose(*bcond);

BCs = new Epetra_MultiVector(*ep->GetRHS());

INFO("Time used to assemble matrix: " << timer.WallTime() - temp_time);

memused = meminfo() - memused;
INFO("Mem used to hold stiffness matrix operator: " << memused << "MB");

Epetra_Vector* X = dynamic_cast<Epetra_Vector*>(ep->GetLHS());
Epetra_Vector* B = dynamic_cast<Epetra_Vector*>(ep->GetRHS());


///manage the vectors as RCP objects.
RCP<Epetra_Vector> pX=rcp(X);
RCP<Epetra_Vector> pB=rcp(B);
RCP<Epetra_Operator> Auu=rcp(ep->GetMatrix());

VectorWriter* myMatlabVec = 0;
    myMatlabVec = new MATLAB_VectorWriter<C_ASCII_GWriter>("XB", comm);
    myMatlabVec->PrintVector(*X);


if (X == 0 || B == 0) {
    ERROR("Use Epetra_Vector in ElastictyProblem", 1);
}

vector<double> null_space;

//Compute null space for ml preconditioner
if (precType == "ml" || precType == "matrixfree")
    set_null_space(mesh, null_space);

Teuchos::RefCountPtr<Epetra_CrsGraph> graph; ///Teuchos:: is added

if (precType == "matrixfree") {
    ElementByElementMatrix* ebe = dynamic_cast<ElementByElementMatrix*>(ep->GetOperator());
    if (ebe != 0) {
        graph = Teuchos::rcp(new Epetra_CrsGraph(Copy, ebe->Map(), 0)); ///Teuchos:: is added
        mesh->MatrixGraph(*(graph.get()));
    }
}

//------------------------beginning of solution------------------------


///Teuchos::RefCountPtr<Epetra_Vector> diagonal;
RCP<Belos::EpetraPrecOp> belosPrec;
RCP<Epetra_Operator> Prec;

if (precType == "Jacobi") {

    ElementByElementMatrix* ebe = dynamic_cast<ElementByElementMatrix*>(ep->GetOperator());
    std::cout<<"Jacobi solver is not yet implemented!"<<std::endl;
    std::cout<<"parfe exit"<<std::endl;
#ifdef HAVE_MPI
MPI_Finalize();
#endif
exit(EXIT_SUCCESS);

}
else if (precType == "matrixfree") {
    ElementByElementMatrix* ebe = dynamic_cast<ElementByElementMatrix*>(ep->GetOperator());
    std::cout<<"matrixfree solver is not yet implemented!"<<std::endl;
    std::cout<<"parfe exit"<<std::endl;
#ifdef HAVE_MPI
MPI_Finalize();
#endif
exit(EXIT_SUCCESS);

}
else if (precType == "ml") {

    // allocate the ML preconditioner
    Teuchos::ParameterList MLList;


    ML_Epetra::SetDefaults("SA", MLList);
    MLList.set("smoother: type (level 0)", "Chebyshev");
    MLList.set("smoother: type (level 1)", "symmetric Gauss-Seidel");
    MLList.set("coarse: max size", 1024);

    MLList.set("null space: dimension", 6);
    MLList.set("null space: type", "pre-computed");
    MLList.set("null space: vectors", &null_space[0]);

    MLList.set("aggregation: type", "MIS");
    ///MLList.set("aggregation: type", "Uncoupled");
    //MLList.set("aggregation: type (level 0)", "ParMETIS");
    //MLList.set("aggregation: type (level 1)", "Uncoupled");
    //MLList.set("aggregation: type (level 2)", "MIS");
    MLList.set("low memory usage", true);
    //MLList.set("max levels", 6);
    if (verbose) MLList.set("ML output", 10);
    else         MLList.set("ML output", 0);

    null_space.resize(0);


    Prec = rcp(  new ML_Epetra::MultiLevelPreconditioner(*ep->GetMatrix(), MLList, true) );

    assert(Prec != Teuchos::null);


    belosPrec = rcp( new Belos::EpetraPrecOp(Prec) );

}

else {
    ///solver.SetAztecOption(AZ_precond, AZ_none); //TODO
    WARN("No preconditioner used");
}


INFO("Time used to build preconditioner: " << timer.WallTime() - temp_time);
memused = meminfo() - memused;
INFO("Mem used to hold preconditioner: " << memused << "MB");

// solution is constant
X->PutScalar(0.0);

/*
MatrixWriter* mw3 = 0;
mw3 = new MATLAB_MatrixWriter<C_ASCII_GWriter>("AuuB", comm);
mw3->PrintRows(dynamic_cast<Epetra_VbrMatrix*>(ep->GetMatrix()));
*/

/// belos parameters:
Teuchos::ParameterList belosList;
belosList.set( "Block Size", 1 );              // Blocksize to be used by iterative solver
belosList.set( "Maximum Iterations", maxIters );   // Maximum number of iterations allowed
belosList.set( "Convergence Tolerance", tolerance);// Relative convergence tolerance requested
belosList.set( "Output Style", (int) Belos::Brief);
belosList.set( "Verbosity", Belos::Errors+Belos::StatusTestDetails+Belos::Warnings+Belos::TimingDetails+Belos::IterationDetails+Belos::FinalSummary );
belosList.set( "Output Frequency", 1); //not wotrking?
belosList.set( "Explicit Residual Test", true );

/// declarations

typedef Epetra_MultiVector                MV;
typedef Epetra_Operator                   OP;

typedef double                          ST;

typedef Belos::OperatorTraits<ST, MV, OP> OPT;// TODO
typedef Belos::MultiVecTraits<ST, MV>    MVT;// TODO




/// linear preconditioned problem:

RCP<Belos::LinearProblem<double, MV, OP> > problem = rcp( new Belos::LinearProblem<double, MV, OP>(Auu, pX, pB));


problem->setLeftPrec(belosPrec);


bool set = problem->setProblem();
if (set == false) {
    if (MyPID==0)
        std::cout << std::endl << "ERROR:  problem set up is not correct" << std::endl;
    return -1;
}


/// solver manager - Minres
RCP< Belos::SolverManager<double, MV, OP> > solver = rcp( new Belos::BlockCGSolMgr<double, MV, OP>(problem, rcp(&belosList, false)) );
//RCP< Belos::SolverManager<double, MV, OP> > solver = rcp( new Belos::PseudoBlockGmresSolMgr<double, MV, OP>(problem, rcp(&belosList, false)) );
//RCP< Belos::SolverManager<double, MV, OP> > solver = rcp( new Belos::TFQMRSolMgr<double, MV, OP>(problem, rcp(&belosList, false)) );
///RCP< Belos::SolverManager<double, MV, OP> > solver = rcp( new Belos::PCPGSolMgr<double, MV, OP>(problem, rcp(&belosList, false)) );
///RCP< Belos::SolverManager<double, MV, OP> > solver = rcp( new Belos::MinresSolMgr<double, MV, OP>(problem, rcp(&belosList, false)) );


/// /////////////
double initialNorm;
const Epetra_Vector* myResVecZero = problem->getInitResVec()->operator()(0);
myResVecZero->Norm2(&initialNorm);
if (comm.MyPID()==0) cout<<"initial norm = "<<initialNorm<<endl;

/// ///////////////



Belos::ReturnType ret = solver->solve();


/// /////////////
Epetra_Vector* testVec = new Epetra_Vector(*X);
double finalNorm;

Epetra_Operator* AuuO=ep->GetOperator();

Epetra_Vector* mySolvecZero = problem->getLHS()->operator()(0);
double testNorm;
mySolvecZero->Norm2(&testNorm);

AuuO->Apply(*testVec,*testVec);
testVec->Update(1.0, *B,-1.0);
testVec->Norm2(&finalNorm);
if (comm.MyPID()==0) cout<<"|| absolute final norm is = "<<finalNorm<<" ||"<<endl;
if (comm.MyPID()==0) cout<<"|| relative convergence is = "<<finalNorm/initialNorm<<" ||"<<endl;

/// ///////////////

if (comm.MyPID()==0) {

    cout<<endl;
    cout<<"-----------------------------------"<<endl;
    cout<<"|| test norm is = "<<testNorm<<" ||"<<endl;
    cout<<"-----------------------------------"<<endl;
    cout<<endl;
    }


if (precType == "matrixfree" || precType == "Jacobi") {
    ElementByElementMatrix* ebe = dynamic_cast<ElementByElementMatrix*>(ep->GetOperator());
    ebe->ResetRestrainedBoundaries(X);
}

///diagonal = Teuchos::null;


//------------------------------Print solution-------------------------

temp_time = timer.WallTime();

if (inputFileName.find(".mesh.h5") == string::npos) {
    int ext_pos =  inputFileName.rfind(".mesh");
    inputFileName = inputFileName.substr(0, ext_pos)+".mesh.h5";
    IBT_ProblemWriter<HDF5_GWriter> pw(inputFileName , comm);
    fe_param.Print(&pw);
    mesh->Print(&pw);
    bcond->Print(&pw);
}


if (inputFileName.find(".mesh.h5") != string::npos) {
    IBT_SolutionWriter<HDF5_GWriter> sw(inputFileName, comm);
    ep->PrintSolution(&sw, fe_param.get<Epetra_SerialDenseMatrix>("material properties"));
}

if (printMEDIT) {
    MEDIT_DispWriter dispw(outputFileName + ".medit.bb", comm, 3);
    ep->PrintLHS(&dispw);
}

INFO("output time: " << timer.WallTime() -temp_time);

comm.Barrier();
INFO("Total time used: " << timer.WallTime() -start_time);

// ======================= //
// Finalize MPI and exit //
// ----------------------- //

/*
Epetra_MultiVector* mySolution = problem->getLHS().get();

//const Epetra_Vector* mySolvecZero = problem->getLHS()->operator()(0); /// if constant, cannot overwritten.
Epetra_Vector* mySolvecZero = problem->getLHS()->operator()(0);

cout<<mySolvecZero->Values()[0]<<endl;
cout<<mySolvecZero->Values()[1]<<endl;
cout<<mySolvecZero->Values()[2]<<endl;
//cout<<mySolvecZero->Map()<<endl;
*/





/// erhan added
/// reset smart pointers: assing to null pointer
/// Teuchos::RCP Beginner's Guide - September 2010. Appendix B 2b.
///manage the vectors as RCP objects.
/*
pX = null;
pB = null;
Auu = null;
Prec = null;
belosPrec = null;
problem=null;
solver=null;
*/


return 0; /// erhan added
///should be active in Minres else we get the same RCP error. Cyril's main-hack idea might work but not tested
#ifdef HAVE_MPI
MPI_Finalize();
#endif
exit(EXIT_SUCCESS);


}
