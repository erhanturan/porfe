/*
 * ParFE: a micro-FE solver for trabecular bone modeling
 * Copyright (C) 2006 ParFE developers, see
 * http://parfe.sourceforge.net/developers.php
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

/*
 * This code should be used to perform a single finite element analysis
 * of a trabecular bone, stored in either ASCII or HDF5 format. Three
 * preconditioners are available: "Jacobi" (for comparisons only, not
 * for production), "ml" (matrix-ready multilevel preconditioner)
 * and "matrixfree" (matrix-free multilevel preconditioner).
 *
 * \author Marzio Sala - ParFE - pfaim.exe
 * \author Cyril Flaig - ParFE - new_pfaim.exe
 * \author Erhan Turan - PorFE - pbnew_pfaim.exe
 *
 * \date Last modified on 25-May-12
 */

#include "parfe_ConfigDefs.h"
#include <string>
#include <fstream>
#include <iomanip>
#include <vector>
#include <Epetra_Time.h>
#ifdef HAVE_MPI
#include <mpi.h>
#include <Epetra_MpiComm.h>
#else
#include <Epetra_SerialComm.h>
#endif
#include <Epetra_Vector.h>
#include <Epetra_IntVector.h>
#include <time.h>
#include "DistMesh.h"
#include "MEDIT_MeshWriter.hpp"
#include "PMVIS_MeshWriter.hpp"
#include "MEDIT_DispWriter.hpp"
#include "IBT_ProblemReader.hpp"
#include "IBT_ProblemWriter.hpp"
#include "MATLAB_MatrixWriter.hpp"
#include "MATLAB_VectorWriter.hpp"
#include "IBT_DispWriter.hpp"
#include "IBT_SolutionWriter.hpp"
#include "EbeElasticityProblem.h"
#include "VbrElasticityProblem.h"
#include "Jacobi.h"
#include "Tools.h"
#include "FEParameterList.hpp"

#include <ml_MultiLevelPreconditioner.h>
#include <ml_MatrixFreePreconditioner.h>
#include <AztecOO.h>
///new belos includes*****
#include "BelosConfigDefs.hpp"
#include "BelosLinearProblem.hpp"
#include "BelosEpetraAdapter.hpp"
//#include "BelosPCPGSolMgr.hpp"
#include "BelosMinresSolMgr.hpp"
#include "BelosPseudoBlockGmresSolMgr.hpp"
#include "BelosBlockGmresSolMgr.hpp"
#include "BelosGmresPolySolMgr.hpp"
#include "BelosTFQMRSolMgr.hpp"
#include <Teuchos_ParameterList.hpp>
#include <Teuchos_RefCountPtr.hpp>
#include <Teuchos_CommandLineProcessor.hpp>
#include "Teuchos_RCP.hpp"
//#include "BelosBlockCGSolMgr.hpp"

#include "ml_include.h" //--enable-epetra --enable-teuchos.
#include "Epetra_Map.h"
#include "Epetra_Time.h"
#include "PoroMatrix.h" /// erhan added
#include "PoroMatrixPreconditioner.h" /// erhan added
#include "PoroMatrixPreconditionerB.h" /// erhan added
#include "PoroMatrixPreconditionerC.h" /// erhan added
#include "PoroMatrixPreconditionerD.h" /// erhan added
#include <Epetra_FEVbrMatrix.h> /// erhan added
#include "PressureElasticityProblem.h" /// erhan added
#include "PressureFluxProblem.h" /// erhan added
#include "PressureProblem.h" /// erhan added
#include "SchurProblem.h" /// erhan added
#include "FluxProblem.h" /// erhan added
#include <Ifpack.h>
#include <Ifpack_IC.h>
//#include <Ifpack_AdditiveSchwarz.h>
#include "Amesos.h"
#include "Amesos_ConfigDefs.h"
#include "Amesos_BaseSolver.h"
///using namespace ML_Epetra;
///using namespace Teuchos;

using namespace std;


// =========== //
// main driver //
// =========== //

int main( int argc, char* argv[] ) {
#ifdef HAVE_MPI
    MPI_Init(&argc, &argv);
// define an Epetra communicator
Epetra_MpiComm comm(MPI_COMM_WORLD);
#else
Epetra_SerialComm comm;
#endif

/// /*
using Teuchos::ParameterList; // all of this may look fine but do not be fooled ...
using Teuchos::RCP;           // it is not so clear what any of this does
using Teuchos::rcp;
/// */

// get the proc ID of this process
int MyPID = comm.MyPID();

// get the total number of processes
int NumProc = comm.NumProc();

Epetra_Time timer(comm);
double start_time = timer.WallTime();
double temp_time = start_time;


//Macros to print information
#define INFO(mesg) \
if (MyPID == 0) cout << "%*** INFO ***: " << mesg << endl;

#define WARN(mesg) \
if (MyPID == 0) cout << "%*** WARNING ***: " << mesg << endl;

#define ERROR(mesg, exit_code)\
if (MyPID == 0) cerr << "%*** ERROR ***: " << mesg << endl; \
        exit(exit_code);

Teuchos::CommandLineProcessor CLP;

char help[] = "This is the latest version of the ParFE driver.\n"
        "It can solve problems stored in ASCII or HDF5 format, perform\n"
        "load balance, build the preconditioner for matrix-free or\n"
        "matrix-ready problems, and print out the solution.\n";
CLP.setDocString(help);

string inputFileName = "../mesh/simple_test.mesh";
string outputFileName = "./output";
string precType = "ml";
///string repartition = "parmetis";
string repartition = "none";
string precDir = "right";
string krylovType = "fgmres";

int maxIters = 3000;
int maxRestarts = 100;
int numBlocks= 100;
int numCycleAuu=1;
int numCycleSpp=1;

double totalTime=1.0;
double deltaT=0.1;

//int maxIters = 10;
double tolerance = 1.e-5;
double AuuTolerance = 1.e-5;
double AffTolerance = 1.e-5;
double SppTolerance = 1.e-5;
int uweight = 3;
int fweight = 1;
int pweight = 1;
int AuuMaxIters = 50;
int AffMaxIters = 10;
int SppMaxIters = 3;

bool verbose = true;
bool printMEDIT = false;
bool printPMVIS = false;
bool printResult = true;
bool initialCondition = true;
bool test1 = false;
int outputFrequency = 1;
string doScale = "noscale";
string doMatlab = "no";
string precClass = "b";

CLP.setOption("filename", &inputFileName, "Name of input file");
CLP.setOption("output", &outputFileName, "Base name of output file");
CLP.setOption("precond", &precType,
        "Preconditioner to be used [Jacobi/ml/matrixfree(not available yet)]");
CLP.setOption("repart", &repartition,
        "Repartioner to be used [parmetis/rcb]");

CLP.setOption("maxiters", &maxIters, "Maximum number of iterations");
CLP.setOption("auumaxiters", &AuuMaxIters, "Maximum number of iterations for Auu block");
CLP.setOption("affmaxiters", &AffMaxIters, "Maximum number of iterations for Aff block");
CLP.setOption("sppmaxiters", &SppMaxIters, "Maximum number of iterations for Spp block");
CLP.setOption("uweight", &uweight, "u weight [3]");
CLP.setOption("fweight", &fweight, "f weight [1]");
CLP.setOption("pweight", &pweight, "p weight [1]");
CLP.setOption("maxrestarts", &maxRestarts, "Maximum number of restarts, valid only for (f)gmres");
CLP.setOption("numblocks", &numBlocks, "Size of the Krylov basis, valid only for (f)gmres");
CLP.setOption("tolerance", &tolerance, "Tolerance for krylov solver");
CLP.setOption("auutolerance", &AuuTolerance, "Tolerance for Auu solver in prec B-C");
CLP.setOption("afftolerance", &AffTolerance, "Tolerance for Auu solver in prec B-C");
CLP.setOption("spptolerance", &SppTolerance, "Tolerance for Spp solver in prec B-C");
CLP.setOption("verbose", "silent", &verbose,
        "Set verbosity level");
CLP.setOption("printmedit", "noprintmedit", &printMEDIT,
        "Print solution in MEDIT format");
CLP.setOption("printpmvis", "noprintpmvis", &printPMVIS,
        "Print partition in PMVIS format");
CLP.setOption("printresult", "noprintresult", &printResult,
        "Print results into file");
CLP.setOption("frequency", &outputFrequency,
        "Prints out residual every specified iterations");
CLP.setOption("auuvcycle", &numCycleAuu,
        "number of v-cycles in ml for Auu [1]");
CLP.setOption("sppvcycle", &numCycleSpp,
        "number of v-cycles in ml for Spp [1]");
//CLP.setOption("scaling", &doScale, "Perform scaling scale/[noscale]");
CLP.setOption("printmatlab", &doMatlab, "print matrices in matlab format [no]/yes]");
CLP.setOption("precdir", &precDir,
        "Preconditioner direction [left]/right");
CLP.setOption("krylov", &krylovType,
        "Krylov solver: minres/[gmres]");
CLP.setOption("type", &precClass, "preconditioner type [a]/b");
CLP.setOption("totaltime", &totalTime, "Total time for transient calculation");
CLP.setOption("deltat", &deltaT, "Time increment for transient calculation");
CLP.setOption("initial", "noinitial", &initialCondition,
        "Apply initial condition");
CLP.setOption("test1", "notest1", &test1,
        "Apply test1:matvec products for 1000 times");

int maXTimeStep = totalTime/deltaT;

CLP.setOption("maxtimeiters", &maXTimeStep, "Maximum number of time iterations");


CLP.recogniseAllOptions(false);
CLP.throwExceptions(false);

if (CLP.parse(argc, argv) == Teuchos::CommandLineProcessor::PARSE_HELP_PRINTED) {
#ifdef HAVE_MPI
    MPI_Finalize();
#endif
exit(EXIT_SUCCESS);
}

FEParameterList fe_param(NumProc);
fe_param.set("input", outputFileName);

Epetra_MultiVector* BCs = 0;

ProblemReader* pr = 0;
if (inputFileName.find(".mesh.h5") != string::npos)
    pr = new IBT_ProblemReader<HDF5_GReader>(inputFileName, comm);
else if (inputFileName.find(".mesh") != string::npos)
    pr = new IBT_ProblemReader<C_ASCII_GReader>(inputFileName, comm);
else {
    cerr << "File name (" << inputFileName << ") not in ASCII or HDF5 format" << endl;
    exit(EXIT_FAILURE);
}

fe_param.Scan(pr);

INFO("#nodes       = " << fe_param.get<int>("#nodes"));
INFO("#dimensions  = " << fe_param.get<int>("#dimensions"));
INFO("#elements    = " << fe_param.get<int>("#elements"));
INFO("#faces       = " << fe_param.get<int>("#faces"));
INFO("#free faces  = " << fe_param.get<int>("#free faces"));

temp_time = timer.WallTime();

//If these parameters are still unset, provide some default values
fe_param.get("iteration limit", maxIters);
fe_param.get("tolerance", tolerance);

DistMesh* mesh = new DistMesh(fe_param.get<int>("#nodes"), fe_param.get<int>("#dimensions"), fe_param.get<int>("#elements"), fe_param.get<int>("#nodes per element"), fe_param.get<IntMap>("material ids"), comm, fe_param.get<int>("#faces"), fe_param.get<int>("#free faces"), uweight, fweight, pweight);

mesh->Scan(pr);
//cout<<"MYPID XXX = "<<comm.MyPID()<<" - "<<mesh->ElementNodesFace()->MyLength()<<endl;
BoundaryCondition* bcond = new BoundaryCondition(fe_param.get<int>("#dofs per node"), comm);
bcond->Scan(pr);

delete pr;

//--------------------------end of input--------------------------------

INFO("Input Time: " << timer.WallTime() - temp_time);
temp_time = timer.WallTime();
if (comm.MyPID()==0) cout<<"%{"<<endl;


///cout<<mesh->NodeMap()->NumMyElements()<<endl;

///cout<<mesh->ElementMap()->NumMyElements()<<endl;

if (repartition == "parmetis"){
    mesh->Redistribute(true, verbose);
}
else if (repartition == "poro"){
    mesh->RedistributePoro(true, verbose);
}
else if (repartition == "rcb"){
    mesh->RedistributeWIsorropia(true, verbose);

    mesh->RedistributeWIsorropiaElements(true, verbose);

    mesh->RedistributeWIsorropiaFaces(true, verbose);

}
else
    mesh->Redistribute(false, verbose);

//cout<<"MYPID XXX = "<<comm.MyPID()<<" - "<<mesh->ElementNodesFace()->MyLength()<<endl;
//return 0;
//return 0; /// <-------------------------------------

///cout<<*mesh->FaceMap()<<endl;
///cout<<*mesh->FaceElementMap()<<endl;

//return 0;

if (repartition == "parmetis" || repartition == "rcb" || repartition == "poro") {
    INFO("Time used to partition mesh: " << timer.WallTime() - temp_time);
} else {
    WARN("No load balancing used");
}

//redistribute the boundary condition data according to the node map
bcond->Redistribute(mesh->NodeMap()->NumMyElements(), mesh->NodeMap()->MyGlobalElements());

if (printMEDIT) {
    MEDIT_MeshWriter meshw(outputFileName + ".medit.mesh", 3, comm);
    mesh->Print(&meshw);
}

if (fe_param.get<bool>("print for PMVIS")) {
    std::string con = outputFileName + ".con";
    std::string xyz = outputFileName + ".xyz";
    std::string part = outputFileName + ".part";
    PMVIS_MeshWriter meshw(xyz, con, part, comm);
    mesh->Print(&meshw);
}
if (comm.MyPID()==0) cout<<"%}"<<endl;
int memused = meminfo();
INFO("Mem used to hold mesh information: " << memused <<  "MB");

INFO("Total Mesh setup time: " << timer.WallTime() - start_time);

//-------------------------beginning of assembly------------------------

temp_time = timer.WallTime();

ElasticityProblem* ep;
ElasticityProblem* sep;
//VbrElasticityProblem* ep;


if (precType == "Jacobi" || precType == "matrixfree") {
    fe_param.set("element by element", true);
    ep = new EbeElasticityProblem(*mesh, fe_param);
}
else{
    ep = new VbrElasticityProblem(*mesh, fe_param, deltaT, 1);
    sep = new VbrElasticityProblem(*mesh, fe_param, deltaT, 2);
}
ep->Impose(*bcond);
sep->Impose(*bcond);

BCs = new Epetra_MultiVector(*ep->GetRHS());

INFO("Time used to assemble matrix: " << timer.WallTime() - temp_time);

memused = meminfo() - memused;
INFO("Mem used to hold stiffness matrix operator: " << memused << "MB");

Epetra_Vector* Xu = dynamic_cast<Epetra_Vector*>(ep->GetLHS());
Epetra_Vector* Bu = dynamic_cast<Epetra_Vector*>(ep->GetRHS());
/// gravity should not be added if zero!!!!

Epetra_RowMatrix *AuuP=ep->GetMatrix();
Epetra_RowMatrix *SuuP=sep->GetMatrix();
//Epetra_Operator *Auu=ep->GetOperator();
///cout<<Xu->Map()<<endl; return 0;

///=============================================================
///  define new subblocks
///=============================================================

//memused = meminfo() - memused;
/// flux problem - Aff Block
FluxProblem* fp = new FluxProblem(*mesh, fe_param, deltaT, 1);
FluxProblem* sfp = new FluxProblem(*mesh, fe_param, deltaT, 2);

/// pressure elasticty problem - Apu block
PressureElasticityProblem* pep = new PressureElasticityProblem(*mesh, *bcond, fe_param, AuuP->OperatorDomainMap());
//PressureElasticityProblem* pep = new PressureElasticityProblem(*mesh, *bcond, fe_param, Xu->Map());

/// pressure flux problem - Apf block
PressureFluxProblem* pfp = new PressureFluxProblem(*mesh, fe_param, deltaT); /// OK

/// pressure problem - Ap block - Just a vector but represents the diagonal matrix App.
PressureProblem* pp = new PressureProblem(*mesh, fe_param); /// OK

/// schur problem - Spp block
SchurProblem* sp = new SchurProblem(*mesh, fe_param, deltaT);  /// OK

Epetra_Vector* Af; /// diagonal of Aff block
Epetra_Vector* Ap; /// pressure vector
//Epetra_FECrsMatrix* AFEff; /// flux matrix
Epetra_CrsMatrix* Aff; /// flux matrix
Epetra_CrsMatrix* Spp; /// schur matrix
Epetra_CrsMatrix* Apu; /// pressure elasticity matrix
Epetra_CrsMatrix* Apf; /// pressure flux matrix

Epetra_CrsMatrix* Sff; /// flux matrix

Aff = &fp->getMatrix();
INFO("Aff created");

Apu = &pep->getMatrix();
INFO("Apu created");

Apf = & pfp->getMatrix();
INFO("Apf created");

Spp = & sp->getMatrix();
INFO("Spp created");

Ap = &pp->getVector();
INFO("Ap created ");

Sff = &sfp->getMatrix();
INFO("Aff created");

Af = new Epetra_Vector(fp->getDiagonal()); /// - should be automatically correct when Aff is.

INFO("Af is extracted");

Epetra_Vector* Xf = dynamic_cast<Epetra_Vector*>(fp->GetLHS());
Epetra_Vector* Bf = dynamic_cast<Epetra_Vector*>(fp->GetRHS());
Epetra_Vector* Xp = dynamic_cast<Epetra_Vector*>(pp->GetLHS());
Epetra_Vector* Bp = dynamic_cast<Epetra_Vector*>(pp->GetRHS());


INFO("ALL = " << meminfo() << "MB");


VectorWriter* myMatlabVec = 0;
MatrixWriter* mw3 = 0;
bool mTest;


/// define the interface for matrix-vector multiplication
PoroMatrix *my_poro = new PoroMatrix(*AuuP, *Aff, *Ap, *Apu, *Apf, *bcond, AuuP->OperatorDomainMap(), comm);
//PoroMatrix *my_poro = new PoroMatrix(*AuuP, *Aff, *Ap, *Apu, *Apf, *bcond, Xu->Map(), comm);
INFO("PoroMatrix is defined");
///*
/// perform scaling on the blocks.
if(doScale=="scale") {

    my_poro->Scale(); ///scale AuuP, Aff, Ap, Apu, Apf.
    my_poro->ScaleF(*Af); /// apply right scale
    my_poro->ScaleF(*Af); /// apply left scale - same as right scale since Af is a vector
    my_poro->ScaleP(*Spp); /// scale Spp only
    INFO("Matrices are scaled");

}
//*/

/// creation of pA to define the matrix in Belos::LinearProblem
RCP<Epetra_Operator> pA = rcp(my_poro);
INFO("PoroMatrix is created");



/// add a new routine to define initial conditions - thay should also change Bp and it should be called before scale and pBvec.

const Epetra_SerialDenseMatrix MaterialProperties(fe_param.get<Epetra_SerialDenseMatrix>("material properties"));
const int numberOfMaterialProperties = MaterialProperties.LDA();

materials myMaterials = calculate_parameters(MaterialProperties);

///cout<<myMaterials.alpha<<endl;
///cout<<myMaterials.Se<<endl;

/// Pzero calculation
double PL = 1e-2; /// Load

double Pzero = myMaterials.alpha*myMaterials.M*PL/(myMaterials.Ku+4.0*myMaterials.mu/3.0);

/// Uzero calcuation

double L=15000.0; /// height

double Uzero = PL/(myMaterials.Ku+4.0*myMaterials.mu/3.0);

///double deltaL=500.0; /// element thickness

double myValue;

double myZ;

Epetra_Vector* myCoordinates(mesh->Coordinates());

///cout<<"my pid = "<<comm.MyPID()<<" and my Xu length = "<<Xu->MyLength()<<endl;

if((initialCondition)||(test1)){
for (int j = 0; j < Xu->MyLength()/3; j++){

    myZ = myCoordinates->Values()[3*j+2];

    myValue =-Uzero*myZ;

    Xu->ReplaceMyValue (3*j+2, 0, myValue);

    }


Xp->PutScalar(Pzero); /// apply initial condition  as initial guess.
}
Epetra_Vector * TempP;

TempP = new Epetra_Vector(Xp->Map());

TempP->PutScalar(0.0);

Bp->Multiply(1.0, *Ap, *Xp,1.0); /// update b with Se*P_zero

Apu->Apply(*Xu,*TempP);

Bp->Update(myMaterials.alpha, *TempP,1.0); /// update b with alpha*div_u

///scale RHS. Only left scale is required so only one scaling is performed per vector.
if(doScale=="scale") {
    my_poro->ScaleU(*Bu);
    my_poro->ScaleF(*Bf);
    my_poro->ScaleP(*Bp);
    INFO("RHS vectors are scaled");
}
//*/

/// creation of pB to define the RHS in Belos::LinearProblem
Epetra_Vector* pBvec = new Epetra_Vector(my_poro->compose_vector(*Bu, *Bf, *Bp ));


RCP<Epetra_Vector> pB  = rcp(pBvec);
INFO("B is created");

double bNorm=1.0;

pBvec->Norm2(&bNorm);
/// /////
if (test1) {
Epetra_Vector XuT(*Xu);

Epetra_Time myTime(comm);

for (int j = 0; j < 1000; j++){
    AuuP->Apply(*Xu,XuT);
}
XuT.Norm2(&bNorm);
if(comm.MyPID()==0) cout<<"Auu Norm = "<< bNorm<<" and time = "<<myTime.ElapsedTime()<<endl;
myTime.ResetStartTime();

for (int j = 0; j < 1000; j++){
    Aff->Apply(*Xf,*Xf);
}
Xf->Norm2(&bNorm);
if(comm.MyPID()==0) cout<<"Aff Norm = "<< bNorm<<" and time = "<<myTime.ElapsedTime()<<endl;
myTime.ResetStartTime();

for (int j = 0; j < 1000; j++){
    Apu->Apply(*Xu,*Xp);
}
Xp->Norm2(&bNorm);
if(comm.MyPID()==0) cout<<"Apu Norm = "<< bNorm<<" and time = "<<myTime.ElapsedTime()<<endl;
myTime.ResetStartTime();

for (int j = 0; j < 1000; j++){
    Apf->Apply(*Xf,*Xp);
}
Xp->Norm2(&bNorm);
if(comm.MyPID()==0) cout<<"Apf Norm = "<< bNorm<<" and time = "<<myTime.ElapsedTime()<<endl;


for (int j = 0; j < 1000; j++){
    Apu->Multiply(true,*Xp,*Xu);
}
Xu->Norm2(&bNorm);
if(comm.MyPID()==0) cout<<"Apu^T Norm = "<< bNorm<<" and time = "<<myTime.ElapsedTime()<<endl;
myTime.ResetStartTime();

for (int j = 0; j < 1000; j++){
    Apf->Multiply(true,*Xp,*Xf);
}
Xf->Norm2(&bNorm);
if(comm.MyPID()==0) cout<<"Apf^T Norm = "<< bNorm<<" and time = "<<myTime.ElapsedTime()<<endl;




pB        = Teuchos::null;
pA        = Teuchos::null;

#ifdef HAVE_MPI
MPI_Finalize();
#endif
exit(EXIT_SUCCESS);
}

/// /////
/// creation of pX to define the unknown in Belos::LinearProblem
Epetra_Vector* pXvec = new Epetra_Vector(my_poro->compose_vector(*Xu, *Xf, *Xp ));
RCP<Epetra_Vector> pX  = rcp(pXvec);
INFO("X is created");

if(doMatlab=="yes") {
    myMatlabVec = new MATLAB_VectorWriter<C_ASCII_GWriter>("Ap", comm);
    myMatlabVec->PrintVector(*Ap);

    myMatlabVec = new MATLAB_VectorWriter<C_ASCII_GWriter>("Bu", comm);
    myMatlabVec->PrintVector(*Bu);

    myMatlabVec = new MATLAB_VectorWriter<C_ASCII_GWriter>("Bp", comm);
    myMatlabVec->PrintVector(*Bp);

    myMatlabVec = new MATLAB_VectorWriter<C_ASCII_GWriter>("Xp", comm);
    myMatlabVec->PrintVector(*Xp);

    myMatlabVec = new MATLAB_VectorWriter<C_ASCII_GWriter>("Xu", comm);
    myMatlabVec->PrintVector(*Xu);

    //mw3 = new MATLAB_MatrixWriter<C_ASCII_GWriter>("Auu", comm);
    //mw3->PrintRows(dynamic_cast<Epetra_VbrMatrix*>(Auu));
    mw3 = new MATLAB_MatrixWriter<C_ASCII_GWriter>("AuuP", comm);
    mw3->PrintRows(dynamic_cast<Epetra_VbrMatrix*>(AuuP));

    mTest = CrsMatrix2MATLAB(*Apu);
    if(comm.MyPID()==0)  {
        cout<<"Apu=A;"<<endl;
        cout<<"clear A;"<<endl;
    }
    mTest = CrsMatrix2MATLAB(*Apf);
    if(comm.MyPID()==0)  {
        cout<<"Apf=A;"<<endl;
        cout<<"clear A;"<<endl;
    }
    mTest = CrsMatrix2MATLAB(*Aff);
    if(comm.MyPID()==0)  {
        cout<<"Aff=A;"<<endl;
        cout<<"clear A;"<<endl;
    }
    mTest = CrsMatrix2MATLAB(*Spp);
    if(comm.MyPID()==0)  {
        cout<<"Spp=A;"<<endl;
        cout<<"clear A;"<<endl;
    }

    mTest = CrsMatrix2MATLAB(*Sff);
    if(comm.MyPID()==0)  {
        cout<<"Sff=A;"<<endl;
        cout<<"clear A;"<<endl;
    }

    return 0;
}


/// #############################
///cout<<*mesh->SchurMap()<<endl;
///cout<<*mesh->SchurElementMap()<<endl;
///cout<<"my pid = "<<comm.MyPID()<<" and NumMyElements = "<<mesh->SchurElementMap()->NumMyElements()<<endl;
///return 0;
if (Xu == 0 || Bu == 0) {
    ERROR("Use Epetra_Vector in ElastictyProblem", 1);
}

vector<double> null_space;

//Compute null space for ml preconditioner
if (precType == "ml" || precType == "matrixfree")
    set_null_space(mesh, null_space);

Teuchos::RefCountPtr<Epetra_CrsGraph> graph; ///Teuchos:: is added

if (precType == "matrixfree") {
    ElementByElementMatrix* ebe = dynamic_cast<ElementByElementMatrix*>(ep->GetOperator());
    if (ebe != 0) {
        graph = Teuchos::rcp(new Epetra_CrsGraph(Copy, ebe->Map(), 0)); ///Teuchos:: is added
        mesh->MatrixGraph(*(graph.get()));
    }
}


/// declaration of the solver variables.

///Teuchos::RefCountPtr<Epetra_Vector> diagonal;
RCP<Belos::EpetraPrecOp> belosPrec;
RCP<Epetra_Operator> Prec;
PoroMatrixPreconditionerB *my_poro_pB  = 0;

if (precType == "Jacobi") {

    ElementByElementMatrix* ebe = dynamic_cast<ElementByElementMatrix*>(ep->GetOperator());
    std::cout<<"Jacobi solver is not yet implemented!"<<std::endl;
    std::cout<<"parfe exit"<<std::endl;
#ifdef HAVE_MPI
MPI_Finalize();
#endif
exit(EXIT_SUCCESS);

}
else if (precType == "matrixfree") {
    ElementByElementMatrix* ebe = dynamic_cast<ElementByElementMatrix*>(ep->GetOperator());
    std::cout<<"matrixfree solver is not yet implemented!"<<std::endl;
    std::cout<<"parfe exit"<<std::endl;
#ifdef HAVE_MPI
MPI_Finalize();
#endif
exit(EXIT_SUCCESS);

}
else if (precType == "ml") {

    Teuchos::ParameterList MLList;

    ML_Epetra::SetDefaults("SA", MLList);

    MLList.set("smoother: type (level 0)", "Chebyshev");
    MLList.set("smoother: type (level 1)", "symmetric Gauss-Seidel");
    MLList.set("coarse: max size", 1024);
    MLList.set("null space: dimension", 6);
    MLList.set("null space: type", "pre-computed");
    MLList.set("null space: vectors", &null_space[0]);
    MLList.set("aggregation: type", "MIS");
    //MLList.set("aggregation: type", "Uncoupled");
    //MLList.set("aggregation: type (level 0)", "ParMETIS");
    //MLList.set("aggregation: type (level 1)", "Uncoupled");
    //MLList.set("aggregation: type (level 2)", "MIS");
    //MLList.set("low memory usage", true);
    //MLList.set("max levels", 6);
    MLList.set("cycle applications", numCycleAuu);
    //MLList.set("prec type", "MGV");
    //MLList.set("eigen-analysis: iterations", 20);

    //MLList.set("aggregation: damping factor", 1.333);
    //MLList.set("aggregation: threshold", 0.0);

    //MLList.set("smoother: sweeps", 6);

    //MLList.set("aggregation: damping factor", 1.333);
    //MLList.set("aggregation: type", "Uncoupled-MIS");
    //MLList.set("aggregation: type", "ParMETIS");
    //MLList.set("coarse: type", "Amesos-KLU");

    null_space.resize(0);

    if (verbose) MLList.set("ML output", 10);
    else         MLList.set("ML output", 0);

    Teuchos::ParameterList MLList_Spp;
    ML_Epetra::SetDefaults("SA", MLList_Spp);

    ///MLList_Spp.set("smoother: type (level 0)", "Chebyshev");
    ///MLList_Spp.set("smoother: type (level 1)", "symmetric Gauss-Seidel");
    ///MLList_Spp.set("smoother: type", "Chebyshev");
    MLList_Spp.set("smoother: type", "symmetric Gauss-Seidel");
    ///MLList_Spp.set("coarse: max size", 1024);
    //MLList_Spp.set("null space: dimension", 6);
    //MLList_Spp.set("null space: type", "pre-computed");
    //MLList_Spp.set("null space: vectors", &null_space[0]);
    //MLList_Spp.set("aggregation: type", "Uncoupled");
    //MLList_Spp.set("aggregation: type", "MIS");
    //MLList.set("aggregation: type (level 0)", "ParMETIS");
    //MLList.set("aggregation: type (level 1)", "Uncoupled");
    //MLList.set("aggregation: type (level 2)", "MIS");
    ///MLList_Spp.set("low memory usage", true);
    //MLList_Spp.set("max levels", 3);
    MLList_Spp.set("cycle applications", numCycleSpp);
    MLList_Spp.set("prec type", "MGV");



    PoroMatrixPreconditioner *my_poro_p = 0 ;

//    PoroMatrixPreconditionerB *my_poro_pB  = 0;

    PoroMatrixPreconditionerC *my_poro_pC  = 0;

    PoroMatrixPreconditionerD *my_poro_pD  = 0;

    INFO("PoroMatrixPreconditioner is defined");

    if(precClass=="a") {
        my_poro_p = new PoroMatrixPreconditioner(*AuuP, *Af, *Spp, comm, MLList, MLList_Spp);
        Prec = rcp(my_poro_p);
    }

    if(precClass=="b") {
        my_poro_pB = new PoroMatrixPreconditionerB(*AuuP, *Aff, *Apu, *Apf, *Af, *Ap, *Spp, comm, MLList, MLList_Spp, AuuTolerance, AffTolerance, SppTolerance, AuuMaxIters, AffMaxIters, SppMaxIters);
        Prec = rcp(my_poro_pB);
    }

    if(precClass=="c") {
        my_poro_pC = new PoroMatrixPreconditionerC(*AuuP, *Aff, *Apu, *Apf, *Af, *Ap, *Spp, comm, MLList, MLList_Spp, AuuTolerance, AffTolerance, SppTolerance, AuuMaxIters, AffMaxIters, SppMaxIters);
        Prec = rcp(my_poro_pC);
    }

    if(precClass=="d") {
        my_poro_pD = new PoroMatrixPreconditionerD(*AuuP, *Aff, *Apu, *Apf, *Af, *Ap, *Spp, comm, MLList, MLList_Spp, AuuTolerance, AffTolerance, SppTolerance, AuuMaxIters, AffMaxIters, SppMaxIters);
        Prec = rcp(my_poro_pD);
    }

    assert(Prec != Teuchos::null);

    belosPrec = rcp( new Belos::EpetraPrecOp(Prec)); ///define a belos preconditioner through Prec.

}

else {

    WARN("No preconditioner used");
}



INFO("Time used to build preconditioner: " << timer.WallTime() - temp_time);
memused = meminfo() - memused;
INFO("Mem used to hold preconditioner: " << memused << "MB");



/// belos parameters:
RCP<Teuchos::ParameterList> belosList  = rcp(new Teuchos::ParameterList);
//Teuchos::ParameterList belosList;
Teuchos::ParameterList belosList2;
//belosList.set( "Block Size", 1 );  /// current implementation of minres allows only 1.
belosList->set( "Maximum Iterations", maxIters );   /// Maximum number of iterations allowed
///belosList.set( "Convergence Tolerance", tolerance); /// Relative convergence tolerance requested
belosList->set( "Output Frequency", outputFrequency);
belosList->set( "Output Style", (int) Belos::Brief);
if (maXTimeStep<2) belosList->set( "Verbosity", Belos::Errors+Belos::StatusTestDetails+Belos::Warnings+Belos::TimingDetails+Belos::IterationDetails+Belos::FinalSummary );
///belosList->set( "Verbosity", Belos::Errors+Belos::StatusTestDetails+Belos::Warnings+Belos::TimingDetails+Belos::IterationDetails+Belos::FinalSummary );

///belosList->set( "Verbosity", Belos::Errors+Belos::StatusTestDetails+Belos::Warnings+Belos::IterationDetails+Belos::FinalSummary );
///belosList->set( "Verbosity", Belos::FinalSummary );

if (krylovType != "minres")  belosList->set( "Maximum Restarts", maxRestarts );   /// Maximum number of restarts allowed
if (krylovType != "minres") belosList->set( "Num Blocks", numBlocks );   /// Maximum number of restarts allowed

if ((krylovType == "gmres") && (precDir=="left")) belosList->set( "Explicit Residual Test", false ); /// minres has no explicit residual test yet and fgmres is not applicable with left prec.
//belosList->set( "Explicit Residual Test", true ); /// minres has no explicit residual test yet and fgmres is not applicable with left prec.
if (krylovType == "fgmres") belosList->set( "Flexible Gmres", true );     /// Flexible Gmres is activated

///belosList->set( "Explicit Residual Scaling", "Norm of Initial Residual" );
///belosList->set( "Explicit Residual Scaling", "Norm of RHS" ); /// or belosList->set( "Explicit Residual Scaling", "Norm of Right-Hand Side" );
///belosList->set( "Explicit Residual Scaling", "Norm of Preconditioned Initial Residual" );
///belosList->set( "Explicit Residual Scaling", "None" );



if ((krylovType == "fgmres") && (precDir=="left")) {

WARN("fgmres could not be left preconditioned");
INFO("precDir is changed to right !");
precDir="right";

}


/// belos related declarations

typedef Epetra_MultiVector MV;
typedef Epetra_Operator OP;

typedef double ST;

typedef Belos::OperatorTraits<ST, MV, OP> OPT;
typedef Belos::MultiVecTraits<ST, MV> MVT;


/// define linear problem:

Belos::LinearProblem<double, MV, OP>* my_problem = new Belos::LinearProblem<double, MV, OP>(pA, pX, pB);

RCP<Belos::LinearProblem<double, MV, OP> > problem  = rcp(my_problem);

/// set the preconditioner for the problem.
if ((precType == "ml") && (precDir == "left")) problem->setLeftPrec(belosPrec);
if ((precType == "ml") && (precDir == "right")) problem->setRightPrec(belosPrec);

bool set = problem->setProblem();

if (set == false) {
    if (MyPID==0)
        std::cout << std::endl << "ERROR:  problem set up is not correct" << std::endl;
    return -1;
}


/// calculate a modified norm to able to check ||b-Au||_2/||b||_2 as the convergence criteria

double initialNorm;
///calculate the absolute norm ||b-Ax||


const Epetra_Vector* myResVecZero = problem->getInitResVec()->operator()(0);
myResVecZero->Norm2(&initialNorm);
if (comm.MyPID()==0) cout<<"initial norm = "<<initialNorm<<endl;
if (comm.MyPID()==0) cout<<"bnorm = "<<bNorm<<endl;

double belosTolerance;
///belosTolerance = tolerance*bNorm/initialNorm;
belosTolerance = tolerance;
///belosTolerance = tolerance*bNorm;
//belosTolerance = tolerance*initialNorm;

///if (belosTolerance>1.0) belosTolerance = 1e-5;

belosList->set( "Convergence Tolerance", belosTolerance); /// Relative convergence tolerance
///if (comm.MyPID()==0) cout<<"CONVERGENCE NORM = "<<belosTolerance<<endl;

/// solver manager for Krylov solvers.


RCP< Belos::SolverManager<double, MV, OP> > solver;


if (krylovType != "minres") solver = rcp( new Belos::BlockGmresSolMgr<double, MV, OP>(problem, belosList) ); // gmres or fgmres
///if (krylovType != "minres") solver = rcp( new Belos::PseudoBlockGmresSolMgr<double, MV, OP>(problem, belosList) ); // gmres or fgmres
if (krylovType == "minres") solver = rcp( new Belos::MinresSolMgr<double, MV, OP>(problem, belosList) );




Epetra_MultiVector* mySolution;
Epetra_Vector* mySolvecZero;
Epetra_Vector* testVec;

double finalUNorm;
double finalFNorm;
double finalPNorm;

double aaa;

double finalNorm;

int uGlobalLength, uLocalLength;
int fGlobalLength, fLocalLength;
int pGlobalLength, pLocalLength;

uGlobalLength=Xu->GlobalLength();
fGlobalLength=Xf->GlobalLength();
pGlobalLength=Xp->GlobalLength();
uLocalLength=Xu->MyLength();
fLocalLength=Xf->MyLength();
pLocalLength=Xp->MyLength();



Belos::ReturnType ret;

int currentIters;
int totalIters=0;
double averageIters=0.0;

/// #####################
/// time loop starts here.
for (int timeStep=0; timeStep<maXTimeStep; timeStep++){

if (timeStep==maXTimeStep-2) belosList->set( "Verbosity", Belos::Errors+Belos::StatusTestDetails+Belos::Warnings+Belos::TimingDetails+Belos::IterationDetails+Belos::FinalSummary );
///belosList->set( "Verbosity", Belos::Errors+Belos::StatusTestDetails+Belos::Warnings+Belos::TimingDetails+Belos::IterationDetails+Belos::FinalSummary );

//------------------------beginning of solution------------------------
/*
if (comm.MyPID()==0) {

cout<<endl;
cout<<"+++++++++++++++++++++++++++++++++++++++++++++++++++++++++"<<endl;
cout<<"+++++++++++++++++++++++++++++++++++++++++++++++++++++++++"<<endl;
cout<<"TIME STEP = "<<timeStep+1<<" and TIME = "<<deltaT*(timeStep+1)<<endl;
cout<<"BELOS TOLERANCE = "<<belosTolerance<<endl;
cout<<"+++++++++++++++++++++++++++++++++++++++++++++++++++++++++"<<endl;
cout<<"+++++++++++++++++++++++++++++++++++++++++++++++++++++++++"<<endl;
cout<<endl;
}
*/


/// ####################################
/// solve the problem.
ret = solver->solve();

currentIters = solver->getNumIters();
totalIters = totalIters + currentIters;
///if (comm.MyPID()==0) cout<<timeStep<<" "<<currentIters<<endl;
/// ####################################

///*************************************************************

///  currently inactive
if (precType == "matrixfree" || precType == "Jacobi") {
    ElementByElementMatrix* ebe = dynamic_cast<ElementByElementMatrix*>(ep->GetOperator());
    ebe->ResetRestrainedBoundaries(Xu);
}

///diagonal = Teuchos::null;


//------------------------------Print solution-------------------------

/// get the solution from the problem
///mySolution = problem->getLHS().get();

mySolvecZero = problem->getLHS()->operator()(0);

testVec = new Epetra_Vector(*pXvec);

///Prec->Apply(*mySolvecZero,*mySolvecZero);

my_poro->extract_partial_vectors(*Xu, *Xf, *Xp, *mySolvecZero);



///calculate the absolute norm ||b-Ax||

pA->Apply(*testVec,*testVec);
testVec->Update(1.0, *pBvec,-1.0);
testVec->Norm2(&finalNorm);
///if (comm.MyPID()==0) cout<<"|| absolute final norm is = "<<finalNorm<<" ||"<<endl;
///if (comm.MyPID()==0) cout<<"|| relative convergence is = "<<finalNorm/initialNorm<<" ||"<<endl;



///if (comm.MyPID()==0) cout<<"before: "<<Xp->Values()[0]<<endl;
/// scale the solution back to the original order.

///*
if(doScale=="scale") {
    my_poro->ScaleU(*Xu);
    my_poro->ScaleF(*Xf);
    my_poro->ScaleP(*Xp);
}
//*/


/// prepare the LHSs for output.
ep->SetLHS(Xu);
fp->SetLHS(Xf);
pp->SetLHS(Xp);



/// update righthand side using with u and p. using update operations on B side.
Bp->PutScalar(0.0); /// reset Bp

Bp->Multiply(1.0, *Ap, *Xp,1.0); /// update b with Se*P_zero

TempP->PutScalar(0.0);
Apu->Apply(*Xu,*TempP);

Bp->Update(myMaterials.alpha, *TempP,1.0); /// update b with alpha*div_u

my_poro->update_vector(*Bp, *pB); /// update pB

pBvec->Norm2(&bNorm);

///belosTolerance = tolerance*bNorm/initialNorm;
belosTolerance = tolerance;
///belosTolerance = tolerance*bNorm;

///if (belosTolerance>1.0) belosTolerance = 1e-5;

belosList->set( "Convergence Tolerance", belosTolerance); /// Relative convergence tolerance

solver->setParameters(belosList);

problem->setProblem(pX,pB);

}

/// time loop ends here
/// ###################

delete TempP;


averageIters = totalIters/double(maXTimeStep);
/// display the first pressure value on the solution with scaling
//if (comm.MyPID()==0){

if (comm.MyPID()==comm.NumProc()-1){

cout<<endl;
//cout<<"+----------------------------------+"<<endl;
//cout<<">>> last node x-displacement = "<<Xu->Values()[uLocalLength-3]<<" <<<"<<endl;
//cout<<">>> last node y-displacement = "<<Xu->Values()[uLocalLength-2]<<" <<<"<<endl;
//cout<<">>> last node z-displacement = "<<Xu->Values()[uLocalLength-1]<<" <<<"<<endl;
//cout<<">>> top face flux = "<<Xf->Values()[fLocalLength-1]<<" <<<"<<endl;
//cout<<">>> first element pressure = "<<Xp->Values()[0]<<" <<<"<<endl;
//cout<<">>> last element pressure = "<<Xp->Values()[pLocalLength-1-1]<<" <<<"<<endl;
cout<<"+----------------------------------+"<<endl;
cout<<"TOTAL ITERS = "<< totalIters << " AVERAGE ITERS = "<<setprecision(3)<<averageIters<<endl;
cout<<"+----------------------------------+"<<endl;
if(precClass=="b") {
    cout<<"Total Auu iters = "<<my_poro_pB->totalAuuIters()<<endl;
    cout<<"Total Aff iters = "<<my_poro_pB->totalAffIters()<<endl;
    cout<<"Total Spp iters = "<<my_poro_pB->totalSppIters()<<endl;
}
cout<<endl;

}


if(Xu->Map().MyGID(Xu->Map().MaxAllGID())){
    cout<<"+-----------------------------------------+"<<endl;
    cout<<">>> last node x-displacement = "<<Xu->Values()[Xu->Map().LID(Xu->Map().MaxAllGID())-2]<<" <<<"<<endl;
    cout<<">>> last node y-displacement = "<<Xu->Values()[Xu->Map().LID(Xu->Map().MaxAllGID())-1]<<" <<<"<<endl;
    cout<<">>> last node z-displacement = "<<Xu->Values()[Xu->Map().LID(Xu->Map().MaxAllGID())]<<" <<<"<<endl;
    cout<<"+-----------------------------------------+"<<endl;
}




if(Xf->Map().MyGID(Xf->Map().MaxAllGID())){
    cout<<"+-----------------------------------------+"<<endl;
    cout<<">>> top face flux = "<<Xf->Values()[Xf->Map().LID(Xf->Map().MaxAllGID())]<<" <<<"<<endl;
    cout<<"+-----------------------------------------+"<<endl;
}


if(Xp->Map().MyGID(Xp->Map().MinAllGID())){
    cout<<"+-----------------------------------------+"<<endl;
    cout<<">>> first element pressure = "<<Xp->Values()[Xp->Map().LID(Xp->Map().MinAllGID())]<<" <<<"<<endl;
    cout<<"+-----------------------------------------+"<<endl;
}
if(Xp->Map().MyGID(Xp->Map().MaxAllGID())){
    cout<<"+-----------------------------------------+"<<endl;
    cout<<">>> last element pressure = "<<Xp->Values()[Xp->Map().LID(Xp->Map().MaxAllGID())]<<" <<<"<<endl;
    cout<<"+-----------------------------------------+"<<endl;
}




temp_time = timer.WallTime();
///*
if (inputFileName.find(".mesh.h5") == string::npos) {
    int ext_pos =  inputFileName.rfind(".mesh");
    inputFileName = inputFileName.substr(0, ext_pos)+".mesh.h5";
    IBT_ProblemWriter<HDF5_GWriter> pw(inputFileName , comm);
    fe_param.Print(&pw);
    mesh->Print(&pw);
    bcond->Print(&pw);
}

///*
if ((inputFileName.find(".mesh.h5") != string::npos)&&(printResult)) {
    ///we are currently using this one. because the mesh info is already there.
    IBT_SolutionWriter<HDF5_GWriter> sw(inputFileName, comm);
    /// print the displacement solution
    ep->PrintSolution(&sw, fe_param.get<Epetra_SerialDenseMatrix>("material properties"));
    /// print the flux solution
    fp->PrintSolution(&sw);
    /// print the pressure solution
    pp->PrintSolution(&sw);
    // might need to modify the stress values with pressure to get absolute values.
}
//*/

if (printMEDIT) {
    MEDIT_DispWriter dispw(outputFileName + ".medit.bb", comm, 3);
    ep->PrintLHS(&dispw);
}

INFO("output time: " << timer.WallTime() -temp_time);
//*/
comm.Barrier();
INFO("Total time used: " << timer.WallTime() -start_time);
INFO("ALL MB: " << meminfo() << "MB");
// ======================= //
// Finalize MPI and exit   //
// ----------------------- //


/// erhan added
/// reset smart pointers: assing to null pointer

pX        = Teuchos::null;
pB        = Teuchos::null;
pA        = Teuchos::null;

belosPrec = Teuchos::null;
Prec      = Teuchos::null;
problem   = Teuchos::null;
solver    = Teuchos::null;
belosList = Teuchos::null;


#ifdef HAVE_MPI
MPI_Finalize();
#endif
exit(EXIT_SUCCESS);


}
