/*
 * ParFE: a micro-FE solver for trabecular bone modeling
 * Copyright (C) 2006 ParFE developers, see
 * http://parfe.sourceforge.net/developers.php
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

/*
 * This code should be used to perform a single finite element analysis
 * of a trabecular bone, stored in either ASCII or HDF5 format. Three
 * preconditioners are available: "Jacobi" (for comparisons only, not
 * for production), "ml" (matrix-ready multilevel preconditioner)
 * and "matrixfree" (matrix-free multilevel preconditioner).
 *
 * \author Marzio Sala
 *
 * \date Last modified on 15-Nov-06
 */

#include "parfe_ConfigDefs.h"
#include <string>
#include <fstream>
#include <vector>
#include <Epetra_Time.h>
#ifdef HAVE_MPI
#include <mpi.h>
#include <Epetra_MpiComm.h>
#else
#include <Epetra_SerialComm.h>
#endif
#include <Epetra_Vector.h>
#include <Epetra_IntVector.h>
#include <time.h>
#include "DistMesh.h"
#include "MEDIT_MeshWriter.hpp"
#include "PMVIS_MeshWriter.hpp"
#include "MEDIT_DispWriter.hpp"
#include "IBT_ProblemReader.hpp"
#include "IBT_ProblemWriter.hpp"
#include "MATLAB_MatrixWriter.hpp"
#include "MATLAB_VectorWriter.hpp"
#include "IBT_DispWriter.hpp"
#include "IBT_SolutionWriter.hpp"
#include "EbeElasticityProblem.h"
#include "VbrElasticityProblem.h"
#include "Jacobi.h"
#include "Tools.h"
#include "FEParameterList.hpp"

#include <ml_MultiLevelPreconditioner.h>
#include <ml_MatrixFreePreconditioner.h>
#include <AztecOO.h>
///new belos includes*****
#include "BelosConfigDefs.hpp"
#include "BelosLinearProblem.hpp"
#include "BelosEpetraAdapter.hpp"
#include "BelosPCPGSolMgr.hpp"
#include "BelosMinresSolMgr.hpp"
#include <Teuchos_ParameterList.hpp>
#include <Teuchos_RefCountPtr.hpp>
#include <Teuchos_CommandLineProcessor.hpp>
#include "Teuchos_RCP.hpp"
////#include "BelosBlockCGSolMgr.hpp"

#include "ml_include.h" //--enable-epetra --enable-teuchos.
#include "Epetra_Map.h"


///#include "Epetra_CrsMatrix.h"
///#include "EpetraExt_MatrixMatrix.h"  // build the example


///using namespace ML_Epetra;
///using namespace Teuchos;

#include "PoroMatrixB.h" /// erhan added
#include "PoroMatrixPreconditionerB.h" /// erhan added



// =========== //
// main driver //
// =========== //

int main( int argc, char* argv[] ) {
#ifdef HAVE_MPI
    MPI_Init(&argc, &argv);
// define an Epetra communicator
Epetra_MpiComm comm(MPI_COMM_WORLD);
#else
Epetra_SerialComm comm;
#endif

/// /*
using Teuchos::ParameterList; // all of this may look fine but do not be fooled ...
using Teuchos::RCP;           // it is not so clear what any of this does
using Teuchos::rcp;
/// */

// get the proc ID of this process
int MyPID = comm.MyPID();

// get the total number of processes
int NumProc = comm.NumProc();

Epetra_Time timer(comm);
double start_time = timer.WallTime();
double temp_time = start_time;


//Macros to print information
#define INFO(mesg) \
if (MyPID == 0) cout << "*** INFO ***: " << mesg << endl;

#define WARN(mesg) \
if (MyPID == 0) cout << "*** WARNING ***: " << mesg << endl;

#define ERROR(mesg, exit_code)\
if (MyPID == 0) cerr << "*** ERROR ***: " << mesg << endl; \
        exit(exit_code);

Teuchos::CommandLineProcessor CLP;

char help[] = "This is the latest version of the ParFE driver.\n"
        "It can solve problems stored in ASCII or HDF5 format, perform\n"
        "load balance, build the preconditioner for matrix-free or\n"
        "matrix-ready problems, and print out the solution.\n";
CLP.setDocString(help);

string inputFileName = "../mesh/simple_test.mesh";
string outputFileName = "./output";
string precType = "ml";
string repartition = "parmetis";
int maxIters = 1550;
double tolerance = 1.e-5;
bool verbose = true;
bool printMEDIT = false;
bool printPMVIS = false;
int outputFrequency = 16;

CLP.setOption("filename", &inputFileName, "Name of input file");
CLP.setOption("output", &outputFileName, "Base name of output file");
CLP.setOption("precond", &precType,
        "Preconditioner to be used [Jacobi/ml/matrixfree]");
CLP.setOption("repart", &repartition,
        "Repartioner to be used [parmetis/rcb]");

CLP.setOption("maxiters", &maxIters, "Maximum CG iterations");
CLP.setOption("tolerance", &tolerance, "Tolerance for CG");
CLP.setOption("verbose", "silent", &verbose,
        "Set verbosity level");
CLP.setOption("printmedit", "noprintmedit", &printMEDIT,
        "Print solution in MEDIT format");
CLP.setOption("printpmvis", "noprintpmvis", &printPMVIS,
        "Print partition in PMVIS format");
CLP.setOption("frequency", &outputFrequency,
        "Prints out residual every specified iterations");

CLP.recogniseAllOptions(false);
CLP.throwExceptions(false);

if (CLP.parse(argc, argv) == Teuchos::CommandLineProcessor::PARSE_HELP_PRINTED) {
#ifdef HAVE_MPI
    MPI_Finalize();
#endif
exit(EXIT_SUCCESS);
}

FEParameterList fe_param(NumProc);
fe_param.set("input", outputFileName);

Epetra_MultiVector* BCs = 0;

ProblemReader* pr = 0;
if (inputFileName.find(".mesh.h5") != string::npos)
    pr = new IBT_ProblemReader<HDF5_GReader>(inputFileName, comm);
else if (inputFileName.find(".mesh") != string::npos)
    pr = new IBT_ProblemReader<C_ASCII_GReader>(inputFileName, comm);
else {
    cerr << "File name (" << inputFileName << ") not in ASCII or HDF5 format" << endl;
    exit(EXIT_FAILURE);
}

fe_param.Scan(pr);

INFO("#nodes       = " << fe_param.get<int>("#nodes"));
INFO("#dimensions  = " << fe_param.get<int>("#dimensions"));
INFO("#elements    = " << fe_param.get<int>("#elements"));
INFO("#faces       = " << fe_param.get<int>("#faces"));
INFO("#free faces  = " << fe_param.get<int>("#free faces"));

temp_time = timer.WallTime();

//If these parameters are still unset, provide some default values
fe_param.get("iteration limit", maxIters);
fe_param.get("tolerance", tolerance);

DistMesh* mesh = new DistMesh(fe_param.get<int>("#nodes"), fe_param.get<int>("#dimensions"), fe_param.get<int>("#elements"), fe_param.get<int>("#nodes per element"), fe_param.get<IntMap>("material ids"), comm, fe_param.get<int>("#faces"), fe_param.get<int>("#free faces"),1,1,1);

mesh->Scan(pr);
BoundaryCondition* bcond = new BoundaryCondition(fe_param.get<int>("#dofs per node"), comm);
bcond->Scan(pr);

delete pr;

//--------------------------end of input--------------------------------

INFO("Input Time: " << timer.WallTime() - temp_time);
temp_time = timer.WallTime();


if (repartition == "parmetis"){
    mesh->Redistribute(true, verbose);
}
else if (repartition == "rcb")
    mesh->RedistributeWIsorropia(true, verbose);
else
    mesh->Redistribute(false, verbose);

if (repartition == "parmetis" || repartition == "rcb") {
    INFO("Time used to partition mesh: " << timer.WallTime() - temp_time);
} else {
    WARN("No load balancing used");
}

//redistribute the boundary condition data according to the node map
bcond->Redistribute(mesh->NodeMap()->NumMyElements(), mesh->NodeMap()->MyGlobalElements());

if (printMEDIT) {
    MEDIT_MeshWriter meshw(outputFileName + ".medit.mesh", 3, comm);
    mesh->Print(&meshw);
}

if (fe_param.get<bool>("print for PMVIS")) {
    std::string con = outputFileName + ".con";
    std::string xyz = outputFileName + ".xyz";
    std::string part = outputFileName + ".part";
    PMVIS_MeshWriter meshw(xyz, con, part, comm);
    mesh->Print(&meshw);
}

int memused = meminfo();
INFO("Mem used to hold mesh information: " << memused <<  "MB");

INFO("Total Mesh setup time: " << timer.WallTime() - start_time);

//-------------------------beginning of assembly------------------------

temp_time = timer.WallTime();

ElasticityProblem* ep;
if (precType == "Jacobi" || precType == "matrixfree") {
    fe_param.set("element by element", true);
    ep = new EbeElasticityProblem(*mesh, fe_param);
}
else
    ep = new VbrElasticityProblem(*mesh, fe_param, 0.0, 1);

ep->Impose(*bcond);

BCs = new Epetra_MultiVector(*ep->GetRHS());

INFO("Time used to assemble matrix: " << timer.WallTime() - temp_time);

memused = meminfo() - memused;
INFO("Mem used to hold stiffness matrix operator: " << memused << "MB");

Epetra_Vector* X = dynamic_cast<Epetra_Vector*>(ep->GetLHS());
Epetra_Vector* B = dynamic_cast<Epetra_Vector*>(ep->GetRHS());


///manage the vectors as RCP objects.
RCP<Epetra_Vector> pX=rcp(X);
RCP<Epetra_Vector> pB=rcp(B);
RCP<Epetra_Operator> Auu=rcp(ep->GetMatrix());

VectorWriter* myMatlabVec = 0;
    myMatlabVec = new MATLAB_VectorWriter<C_ASCII_GWriter>("XB", comm);
    myMatlabVec->PrintVector(*X);


if (X == 0 || B == 0) {
    ERROR("Use Epetra_Vector in ElastictyProblem", 1);
}

vector<double> null_space;

//Compute null space for ml preconditioner
if (precType == "ml" || precType == "matrixfree")
    set_null_space(mesh, null_space);

Teuchos::RefCountPtr<Epetra_CrsGraph> graph; ///Teuchos:: is added

if (precType == "matrixfree") {
    ElementByElementMatrix* ebe = dynamic_cast<ElementByElementMatrix*>(ep->GetOperator());
    if (ebe != 0) {
        graph = Teuchos::rcp(new Epetra_CrsGraph(Copy, ebe->Map(), 0)); ///Teuchos:: is added
        mesh->MatrixGraph(*(graph.get()));
    }
}

//------------------------beginning of solution------------------------
//Epetra_RowMatrix *AuuO=ep->GetOperator();
Epetra_RowMatrix *AuuP=ep->GetMatrix();

///Teuchos::RefCountPtr<Epetra_Vector> diagonal;
RCP<Belos::EpetraPrecOp> belosPrec;
RCP<Epetra_Operator> Prec;

if (precType == "Jacobi") {

    ElementByElementMatrix* ebe = dynamic_cast<ElementByElementMatrix*>(ep->GetOperator());
    std::cout<<"Jacobi solver is not yet implemented!"<<std::endl;
    std::cout<<"parfe exit"<<std::endl;
#ifdef HAVE_MPI
MPI_Finalize();
#endif
exit(EXIT_SUCCESS);

}
else if (precType == "matrixfree") {
    ElementByElementMatrix* ebe = dynamic_cast<ElementByElementMatrix*>(ep->GetOperator());
    std::cout<<"matrixfree solver is not yet implemented!"<<std::endl;
    std::cout<<"parfe exit"<<std::endl;
#ifdef HAVE_MPI
MPI_Finalize();
#endif
exit(EXIT_SUCCESS);

}
else if (precType == "ml") {

    // allocate the ML preconditioner
    Teuchos::ParameterList MLList;


    ML_Epetra::SetDefaults("SA", MLList);
    ///MLList.set("smoother: type (level 0)", "Chebyshev");
    ///MLList.set("smoother: type (level 1)", "symmetric Gauss-Seidel");
    ///MLList.set("coarse: max size", 1024);
    /*
    MLList.set("null space: dimension", 6);
    MLList.set("null space: type", "pre-computed");
    MLList.set("null space: vectors", &null_space[0]);
    */
    ///MLList.set("aggregation: type", "Uncoupled");
    //MLList.set("aggregation: type (level 0)", "ParMETIS");
    //MLList.set("aggregation: type (level 1)", "Uncoupled");
    //MLList.set("aggregation: type (level 2)", "MIS");
    ///MLList.set("low memory usage", true);
    //MLList.set("max levels", 6);
    if (verbose) MLList.set("ML output", 0);
    else         MLList.set("ML output", 0);




    ///Prec = rcp(  new ML_Epetra::MultiLevelPreconditioner(*ep->GetMatrix(), MLList, true) );


    ///PoroMatrixPreconditionerB *my_poro_p = new PoroMatrixPreconditionerB(*AuuP, comm, MLList);

    ///Prec = rcp(my_poro_p);





    ///assert(Prec != Teuchos::null);


    ///belosPrec = rcp( new Belos::EpetraPrecOp(Prec) );








}

else {
    ///solver.SetAztecOption(AZ_precond, AZ_none); //TODO
    WARN("No preconditioner used");
}


INFO("Time used to build preconditioner: " << timer.WallTime() - temp_time);
memused = meminfo() - memused;
INFO("Mem used to hold preconditioner: " << memused << "MB");

// solution is constant
X->PutScalar(0.0);

/*
MatrixWriter* mw3 = 0;
mw3 = new MATLAB_MatrixWriter<C_ASCII_GWriter>("AuuB", comm);
mw3->PrintRows(dynamic_cast<Epetra_VbrMatrix*>(ep->GetMatrix()));
*/

/// belos parameters:
Teuchos::ParameterList belosList;
belosList.set( "Block Size", 1 );              // Blocksize to be used by iterative solver
belosList.set( "Maximum Iterations", maxIters );   // Maximum number of iterations allowed
belosList.set( "Convergence Tolerance", tolerance);// Relative convergence tolerance requested
belosList.set( "Verbosity", Belos::Errors+Belos::Warnings+Belos::TimingDetails+Belos::FinalSummary );
///belosList.set( "Output Frequency", 10); not wotrking?


/// declarations

typedef Epetra_MultiVector                MV;
typedef Epetra_Operator                   OP;

typedef double                          ST;

typedef Belos::OperatorTraits<ST, MV, OP> OPT;// TODO
typedef Belos::MultiVecTraits<ST, MV>    MVT;// TODO







/// new declarations of the interface

/// define the interface for matrix-vector multiplication
//PoroMatrixB *my_poro = new PoroMatrixB(*bcond, comm);
PoroMatrixB *my_poro = new PoroMatrixB(*AuuP, *bcond, comm);





/// creation of pA to define the matrix in Belos::LinearProblem
RCP<Epetra_Operator> pA = rcp(my_poro);







/// linear preconditioned problem:

//RCP<Belos::LinearProblem<double, MV, OP> > problem = rcp( new Belos::LinearProblem<double, MV, OP>(Auu, pX, pB));
RCP<Belos::LinearProblem<double, MV, OP> > problem = rcp( new Belos::LinearProblem<double, MV, OP>(pA, pX, pB));


//problem->setLeftPrec(belosPrec);


bool set = problem->setProblem();
if (set == false) {
    if (MyPID==0)
        std::cout << std::endl << "ERROR:  problem set up is not correct" << std::endl;
    return -1;
}


/// solver manager - Minres
RCP< Belos::SolverManager<double, MV, OP> > solver = rcp( new Belos::MinresSolMgr<double, MV, OP>(problem, rcp(&belosList, false)) );



/// /////////////
double initialNorm;
const Epetra_Vector* myResVecZero = problem->getInitResVec()->operator()(0);
myResVecZero->Norm2(&initialNorm);
if (comm.MyPID()==0) cout<<"initial norm = "<<initialNorm<<endl;

/// ///////////////


Belos::ReturnType ret = solver->solve();


/// /////////////
Epetra_Vector* testVec = new Epetra_Vector(*X);
double finalNorm;
pA->Apply(*testVec,*testVec);
testVec->Update(1.0, *B,-1.0);
testVec->Norm2(&finalNorm);
if (comm.MyPID()==0) cout<<"|| absolute final norm is = "<<finalNorm<<" ||"<<endl;
if (comm.MyPID()==0) cout<<"|| relative convergence is = "<<finalNorm/initialNorm<<" ||"<<endl;

/// ///////////////








if (precType == "matrixfree" || precType == "Jacobi") {
    ElementByElementMatrix* ebe = dynamic_cast<ElementByElementMatrix*>(ep->GetOperator());
    ebe->ResetRestrainedBoundaries(X);
}

///diagonal = Teuchos::null;


//------------------------------Print solution-------------------------

temp_time = timer.WallTime();

if (inputFileName.find(".mesh.h5") == string::npos) {
    int ext_pos =  inputFileName.rfind(".mesh");
    inputFileName = inputFileName.substr(0, ext_pos)+".mesh.h5";
    IBT_ProblemWriter<HDF5_GWriter> pw(inputFileName , comm);
    fe_param.Print(&pw);
    mesh->Print(&pw);
    bcond->Print(&pw);
}


if (inputFileName.find(".mesh.h5") != string::npos) {
    IBT_SolutionWriter<HDF5_GWriter> sw(inputFileName, comm);
    ep->PrintSolution(&sw, fe_param.get<Epetra_SerialDenseMatrix>("material properties"));
}

if (printMEDIT) {
    MEDIT_DispWriter dispw(outputFileName + ".medit.bb", comm, 3);
    ep->PrintLHS(&dispw);
}

INFO("output time: " << timer.WallTime() -temp_time);

comm.Barrier();
INFO("Total time used: " << timer.WallTime() -start_time);

// ======================= //
// Finalize MPI and exit //
// ----------------------- //

Epetra_MultiVector* mySolution = problem->getLHS().get();

//const Epetra_Vector* mySolvecZero = problem->getLHS()->operator()(0); /// if constant, cannot overwritten.
Epetra_Vector* mySolvecZero = problem->getLHS()->operator()(0);

cout<<mySolvecZero->Values()[0]<<endl;
cout<<mySolvecZero->Values()[1]<<endl;
cout<<mySolvecZero->Values()[2]<<endl;
//cout<<mySolvecZero->Map()<<endl;






/// erhan added
/// reset smart pointers: assing to null pointer
/// Teuchos::RCP Beginner's Guide - September 2010. Appendix B 2b.
///manage the vectors as RCP objects.
/*
pX = null;
pB = null;
Auu = null;
Prec = null;
belosPrec = null;
problem=null;
solver=null;
*/


return 0; /// erhan added
///should be active in Minres else we get the same RCP error. Cyril's main-hack idea might work but not tested
#ifdef HAVE_MPI
MPI_Finalize();
#endif
exit(EXIT_SUCCESS);


}






///*
// * ParFE: a micro-FE solver for trabecular bone modeling
// * Copyright (C) 2006 ParFE developers, see
// * http://parfe.sourceforge.net/developers.php
// *
// * This program is free software; you can redistribute it and/or
// * modify it under the terms of the GNU General Public License
// * as published by the Free Software Foundation; either version 2
// * of the License, or (at your option) any later version.
// *
// * This program is distributed in the hope that it will be useful,
// * but WITHOUT ANY WARRANTY; without even the implied warranty of
// * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// * GNU General Public License for more details.
// *
// * You should have received a copy of the GNU General Public License
// * along with this program; if not, write to the Free Software
// * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// * 02110-1301, USA.
// */
//
///*
// * This code should be used to perform a single finite element analysis
// * of a trabecular bone, stored in either ASCII or HDF5 format. Three
// * preconditioners are available: "Jacobi" (for comparisons only, not
// * for production), "ml" (matrix-ready multilevel preconditioner)
// * and "matrixfree" (matrix-free multilevel preconditioner).
// *
// * \author Marzio Sala
// *
// * \date Last modified on 15-Nov-06
// */
//
//#include "parfe_ConfigDefs.h"
//#include <string>
//#include <fstream>
//#include <vector>
//#include <Epetra_Time.h>
//#ifdef HAVE_MPI
//#include <mpi.h>
//#include <Epetra_MpiComm.h>
//#else
//#include <Epetra_SerialComm.h>
//#endif
//#include <Epetra_Vector.h>
//#include <Epetra_IntVector.h>
//#include <time.h>
//#include "DistMesh.h"
//#include "MEDIT_MeshWriter.hpp"
//#include "PMVIS_MeshWriter.hpp"
//#include "MEDIT_DispWriter.hpp"
//#include "IBT_ProblemReader.hpp"
//#include "IBT_ProblemWriter.hpp"
//#include "MATLAB_MatrixWriter.hpp"
//#include "MATLAB_VectorWriter.hpp"
//#include "IBT_DispWriter.hpp"
//#include "IBT_SolutionWriter.hpp"
//#include "EbeElasticityProblem.h"
//#include "VbrElasticityProblem.h"
//#include "Jacobi.h"
//#include "Tools.h"
//#include "FEParameterList.hpp"
//
//#include <ml_MultiLevelPreconditioner.h>
//#include <ml_MatrixFreePreconditioner.h>
//#include <AztecOO.h>
/////new belos includes*****
//#include "BelosConfigDefs.hpp"
//#include "BelosLinearProblem.hpp"
//#include "BelosEpetraAdapter.hpp"
////#include "BelosPCPGSolMgr.hpp"
//#include "BelosMinresSolMgr.hpp"
//#include "BelosPseudoBlockGmresSolMgr.hpp"
//#include "BelosTFQMRSolMgr.hpp"
//#include <Teuchos_ParameterList.hpp>
//#include <Teuchos_RefCountPtr.hpp>
//#include <Teuchos_CommandLineProcessor.hpp>
//#include "Teuchos_RCP.hpp"
////#include "BelosBlockCGSolMgr.hpp"
//
//#include "ml_include.h" //--enable-epetra --enable-teuchos.
//#include "Epetra_Map.h"
//
//#include "PoroMatrix.h" /// erhan added
//#include "PoroMatrixPreconditioner.h" /// erhan added
//#include "PoroMatrixPreconditionerB.h" /// erhan added
//#include <Epetra_FEVbrMatrix.h> /// erhan added
//#include "PressureElasticityProblem.h" /// erhan added
//#include "PressureFluxProblem.h" /// erhan added
//#include "PressureProblem.h" /// erhan added
//#include "SchurProblem.h" /// erhan added
//#include "FluxProblem.h" /// erhan added
//
//
/////using namespace ML_Epetra;
/////using namespace Teuchos;
//
//using namespace std;
//
//
//// =========== //
//// main driver //
//// =========== //
//
//int main( int argc, char* argv[] ) {
//#ifdef HAVE_MPI
//    MPI_Init(&argc, &argv);
//// define an Epetra communicator
//Epetra_MpiComm comm(MPI_COMM_WORLD);
//#else
//Epetra_SerialComm comm;
//#endif
//
///// /*
//using Teuchos::ParameterList; // all of this may look fine but do not be fooled ...
//using Teuchos::RCP;           // it is not so clear what any of this does
//using Teuchos::rcp;
///// */
//
//// get the proc ID of this process
//int MyPID = comm.MyPID();
//
//// get the total number of processes
//int NumProc = comm.NumProc();
//
//Epetra_Time timer(comm);
//double start_time = timer.WallTime();
//double temp_time = start_time;
//
//
////Macros to print information
//#define INFO(mesg) \
//if (MyPID == 0) cout << "%*** INFO ***: " << mesg << endl;
//
//#define WARN(mesg) \
//if (MyPID == 0) cout << "%*** WARNING ***: " << mesg << endl;
//
//#define ERROR(mesg, exit_code)\
//if (MyPID == 0) cerr << "%*** ERROR ***: " << mesg << endl; \
//        exit(exit_code);
//
//Teuchos::CommandLineProcessor CLP;
//
//char help[] = "This is the latest version of the ParFE driver.\n"
//        "It can solve problems stored in ASCII or HDF5 format, perform\n"
//        "load balance, build the preconditioner for matrix-free or\n"
//        "matrix-ready problems, and print out the solution.\n";
//CLP.setDocString(help);
//
//string inputFileName = "../mesh/simple_test.mesh";
//string outputFileName = "./output";
//string precType = "ml";
//string repartition = "parmetis";
/////int maxIters = 1550;
////int maxIters = 2550;
/////int maxIters = 22550;
//int maxIters = 3000;
//
////int maxIters = 10;
//double tolerance = 1.e-5;
//bool verbose = true;
//bool printMEDIT = false;
//bool printPMVIS = false;
//int outputFrequency = 16;
//string doScale = "noscale";
//string doMatlab = "no";
//
//CLP.setOption("filename", &inputFileName, "Name of input file");
//CLP.setOption("output", &outputFileName, "Base name of output file");
//CLP.setOption("precond", &precType,
//        "Preconditioner to be used [Jacobi/ml/matrixfree]");
//CLP.setOption("repart", &repartition,
//        "Repartioner to be used [parmetis/rcb]");
//
//CLP.setOption("maxiters", &maxIters, "Maximum CG iterations");
//CLP.setOption("tolerance", &tolerance, "Tolerance for CG");
//CLP.setOption("verbose", "silent", &verbose,
//        "Set verbosity level");
//CLP.setOption("printmedit", "noprintmedit", &printMEDIT,
//        "Print solution in MEDIT format");
//CLP.setOption("printpmvis", "noprintpmvis", &printPMVIS,
//        "Print partition in PMVIS format");
//CLP.setOption("frequency", &outputFrequency,
//        "Prints out residual every specified iterations");
//CLP.setOption("scaling", &doScale, "Perform scaling [scale/noscale]");
//CLP.setOption("printmatlab", &doMatlab, "print matrices in matlab format [yes]");
//
//
//CLP.recogniseAllOptions(false);
//CLP.throwExceptions(false);
//
//if (CLP.parse(argc, argv) == Teuchos::CommandLineProcessor::PARSE_HELP_PRINTED) {
//#ifdef HAVE_MPI
//    MPI_Finalize();
//#endif
//exit(EXIT_SUCCESS);
//}
//
//FEParameterList fe_param(NumProc);
//fe_param.set("input", outputFileName);
//
//Epetra_MultiVector* BCs = 0;
//
//ProblemReader* pr = 0;
//if (inputFileName.find(".mesh.h5") != string::npos)
//    pr = new IBT_ProblemReader<HDF5_GReader>(inputFileName, comm);
//else if (inputFileName.find(".mesh") != string::npos)
//    pr = new IBT_ProblemReader<C_ASCII_GReader>(inputFileName, comm);
//else {
//    cerr << "File name (" << inputFileName << ") not in ASCII or HDF5 format" << endl;
//    exit(EXIT_FAILURE);
//}
//
//fe_param.Scan(pr);
//
//INFO("#nodes       = " << fe_param.get<int>("#nodes"));
//INFO("#dimensions  = " << fe_param.get<int>("#dimensions"));
//INFO("#elements    = " << fe_param.get<int>("#elements"));
//INFO("#faces       = " << fe_param.get<int>("#faces"));
//INFO("#free faces  = " << fe_param.get<int>("#free faces"));
//
//temp_time = timer.WallTime();
//
////If these parameters are still unset, provide some default values
//fe_param.get("iteration limit", maxIters);
//fe_param.get("tolerance", tolerance);
//
//DistMesh* mesh = new DistMesh(fe_param.get<int>("#nodes"), fe_param.get<int>("#dimensions"), fe_param.get<int>("#elements"), fe_param.get<int>("#nodes per element"), fe_param.get<IntMap>("material ids"), comm, fe_param.get<int>("#faces"), fe_param.get<int>("#free faces"));
//
//mesh->Scan(pr);
//BoundaryCondition* bcond = new BoundaryCondition(fe_param.get<int>("#dofs per node"), comm);
//bcond->Scan(pr);
//
//delete pr;
//
////--------------------------end of input--------------------------------
//
//INFO("Input Time: " << timer.WallTime() - temp_time);
//temp_time = timer.WallTime();
//
//
/////cout<<mesh->NodeMap()->NumMyElements()<<endl;
//
/////cout<<mesh->ElementMap()->NumMyElements()<<endl;
//
//if (repartition == "parmetis"){
//    mesh->Redistribute(true, verbose);
//    //mesh->RedistributePoro(true, verbose);
//}
//else if (repartition == "rcb")
//    mesh->RedistributeWIsorropia(true, verbose);
//else
//    mesh->Redistribute(false, verbose);
//
//if (repartition == "parmetis" || repartition == "rcb") {
//    INFO("Time used to partition mesh: " << timer.WallTime() - temp_time);
//} else {
//    WARN("No load balancing used");
//}
//
////redistribute the boundary condition data according to the node map
//bcond->Redistribute(mesh->NodeMap()->NumMyElements(), mesh->NodeMap()->MyGlobalElements());
//
//if (printMEDIT) {
//    MEDIT_MeshWriter meshw(outputFileName + ".medit.mesh", 3, comm);
//    mesh->Print(&meshw);
//}
//
//if (fe_param.get<bool>("print for PMVIS")) {
//    std::string con = outputFileName + ".con";
//    std::string xyz = outputFileName + ".xyz";
//    std::string part = outputFileName + ".part";
//    PMVIS_MeshWriter meshw(xyz, con, part, comm);
//    mesh->Print(&meshw);
//}
//
//int memused = meminfo();
//INFO("Mem used to hold mesh information: " << memused <<  "MB");
//
//INFO("Total Mesh setup time: " << timer.WallTime() - start_time);
//
////-------------------------beginning of assembly------------------------
//
//temp_time = timer.WallTime();
//
//ElasticityProblem* ep;
////VbrElasticityProblem* ep;
//
//
//if (precType == "Jacobi" || precType == "matrixfree") {
//    fe_param.set("element by element", true);
//    ep = new EbeElasticityProblem(*mesh, fe_param);
//}
//else
//    ep = new VbrElasticityProblem(*mesh, fe_param);
//
//ep->Impose(*bcond);
//
//BCs = new Epetra_MultiVector(*ep->GetRHS());
//
//INFO("Time used to assemble matrix: " << timer.WallTime() - temp_time);
//
//memused = meminfo() - memused;
//INFO("Mem used to hold stiffness matrix operator: " << memused << "MB");
//
//Epetra_Vector* Xu = dynamic_cast<Epetra_Vector*>(ep->GetLHS());
//Epetra_Vector* Bu = dynamic_cast<Epetra_Vector*>(ep->GetRHS());
//
//
/////=============================================================
/////  define new subblocks
/////=============================================================
//
//
///// flux problem - Aff Block
//FluxProblem* fp = new FluxProblem(*mesh, fe_param);
//
///// pressure elasticty problem - Apu block
//PressureElasticityProblem* pep = new PressureElasticityProblem(*mesh, *bcond, fe_param, Xu->Map());
//
///// pressure flux problem - Apf block
//PressureFluxProblem* pfp = new PressureFluxProblem(*mesh, fe_param); /// OK
//
///// pressure problem - Ap block - Just a vector but represents the diagonal matrix App.
//PressureProblem* pp = new PressureProblem(*mesh, fe_param); /// OK
//
///// schur problem - Spp block
//SchurProblem* sp = new SchurProblem(*mesh, fe_param);  /// OK
//
//
//Epetra_Vector* Af; /// diagonal of Aff block
//Epetra_Vector* Ap; /// pressure vector
////Epetra_FECrsMatrix* AFEff; /// flux matrix
//Epetra_CrsMatrix* Aff; /// flux matrix
//Epetra_CrsMatrix* Spp; /// schur matrix
//Epetra_CrsMatrix* Apu; /// pressure elasticity matrix
//Epetra_CrsMatrix* Apf; /// pressure flux matrix
//
//
//Aff = &fp->getMatrix();
//Apu = &pep->getMatrix();
//Apf = & pfp->getMatrix();
//Spp = & sp->getMatrix();
//Ap = &pp->getVector();
//
//Af = new Epetra_Vector(fp->getDiagonal()); /// - should be automatically correct when Aff is.
//
//
//Epetra_Vector* Xf = dynamic_cast<Epetra_Vector*>(fp->GetLHS());
//Epetra_Vector* Bf = dynamic_cast<Epetra_Vector*>(fp->GetRHS());
//Epetra_Vector* Xp = dynamic_cast<Epetra_Vector*>(pp->GetLHS());
//Epetra_Vector* Bp = dynamic_cast<Epetra_Vector*>(pp->GetRHS());
//
//
//Epetra_RowMatrix *AuuP=ep->GetMatrix();
//
//
//VectorWriter* myMatlabVec = 0;
//MatrixWriter* mw3 = 0;
//bool mTest;
//
///*
//if(doMatlab=="yes") {
//    myMatlabVec = new MATLAB_VectorWriter<C_ASCII_GWriter>("Ap", comm);
//    myMatlabVec->PrintVector(*Ap);
//
//    myMatlabVec = new MATLAB_VectorWriter<C_ASCII_GWriter>("Bu", comm);
//    myMatlabVec->PrintVector(*Bu);
//
//    //mw3 = new MATLAB_MatrixWriter<C_ASCII_GWriter>("Auu", comm);
//    //mw3->PrintRows(dynamic_cast<Epetra_VbrMatrix*>(Auu));
//    mw3 = new MATLAB_MatrixWriter<C_ASCII_GWriter>("AuuP", comm);
//    mw3->PrintRows(dynamic_cast<Epetra_VbrMatrix*>(AuuP));
//
//    mTest = CrsMatrix2MATLAB(*Apu);
//    if(comm.MyPID()==0)  {
//        cout<<"Apu=A;"<<endl;
//        cout<<"clear A;"<<endl;
//    }
//    mTest = CrsMatrix2MATLAB(*Apf);
//    if(comm.MyPID()==0)  {
//        cout<<"Apf=A;"<<endl;
//        cout<<"clear A;"<<endl;
//    }
//    mTest = CrsMatrix2MATLAB(*Aff);
//    if(comm.MyPID()==0)  {
//        cout<<"Aff=A;"<<endl;
//        cout<<"clear A;"<<endl;
//    }
//    mTest = CrsMatrix2MATLAB(*Spp);
//    if(comm.MyPID()==0)  {
//        cout<<"Spp=A;"<<endl;
//        cout<<"clear A;"<<endl;
//    }
//    return 0;
//}
//*/
//
///// define the interface for matrix-vector multiplication
//PoroMatrix *my_poro = new PoroMatrix(*AuuP, *Aff, *Ap, *Apu, *Apf, *bcond, Xu->Map(), comm);
//
///// perform scaling on the blocks.
//if(doScale=="scale") {
//
//    my_poro->Scale(); ///scale AuuP, Aff, Ap, Apu, Apf.
//    my_poro->ScaleF(*Af); /// apply right scale
//    my_poro->ScaleF(*Af); /// apply left scale - same as right scale since Af is a vector
//    my_poro->ScaleP(*Spp); /// scale Spp only
//
//}
//
//
///// creation of pA to define the matrix in Belos::LinearProblem
//RCP<Epetra_Operator> pA = rcp(my_poro);
//
//
//
//
/////scale RHS. Only left scale is required so only one scaling is performed per vector.
//if(doScale=="scale") {
//    my_poro->ScaleU(*Bu);
//    my_poro->ScaleF(*Bf);
//    my_poro->ScaleP(*Bp);
//}
//
//
///// creation of pB to define the RHS in Belos::LinearProblem
//Epetra_Vector* pBvec = new Epetra_Vector(my_poro->compose_vector(*Bu, *Bf, *Bp ));
//RCP<Epetra_Vector> pB  = rcp(pBvec);
//
//
///// add a new routine to define initial conditions - thay should also change Bp and it should be called before scale and pBvec.
//
///// try to generate an artificial b.
/////Xu->PutScalar(1.0);
/////Xf->PutScalar(1.0);
/////Xp->PutScalar(1.0);
//
//
///*
//Xu->Seed();
//Xf->Seed();
//Xp->Seed();
//*/
//
///// creation of pX to define the unknown in Belos::LinearProblem
//Epetra_Vector* pXvec = new Epetra_Vector(my_poro->compose_vector(*Xu, *Xf, *Xp ));
//RCP<Epetra_Vector> pX  = rcp(pXvec);
//
//
//
//
//
/////cout<<pBvec->Values()[0]<<endl;
//
////modify b and assign.
/////pA->Apply(*pXvec,*pBvec);
//
/////cout<<pBvec->Values()[0]<<endl;
//
/////pXvec->PutScalar(0.0);
////return 0;
//
///*
//Xu->PutScalar(0.0);
//Xf->PutScalar(0.0);
//Xp->PutScalar(0.0);
//*/
//
//
///// #############################
//
//
//if(doMatlab=="yes") {
//    myMatlabVec = new MATLAB_VectorWriter<C_ASCII_GWriter>("Ap", comm);
//    myMatlabVec->PrintVector(*Ap);
//
//    myMatlabVec = new MATLAB_VectorWriter<C_ASCII_GWriter>("Bu", comm);
//    myMatlabVec->PrintVector(*Bu);
//
//    //mw3 = new MATLAB_MatrixWriter<C_ASCII_GWriter>("Auu", comm);
//    //mw3->PrintRows(dynamic_cast<Epetra_VbrMatrix*>(Auu));
//    mw3 = new MATLAB_MatrixWriter<C_ASCII_GWriter>("AuuP", comm);
//    mw3->PrintRows(dynamic_cast<Epetra_VbrMatrix*>(AuuP));
//
//    mTest = CrsMatrix2MATLAB(*Apu);
//    if(comm.MyPID()==0)  {
//        cout<<"Apu=A;"<<endl;
//        cout<<"clear A;"<<endl;
//    }
//    mTest = CrsMatrix2MATLAB(*Apf);
//    if(comm.MyPID()==0)  {
//        cout<<"Apf=A;"<<endl;
//        cout<<"clear A;"<<endl;
//    }
//    mTest = CrsMatrix2MATLAB(*Aff);
//    if(comm.MyPID()==0)  {
//        cout<<"Aff=A;"<<endl;
//        cout<<"clear A;"<<endl;
//    }
//    mTest = CrsMatrix2MATLAB(*Spp);
//    if(comm.MyPID()==0)  {
//        cout<<"Spp=A;"<<endl;
//        cout<<"clear A;"<<endl;
//    }
//    return 0;
//}
//
//
//
///// #############################
//
//
//
//if (Xu == 0 || Bu == 0) {
//    ERROR("Use Epetra_Vector in ElastictyProblem", 1);
//}
//
//vector<double> null_space;
//
////Compute null space for ml preconditioner
//if (precType == "ml" || precType == "matrixfree")
//    set_null_space(mesh, null_space);
//
//Teuchos::RefCountPtr<Epetra_CrsGraph> graph; ///Teuchos:: is added
//
//if (precType == "matrixfree") {
//    ElementByElementMatrix* ebe = dynamic_cast<ElementByElementMatrix*>(ep->GetOperator());
//    if (ebe != 0) {
//        graph = Teuchos::rcp(new Epetra_CrsGraph(Copy, ebe->Map(), 0)); ///Teuchos:: is added
//        mesh->MatrixGraph(*(graph.get()));
//    }
//}
//
////------------------------beginning of solution------------------------
//
//
/////Teuchos::RefCountPtr<Epetra_Vector> diagonal;
//RCP<Belos::EpetraPrecOp> belosPrec;
//RCP<Epetra_Operator> Prec;
//
//
//if (precType == "Jacobi") {
//
//    ElementByElementMatrix* ebe = dynamic_cast<ElementByElementMatrix*>(ep->GetOperator());
//    std::cout<<"Jacobi solver is not yet implemented!"<<std::endl;
//    std::cout<<"parfe exit"<<std::endl;
//#ifdef HAVE_MPI
//MPI_Finalize();
//#endif
//exit(EXIT_SUCCESS);
//
//}
//else if (precType == "matrixfree") {
//    ElementByElementMatrix* ebe = dynamic_cast<ElementByElementMatrix*>(ep->GetOperator());
//    std::cout<<"matrixfree solver is not yet implemented!"<<std::endl;
//    std::cout<<"parfe exit"<<std::endl;
//#ifdef HAVE_MPI
//MPI_Finalize();
//#endif
//exit(EXIT_SUCCESS);
//
//}
//else if (precType == "ml") {
//
//
//    Teuchos::ParameterList MLList;
//
//    ML_Epetra::SetDefaults("SA", MLList);
//    ///MLList.set("smoother: type (level 0)", "Chebyshev");
//    ///MLList.set("smoother: type (level 1)", "symmetric Gauss-Seidel");
//    MLList.set("smoother: type", "symmetric Gauss-Seidel");
//    ///MLList.set("coarse: max size", 1024);
//    ///MLList.set("null space: dimension", 6);
//    ///MLList.set("null space: type", "pre-computed");
//    ///MLList.set("null space: vectors", &null_space[0]);
//    ///MLList.set("aggregation: type", "Uncoupled");
//    ////MLList.set("aggregation: type (level 0)", "ParMETIS");
//    ////MLList.set("aggregation: type (level 1)", "Uncoupled");
//    ////MLList.set("aggregation: type (level 2)", "MIS");
//    ///MLList.set("low memory usage", true);
//    //MLList.set("max levels", 6);
//    if (verbose) MLList.set("output", 10);
//    else         MLList.set("output", 0);
//
//
//    //PoroMatrixPreconditioner *my_poro_p = new PoroMatrixPreconditioner(*AuuP, *Af, *Spp, comm, MLList);
//
//    //Prec = rcp(my_poro_p);
//
//    //assert(Prec != Teuchos::null);
//
//    //belosPrec = rcp( new Belos::EpetraPrecOp(Prec)); ///define a belos preconditioner through Prec.
//
//
//    Prec = rcp(  new ML_Epetra::MultiLevelPreconditioner(*Spp, MLList, true) );
//
//    assert(Prec != Teuchos::null);
//
//    belosPrec = rcp( new Belos::EpetraPrecOp(Prec) );
//
//}
//
//else {
//
//    WARN("No preconditioner used");
//}
//
//
//INFO("Time used to build preconditioner: " << timer.WallTime() - temp_time);
//memused = meminfo() - memused;
//INFO("Mem used to hold preconditioner: " << memused << "MB");
//
///// belos parameters:
//Teuchos::ParameterList belosList;
//belosList.set( "Block Size", 1 );  /// current implementation of minres allows only 1.
//belosList.set( "Maximum Iterations", maxIters );   /// Maximum number of iterations allowed
//belosList.set( "Convergence Tolerance", tolerance); /// Relative convergence tolerance requested
//
//belosList.set( "Output Frequency", 1);
//belosList.set( "Output Style", (int) Belos::Brief);
//belosList.set( "Verbosity", Belos::Errors+Belos::StatusTestDetails+Belos::Warnings+Belos::TimingDetails+Belos::IterationDetails+Belos::FinalSummary );
////belosList.set( "Explicit Residual Test", true );
//
///// belos related declarations
//
//typedef Epetra_MultiVector MV;
//typedef Epetra_Operator OP;
//
//typedef double ST;
//
//typedef Belos::OperatorTraits<ST, MV, OP> OPT;
//typedef Belos::MultiVecTraits<ST, MV> MVT;
//
///*
//typedef Belos::StatusTestGenResNorm<ST, MV, OP> STN;
//
//defineResForm("Explicit");
/////enum Belos::StatusTestGenResNorm::ResType
/////int Belos::StatusTestGenResNorm< ScalarType, MV, OP >::defineResForm
//*/
//
///// define linear problem:
//
////Belos::LinearProblem<double, MV, OP>* my_problem = new Belos::LinearProblem<double, MV, OP>(pA, pX, pB);
//
////RCP<Belos::LinearProblem<double, MV, OP> > problem  = rcp(my_problem);
//
//
///*
//set x
//modify p by apply.
//expect same x later.
//*/
////cout<<*Xp<<endl;
//
/////*
//Xp->Seed();
//Xp->Random();
////Xp->PutScalar(1.0);
////
//cout<<"initial x[0] = "<< Xp->Values()[0]<<endl;
//int myNewtest;
////cout<<*Xp<<endl;
////cout<<*Bp<<endl;
//myNewtest=Spp->Multiply(false, *Xp  , *Bp );
////cout<<*Bp<<endl;
////cout<<myNewtest<<endl;
////*/
//
////Bp->PutScalar(1.0);
//Bp->Random();
//
////mTest = CrsMatrix2MATLAB(*Spp);
////random b ile de olur ama sonuc acisindan onemli bu kisim
//
/////manage the vectors as RCP objects.
//RCP<Epetra_Vector> pXp=rcp(Xp);
//RCP<Epetra_Vector> pBp=rcp(Bp);
//RCP<Epetra_Operator> pSpp=rcp(Spp);
//
//
//RCP<Belos::LinearProblem<double, MV, OP> > problem = rcp( new Belos::LinearProblem<double, MV, OP>(pSpp, pXp, pBp));
//
//
///// set the preconditioner for the problem.
//if (precType == "ml") problem->setLeftPrec(belosPrec);
////if (precType == "ml") problem->setRightPrec(belosPrec);
//
//bool set = problem->setProblem();
//
//if (set == false) {
//    if (MyPID==0)
//        std::cout << std::endl << "ERROR:  problem set up is not correct" << std::endl;
//    return -1;
//}
//
///// solver manager for Minres
////RCP< Belos::SolverManager<double, MV, OP> > solver = rcp( new Belos::PseudoBlockGmresSolMgr<double, MV, OP>(problem, rcp(&belosList, false)) );
////RCP< Belos::SolverManager<double, MV, OP> > solver = rcp( new Belos::TFQMRSolMgr<double, MV, OP>(problem, rcp(&belosList, false)) );
//RCP< Belos::SolverManager<double, MV, OP> > solver = rcp( new Belos::MinresSolMgr<double, MV, OP>(problem, rcp(&belosList, false)) );
//
//double initialNorm;
/////calculate the absolute norm ||b-Ax||
//
////pXvec->PutScalar(1.0);
////pXvec->PutScalar(0.1);
////pXvec->Seed();
//
//const Epetra_Vector* myResVecZero = problem->getInitResVec()->operator()(0);
//myResVecZero->Norm2(&initialNorm);
//cout<<"initial norm = "<<initialNorm<<endl;
//
////pA->Apply(*pXvec,*pXvec);
////pXvec->Update(1.0, *pBvec,-1.0);
////pXvec->Norm2(&initialNorm);
//
////cout<<"sdasdsasasa "<<Xp->Values()[0]<<endl;
//cout<<"sdasdsasasa "<<pXvec->Values()[0]<<endl;
//
//
//if (comm.MyPID()==0) cout<<"|| absolute initial norm is = "<<initialNorm<<" ||"<<endl;
//
///// solve the problem.
//Belos::ReturnType ret = solver->solve();
//
/////*************************************************************
//
/////  currently inactive
//if (precType == "matrixfree" || precType == "Jacobi") {
//    ElementByElementMatrix* ebe = dynamic_cast<ElementByElementMatrix*>(ep->GetOperator());
//    ebe->ResetRestrainedBoundaries(Xu);
//}
//
/////diagonal = Teuchos::null;
//
//
////------------------------------Print solution-------------------------
//
///// get the solution from the problem
//Epetra_MultiVector* mySolution = problem->getLHS().get();
//
////const Epetra_Vector* mySolvecZero = problem->getLHS()->operator()(0); /// if constant, cannot overwritten.
//Epetra_Vector* mySolvecZero = problem->getLHS()->operator()(0);
//
//// now mySolvecZero is the solution
//
//cout<<"result >>>>>>"<<mySolvecZero->Values()[0]<<endl;
//
////cout<<mySolvecZero[0]<<endl;
//
//
//
//
//return 0;
//
//
//
//Epetra_Vector* testVec = new Epetra_Vector(*pXvec);
//
//
//my_poro->extract_partial_vectors(*Xu, *Xf, *Xp, *mySolvecZero);
//
//double aaa;
/////calculate the absolute norm ||b-Ax||
//double finalNorm;
//pA->Apply(*testVec,*testVec);
//testVec->Update(1.0, *pBvec,-1.0);
//testVec->Norm2(&finalNorm);
//if (comm.MyPID()==0) cout<<"|| absolute final norm is = "<<finalNorm<<" ||"<<endl;
//if (comm.MyPID()==0) cout<<"|| relative convergence is = "<<finalNorm/initialNorm<<" ||"<<endl;
//
//
////cout<<"before: "<<Xu->Values()[0]<<endl;
////cout<<"before: "<<Xf->Values()[1]<<endl;
//cout<<"before: "<<Xp->Values()[0]<<endl;
///// scale the solution back to the original order.
//if(doScale=="scale") {
//    my_poro->ScaleU(*Xu);
//    my_poro->ScaleF(*Xf);
//    my_poro->ScaleP(*Xp);
//}
////cout<<"after : "<<Xu->Values()[0]<<endl;
////cout<<"after : "<<Xf->Values()[1]<<endl;
////cout<<"after : "<<Xp->Values()[0]<<endl;
///// a test computation. dot product of the pressure vector.
//
//Xp->Dot(*Xp, &aaa);
//if (comm.MyPID()==0) cout<<"xp_dot_xp= "<<aaa<<endl;
//
//
//
///// display the first pressure value on the solution with scaling
//if (comm.MyPID()==0){
//
//cout<<endl;
//cout<<"+----------------------------------+"<<endl;
//cout<<">>> test displacement = "<<Xu->Values()[0]<<" <<<"<<endl;
//cout<<">>> test flux = "<<Xf->Values()[1]<<" <<<"<<endl;
//cout<<">>> test pressure = "<<Xp->Values()[0]<<" <<<"<<endl;
//cout<<"+----------------------------------+"<<endl;
//cout<<endl;
//
//}
//
///// prepare the LHSs for output.
//ep->SetLHS(Xu);
//fp->SetLHS(Xf);
//pp->SetLHS(Xp);
//
//
//temp_time = timer.WallTime();
//
//if (inputFileName.find(".mesh.h5") == string::npos) {
//    int ext_pos =  inputFileName.rfind(".mesh");
//    inputFileName = inputFileName.substr(0, ext_pos)+".mesh.h5";
//    IBT_ProblemWriter<HDF5_GWriter> pw(inputFileName , comm);
//    fe_param.Print(&pw);
//    mesh->Print(&pw);
//    bcond->Print(&pw);
//}
//
//
//if (inputFileName.find(".mesh.h5") != string::npos) {
//    ///we are currently using this one. because the mesh info is already there.
//    IBT_SolutionWriter<HDF5_GWriter> sw(inputFileName, comm);
//    /// print the displacement solution
//    ep->PrintSolution(&sw, fe_param.get<Epetra_SerialDenseMatrix>("material properties"));
//    /// print the flux solution
//    fp->PrintSolution(&sw);
//    /// print the pressure solution
//    pp->PrintSolution(&sw);
//    // might need to modify the stress values with pressure to get absolute values.
//
//
//}
//
//if (printMEDIT) {
//    MEDIT_DispWriter dispw(outputFileName + ".medit.bb", comm, 3);
//    ep->PrintLHS(&dispw);
//}
//
//INFO("output time: " << timer.WallTime() -temp_time);
//
//comm.Barrier();
//INFO("Total time used: " << timer.WallTime() -start_time);
//
//// ======================= //
//// Finalize MPI and exit   //
//// ----------------------- //
//
//
///// erhan added
///// reset smart pointers: assing to null pointer
///// Teuchos::RCP Beginner's Guide - September 2010. Appendix B 2b.
/////manage the vectors as RCP objects.
///*
//pX = null;
//pB = null;
//Prec = null;
//belosPrec = null;
//problem=null;
//solver=null;
//pA=null;
//*/
//
//
//return 0; /// erhan added
/////should be active in Minres else we get the same RCP error. Cyril's main-hack idea might work but not tested
//#ifdef HAVE_MPI
//MPI_Finalize();
//#endif
//exit(EXIT_SUCCESS);
//
//
//}















///*
// * ParFE: a micro-FE solver for trabecular bone modeling
// * Copyright (C) 2006 ParFE developers, see
// * http://parfe.sourceforge.net/developers.php
// *
// * This program is free software; you can redistribute it and/or
// * modify it under the terms of the GNU General Public License
// * as published by the Free Software Foundation; either version 2
// * of the License, or (at your option) any later version.
// *
// * This program is distributed in the hope that it will be useful,
// * but WITHOUT ANY WARRANTY; without even the implied warranty of
// * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// * GNU General Public License for more details.
// *
// * You should have received a copy of the GNU General Public License
// * along with this program; if not, write to the Free Software
// * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// * 02110-1301, USA.
// */
//
///*
// * This code should be used to perform a single finite element analysis
// * of a trabecular bone, stored in either ASCII or HDF5 format. Three
// * preconditioners are available: "Jacobi" (for comparisons only, not
// * for production), "ml" (matrix-ready multilevel preconditioner)
// * and "matrixfree" (matrix-free multilevel preconditioner).
// *
// * \author Marzio Sala
// *
// * \date Last modified on 15-Nov-06
// */
//
//#include "parfe_ConfigDefs.h"
//#include <string>
//#include <fstream>
//#include <vector>
//#include <Epetra_Time.h>
//#ifdef HAVE_MPI
//#include <mpi.h>
//#include <Epetra_MpiComm.h>
//#else
//#include <Epetra_SerialComm.h>
//#endif
//#include <Epetra_Vector.h>
//#include <Epetra_IntVector.h>
//#include <time.h>
//#include "DistMesh.h"
//#include "MEDIT_MeshWriter.hpp"
//#include "PMVIS_MeshWriter.hpp"
//#include "MEDIT_DispWriter.hpp"
//#include "IBT_ProblemReader.hpp"
//#include "IBT_ProblemWriter.hpp"
//#include "MATLAB_MatrixWriter.hpp"
//#include "MATLAB_VectorWriter.hpp"
//#include "IBT_DispWriter.hpp"
//#include "IBT_SolutionWriter.hpp"
//#include "EbeElasticityProblem.h"
//#include "VbrElasticityProblem.h"
//#include "Jacobi.h"
//#include "Tools.h"
//#include "FEParameterList.hpp"
//
//#include <ml_MultiLevelPreconditioner.h>
//#include <ml_MatrixFreePreconditioner.h>
//#include <AztecOO.h>
/////new belos includes*****
//#include "BelosConfigDefs.hpp"
//#include "BelosLinearProblem.hpp"
//#include "BelosEpetraAdapter.hpp"
//#include "BelosPCPGSolMgr.hpp"
//#include "BelosMinresSolMgr.hpp"
//#include <Teuchos_ParameterList.hpp>
//#include <Teuchos_RefCountPtr.hpp>
//#include <Teuchos_CommandLineProcessor.hpp>
//#include "Teuchos_RCP.hpp"
//////#include "BelosBlockCGSolMgr.hpp"
//
//#include "ml_include.h" //--enable-epetra --enable-teuchos.
//#include "Epetra_Map.h"
//
//#include "PoroMatrixB.h" /// erhan added
//#include "PoroMatrixPreconditionerB.h" /// erhan added
//
//
//
/////#include "Epetra_CrsMatrix.h"
/////#include "EpetraExt_MatrixMatrix.h"  // build the example
//
//
/////using namespace ML_Epetra;
/////using namespace Teuchos;
//
//
//// =========== //
//// main driver //
//// =========== //
//
//int main( int argc, char* argv[] ) {
//#ifdef HAVE_MPI
//    MPI_Init(&argc, &argv);
//// define an Epetra communicator
//Epetra_MpiComm comm(MPI_COMM_WORLD);
//#else
//Epetra_SerialComm comm;
//#endif
//
///// /*
//using Teuchos::ParameterList; // all of this may look fine but do not be fooled ...
//using Teuchos::RCP;           // it is not so clear what any of this does
//using Teuchos::rcp;
///// */
//
//// get the proc ID of this process
//int MyPID = comm.MyPID();
//
//// get the total number of processes
//int NumProc = comm.NumProc();
//
//Epetra_Time timer(comm);
//double start_time = timer.WallTime();
//double temp_time = start_time;
//
//
////Macros to print information
//#define INFO(mesg) \
//if (MyPID == 0) cout << "*** INFO ***: " << mesg << endl;
//
//#define WARN(mesg) \
//if (MyPID == 0) cout << "*** WARNING ***: " << mesg << endl;
//
//#define ERROR(mesg, exit_code)\
//if (MyPID == 0) cerr << "*** ERROR ***: " << mesg << endl; \
//        exit(exit_code);
//
//Teuchos::CommandLineProcessor CLP;
//
//char help[] = "This is the latest version of the ParFE driver.\n"
//        "It can solve problems stored in ASCII or HDF5 format, perform\n"
//        "load balance, build the preconditioner for matrix-free or\n"
//        "matrix-ready problems, and print out the solution.\n";
//CLP.setDocString(help);
//
//string inputFileName = "../mesh/simple_test.mesh";
//string outputFileName = "./output";
//string precType = "ml";
//string repartition = "parmetis";
//int maxIters = 1550;
//double tolerance = 1.e-5;
//bool verbose = true;
//bool printMEDIT = false;
//bool printPMVIS = false;
//int outputFrequency = 16;
//
//CLP.setOption("filename", &inputFileName, "Name of input file");
//CLP.setOption("output", &outputFileName, "Base name of output file");
//CLP.setOption("precond", &precType,
//        "Preconditioner to be used [Jacobi/ml/matrixfree]");
//CLP.setOption("repart", &repartition,
//        "Repartioner to be used [parmetis/rcb]");
//
//CLP.setOption("maxiters", &maxIters, "Maximum CG iterations");
//CLP.setOption("tolerance", &tolerance, "Tolerance for CG");
//CLP.setOption("verbose", "silent", &verbose,
//        "Set verbosity level");
//CLP.setOption("printmedit", "noprintmedit", &printMEDIT,
//        "Print solution in MEDIT format");
//CLP.setOption("printpmvis", "noprintpmvis", &printPMVIS,
//        "Print partition in PMVIS format");
//CLP.setOption("frequency", &outputFrequency,
//        "Prints out residual every specified iterations");
//
//CLP.recogniseAllOptions(false);
//CLP.throwExceptions(false);
//
//if (CLP.parse(argc, argv) == Teuchos::CommandLineProcessor::PARSE_HELP_PRINTED) {
//#ifdef HAVE_MPI
//    MPI_Finalize();
//#endif
//exit(EXIT_SUCCESS);
//}
//
//FEParameterList fe_param(NumProc);
//fe_param.set("input", outputFileName);
//
//Epetra_MultiVector* BCs = 0;
//
//ProblemReader* pr = 0;
//if (inputFileName.find(".mesh.h5") != string::npos)
//    pr = new IBT_ProblemReader<HDF5_GReader>(inputFileName, comm);
//else if (inputFileName.find(".mesh") != string::npos)
//    pr = new IBT_ProblemReader<C_ASCII_GReader>(inputFileName, comm);
//else {
//    cerr << "File name (" << inputFileName << ") not in ASCII or HDF5 format" << endl;
//    exit(EXIT_FAILURE);
//}
//
//fe_param.Scan(pr);
//
//INFO("#nodes       = " << fe_param.get<int>("#nodes"));
//INFO("#dimensions  = " << fe_param.get<int>("#dimensions"));
//INFO("#elements    = " << fe_param.get<int>("#elements"));
//INFO("#faces       = " << fe_param.get<int>("#faces"));
//INFO("#free faces  = " << fe_param.get<int>("#free faces"));
//
//temp_time = timer.WallTime();
//
////If these parameters are still unset, provide some default values
//fe_param.get("iteration limit", maxIters);
//fe_param.get("tolerance", tolerance);
//
//DistMesh* mesh = new DistMesh(fe_param.get<int>("#nodes"), fe_param.get<int>("#dimensions"), fe_param.get<int>("#elements"), fe_param.get<int>("#nodes per element"), fe_param.get<IntMap>("material ids"), comm, fe_param.get<int>("#faces"), fe_param.get<int>("#free faces"));
//
//mesh->Scan(pr);
//BoundaryCondition* bcond = new BoundaryCondition(fe_param.get<int>("#dofs per node"), comm);
//bcond->Scan(pr);
//
//delete pr;
//
////--------------------------end of input--------------------------------
//
//INFO("Input Time: " << timer.WallTime() - temp_time);
//temp_time = timer.WallTime();
//
//
//if (repartition == "parmetis"){
//    mesh->Redistribute(true, verbose);
//}
//else if (repartition == "rcb")
//    mesh->RedistributeWIsorropia(true, verbose);
//else
//    mesh->Redistribute(false, verbose);
//
//if (repartition == "parmetis" || repartition == "rcb") {
//    INFO("Time used to partition mesh: " << timer.WallTime() - temp_time);
//} else {
//    WARN("No load balancing used");
//}
//
////redistribute the boundary condition data according to the node map
//bcond->Redistribute(mesh->NodeMap()->NumMyElements(), mesh->NodeMap()->MyGlobalElements());
//
//if (printMEDIT) {
//    MEDIT_MeshWriter meshw(outputFileName + ".medit.mesh", 3, comm);
//    mesh->Print(&meshw);
//}
//
//if (fe_param.get<bool>("print for PMVIS")) {
//    std::string con = outputFileName + ".con";
//    std::string xyz = outputFileName + ".xyz";
//    std::string part = outputFileName + ".part";
//    PMVIS_MeshWriter meshw(xyz, con, part, comm);
//    mesh->Print(&meshw);
//}
//
//int memused = meminfo();
//INFO("Mem used to hold mesh information: " << memused <<  "MB");
//
//INFO("Total Mesh setup time: " << timer.WallTime() - start_time);
//
////-------------------------beginning of assembly------------------------
//
//temp_time = timer.WallTime();
//
//ElasticityProblem* ep;
//if (precType == "Jacobi" || precType == "matrixfree") {
//    fe_param.set("element by element", true);
//    ep = new EbeElasticityProblem(*mesh, fe_param);
//}
//else
//    ep = new VbrElasticityProblem(*mesh, fe_param);
//
//ep->Impose(*bcond);
//
//BCs = new Epetra_MultiVector(*ep->GetRHS());
//
//INFO("Time used to assemble matrix: " << timer.WallTime() - temp_time);
//
//memused = meminfo() - memused;
//INFO("Mem used to hold stiffness matrix operator: " << memused << "MB");
//
//Epetra_Vector* X = dynamic_cast<Epetra_Vector*>(ep->GetLHS());
//Epetra_Vector* B = dynamic_cast<Epetra_Vector*>(ep->GetRHS());
//
//
/////manage the vectors as RCP objects.
//RCP<Epetra_Operator> Auu=rcp(ep->GetMatrix());
//
//
//
//if (X == 0 || B == 0) {
//    ERROR("Use Epetra_Vector in ElastictyProblem", 1);
//}
//
//vector<double> null_space;
//
////Compute null space for ml preconditioner
//if (precType == "ml" || precType == "matrixfree")
//    set_null_space(mesh, null_space);
//
//Teuchos::RefCountPtr<Epetra_CrsGraph> graph; ///Teuchos:: is added
//
//if (precType == "matrixfree") {
//    ElementByElementMatrix* ebe = dynamic_cast<ElementByElementMatrix*>(ep->GetOperator());
//    if (ebe != 0) {
//        graph = Teuchos::rcp(new Epetra_CrsGraph(Copy, ebe->Map(), 0)); ///Teuchos:: is added
//        mesh->MatrixGraph(*(graph.get()));
//    }
//}
//
////------------------------beginning of solution------------------------
//
//
/////Teuchos::RefCountPtr<Epetra_Vector> diagonal;
//RCP<Belos::EpetraPrecOp> belosPrec;
//RCP<Epetra_Operator> Prec;
//
//if (precType == "Jacobi") {
//
//    ElementByElementMatrix* ebe = dynamic_cast<ElementByElementMatrix*>(ep->GetOperator());
//    std::cout<<"Jacobi solver is not yet implemented!"<<std::endl;
//    std::cout<<"parfe exit"<<std::endl;
//#ifdef HAVE_MPI
//MPI_Finalize();
//#endif
//exit(EXIT_SUCCESS);
//
//}
//else if (precType == "matrixfree") {
//    ElementByElementMatrix* ebe = dynamic_cast<ElementByElementMatrix*>(ep->GetOperator());
//    std::cout<<"matrixfree solver is not yet implemented!"<<std::endl;
//    std::cout<<"parfe exit"<<std::endl;
//#ifdef HAVE_MPI
//MPI_Finalize();
//#endif
//exit(EXIT_SUCCESS);
//
//}
//else if (precType == "ml") {
//
//    // allocate the ML preconditioner
//    Teuchos::ParameterList MLList;
//
//
//    ML_Epetra::SetDefaults("SA", MLList);
//    MLList.set("smoother: type (level 0)", "Chebyshev");
//    MLList.set("smoother: type (level 1)", "symmetric Gauss-Seidel");
//    MLList.set("coarse: max size", 1024);
//    MLList.set("null space: dimension", 6);
//    MLList.set("null space: type", "pre-computed");
//    MLList.set("null space: vectors", &null_space[0]);
//    MLList.set("aggregation: type", "Uncoupled");
//    //MLList.set("aggregation: type (level 0)", "ParMETIS");
//    //MLList.set("aggregation: type (level 1)", "Uncoupled");
//    //MLList.set("aggregation: type (level 2)", "MIS");
//    MLList.set("low memory usage", true);
//    //MLList.set("max levels", 6);
//    if (verbose) MLList.set("output", 10);
//    else         MLList.set("output", 0);
//
//
//    Epetra_RowMatrix *AuuP=ep->GetMatrix();
//
//
//    PoroMatrixPreconditionerB *my_poro_p = new PoroMatrixPreconditionerB(*AuuP, comm, MLList);
//
//    Prec = rcp(my_poro_p);
//
//
//    assert(Prec != Teuchos::null);
//
//
//
//    belosPrec = rcp( new Belos::EpetraPrecOp(Prec) );
//
//}
//
//else {
//    ///solver.SetAztecOption(AZ_precond, AZ_none); //TODO
//    WARN("No preconditioner used");
//}
//
//
//INFO("Time used to build preconditioner: " << timer.WallTime() - temp_time);
//memused = meminfo() - memused;
//INFO("Mem used to hold preconditioner: " << memused << "MB");
//
//// solution is constant
//X->PutScalar(0.0);
//
//
//
///// belos parameters:
//Teuchos::ParameterList belosList;
//belosList.set( "Block Size", 1 );              // Blocksize to be used by iterative solver
//belosList.set( "Maximum Iterations", maxIters );   // Maximum number of iterations allowed
//belosList.set( "Convergence Tolerance", tolerance);// Relative convergence tolerance requested
//belosList.set( "Verbosity", Belos::Errors+Belos::Warnings+Belos::TimingDetails+Belos::FinalSummary );
/////belosList.set( "Output Frequency", 10); not wotrking?
//
//
///// declarations
//
//typedef Epetra_MultiVector                MV;
//typedef Epetra_Operator                   OP;
//
//typedef double                          ST;
//
//typedef Belos::OperatorTraits<ST, MV, OP> OPT;// TODO
//typedef Belos::MultiVecTraits<ST, MV>    MVT;// TODO
//
//
//Epetra_Operator *Auus=ep->GetOperator(); /// this one is usefult if we want to use nice looking variable names.
//
//PoroMatrixB *my_poroB = new PoroMatrixB(*Auus, *bcond, comm);
//
//
//
///// creation of my_poro
//RCP<Epetra_Operator> pA = rcp(my_poroB);
//
//RCP<Epetra_Vector> pX=rcp(X);
//RCP<Epetra_Vector> pB=rcp(B);
//
//
//
///// linear preconditioned problem:
//
//RCP<Belos::LinearProblem<double, MV, OP> > problem = rcp( new Belos::LinearProblem<double, MV, OP>(pA, pX, pB));
//
//
//
//problem->setLeftPrec(belosPrec);
//
//
//bool set = problem->setProblem();
//if (set == false) {
//    if (MyPID==0)
//        std::cout << std::endl << "ERROR:  problem set up is not correct" << std::endl;
//    return -1;
//}
//
///// solver manager - Minres
//RCP< Belos::SolverManager<double, MV, OP> > solver = rcp( new Belos::MinresSolMgr<double, MV, OP>(problem, rcp(&belosList, false)) );
//
//
//Belos::ReturnType ret = solver->solve();
//
/////*************************************************************
//
//
//if (precType == "matrixfree" || precType == "Jacobi") {
//    ElementByElementMatrix* ebe = dynamic_cast<ElementByElementMatrix*>(ep->GetOperator());
//    ebe->ResetRestrainedBoundaries(X);
//}
//
/////diagonal = Teuchos::null;
//
//
////------------------------------Print solution-------------------------
//
//temp_time = timer.WallTime();
//
//if (inputFileName.find(".mesh.h5") == string::npos) {
//    int ext_pos =  inputFileName.rfind(".mesh");
//    inputFileName = inputFileName.substr(0, ext_pos)+".mesh.h5";
//    IBT_ProblemWriter<HDF5_GWriter> pw(inputFileName , comm);
//    fe_param.Print(&pw);
//    mesh->Print(&pw);
//    bcond->Print(&pw);
//}
//
//
//if (inputFileName.find(".mesh.h5") != string::npos) {
//    IBT_SolutionWriter<HDF5_GWriter> sw(inputFileName, comm);
//    ep->PrintSolution(&sw, fe_param.get<Epetra_SerialDenseMatrix>("material properties"));
//}
//
//if (printMEDIT) {
//    MEDIT_DispWriter dispw(outputFileName + ".medit.bb", comm, 3);
//    ep->PrintLHS(&dispw);
//}
//
//INFO("output time: " << timer.WallTime() -temp_time);
//
//comm.Barrier();
//INFO("Total time used: " << timer.WallTime() -start_time);
//
//// ======================= //
//// Finalize MPI and exit //
//// ----------------------- //
//
///// erhan added
///// reset smart pointers: assing to null pointer
///// Teuchos::RCP Beginner's Guide - September 2010. Appendix B 2b.
/////manage the vectors as RCP objects.
//pX = null;
//pB = null;
//Auu = null;
//Prec = null;
//belosPrec = null;
//problem=null;
//solver=null;
//
//
//return 0; /// erhan added
/////should be active in Minres else we get the same RCP error. Cyril's main-hack idea might work but not tested
//#ifdef HAVE_MPI
//MPI_Finalize();
//#endif
//exit(EXIT_SUCCESS);
//
//
//}
